# Script that will pad the elements in the first column of a tsv 
# to all the same length so that viewing with vim is easier
# filenames are passed via command line arguments
# Aditya Yedetore, March 2020 
 
import sys 
import pandas as pd 

all_filenames = sys.argv[1:] 

# First must find the longest string of all tsvs passed to script
# Combine all files in the list
combined_tsv = pd.concat([pd.read_csv(f, sep='\t') for f in all_filenames ], sort=False)

# Strip whitespace from 1st col strings
combined_tsv.iloc[:,0] = [x.strip() for x in combined_tsv.iloc[:,0]]
# Get the longest string length
maxlen = len(max(combined_tsv.iloc[:,0], key=len)) + 1

# Add padding to the strings in the first column, and 
for f in all_filenames: 
    # Read the tsv
    tsv = pd.read_csv(f, sep='\t')

    # Strip whitespace from each element of first column
    tsv.iloc[:,0] = [x.strip() for x in tsv.iloc[:,0]]

    # Set appropriate (right) padding for each element of first column
    tsv.iloc[:,0] = [x + (' ' * (maxlen - len(x))) for x in tsv.iloc[:,0]]

    # Get column name, and set it to same name with appropriate padding
    col_name = tsv.columns.values[0]
    tsv.rename(columns = {col_name: col_name.strip() + (' ' * (maxlen - len(col_name.strip())))}, inplace = True)
    
    # Write out the tsv
    tsv.to_csv( f, index=False, sep='\t') 

