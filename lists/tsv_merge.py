# Basic script to merge a list of tsv files given as command line arguments
# Output file name is first argument, other tsv files are other arguments
# Aditya Yedetore, March 2020

import sys
import pandas as pd
all_filenames = sys.argv[2:]
#combine all files in the list
combined_tsv = pd.concat([pd.read_csv(f, sep='\t') for f in all_filenames ], sort=False)
#sort by Rating
combined_tsv = combined_tsv.sort_values(by=['Rating'], ascending=False)
#export to tsv
combined_tsv.to_csv( sys.argv[1], index=False, encoding='utf-8-sig', sep='\t')
