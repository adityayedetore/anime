# Tracking Anime Watching and Writing
This repository originally was for tracking the anime I watch and storing my thoughts them. The commit history gives me a nice timeline of what I watched and when. 

However, now I'm trying to do more than just that.
The goal of this project is to improve my ability to understand what pieces of art communicate, though I will be using anime as the vehicle to do so. 
The section `Goals` below describes this in a bit more detail. 

## Resources

#### For reading about anime

Blogs: 

 * [Wrong Every time](https://wrongeverytime.com) 
 * [therefore it is](https://thereforeitis.wordpress.com)
 * [Mage in a Barrel](https://mageinabarrel.com)
 * [Infinite Zenith](https://infinitemirai.wordpress.com)
 * [Alteir and Vega](https://altairandvega.wordpress.com/tag/anime/)
 * [Atelier Emily](https://formeinfullbloom.wordpress.com)
 * [Isn't It Electrifying?](https://shibirerudarou.wordpress.com)
 * [Sakuga Blog](https://blog.sakugabooru.com/)

Episodic reviews:

 * [Oregairu](https://www.animenewsnetwork.com/review/my-teen-romantic-comedy-snafu-too/episodes-1-3/.87266)
 
#### For finding anime
MAL lists:

 * [Bobduh](https://myanimelist.net/profile/Bobduh) (writer of Wrong Every Time)
 * [ZeroReq011](https://myanimelist.net/profile/ZeroReq011) (writer of therefore it is)
 * [iblessall](https://myanimelist.net/profile/iblessall) (writer of Mage in a Barrel)
 * [InfiniteZenith](https://myanimelist.net/profile/InfiniteZenith)

## Documentation 

### Anime writing 
I write comments on each episode of anime I watch, which can be found in the relevant files in the directory `ep_reactions`. 

The command `./watch_anime keyword` will call a bash script that will open the website were I can watch the anime in google chrome (provided that in that specific anime directory a file `url.txt` contains the url to open) and will create and open a file for the comments on the video with vim. The conventions for file names I use are `anime_ep01.md`, where it is the first episode of the anime that I have given the keyword `anime`. 

### Anime lists
The lists anime I watch will live in the directory `anime_list`. 
Each tsv file will contain information about the anime, and my overall rating of it. 
* `dropped` are shows I dropped part way through. 
* `finished` are shows I finished the at least one arc of. 
* `currently_watching` are the shows I am currently watching. 
* `watched` is a compliation of all shows I have watched episodes of (the union of `dropped`, `finished`, and `currently watching`. 
* `to_watch` are shows I want to watch or rewatch. 

#### Editing the tsv files
The workflow I've found to work well for editing these tsv files is using `open` to open a file with the numbers app, then doing the necessary rearranging, then copy pasting the contents of the file into the tsv opened with `vim file.tsv`. The only reason I need the numbers app at all is that it will make ordering the rows by some values much easier. 

Note that the tsv files probably shouldn't be reordered and pushed, since this might the way that git keeps track of the changes less clear. We will have to see though, I'll check this out. 

Also, I have created python scripts that will add padding to each value in the first column of a tsv so that the rest are aligned, which helps with viewing in vim. The alignment can be executed with `make aligned` and both the alignment and creating `watched.tsv` can be created with `make`. 

### Commit messages
I plan to use the keywords `A:` and `M:` too distinguish between the git commits that involve changes to the information, and changes to the structure. This way when I want to look only at the changes to my watch history, for instance, I can search for A: changes in the commit messages. 

## Goal

### My history with anime
For a long time, the anime I liked the most has been the anime that I felt were carefully constructed to communicate to the audience. 
I could tell anime like Hyouka or Oregairu had things to say, and were saying them with an unusual level of subtlety and skill, and I loved them for it.
Unfortunately, though, I was also pretty bad at understanding what was being communicated. 
I liked the last episode of Hyouka because it seemed to me to be a cutting depiction of the melancholy of unspoken and unrequited love. I missed the budding romance (means the love was requited and Oreki's character development. 
In Oregairu I idolized Hachiman's cynicism and loner ways, without realizing the show in fact communicates the _problems_ with 8man's thought process and point of view. 

### Art as communication 
Now why is it that I missed so much of these shows? 
It is because I was watching the shows without realizing that some person had created them. 
To communicate any meaning, art often relies on the fact that the interlocutor is not passive, but actively processing what they see or read. 
The creator knows that a human with some knowledge and tools is on the other end of whatever they are creating, so they are able to impart more meaning than just the words or images themselves. 
In this way, art is a form of communication between the creator and the consumer. 
When I realized that there was a person behind the choices that go into a show (while watching Eizouken, by the way) I started considering questions I hadn't put effort into thinking about before. 
Why does this character act in this way? Why did they say that? Why frame the shot like this? 
The more I wondered why the creators made the decisions they did, the more I uncovered the wealth of communicated ideas I had been previously missing. 

### How I'll improve 
As a consumer, being fluent in the language the creators use to tell their story aids in the comprehension of what is communicated. 
Creators can communicate all sorts of things: an emotion, a story, a theory, an image. 
In general, the more they attempt to communicate in a episode or show, the more they rely on the audience being active in their consumption. 
I think that so far, I have been a passive consumer. 
A simple way to get better, therefore, is to become an active consumer. 
I will do this by writing commentary on the anime I watch, as I watch them, which will force me to stop and think about what is happening and what is being depicted. 

Of course, another way to improve is to simply watch more anime. 
The language that creators use often overlaps, so figuring out one anime's language will help with figuring another's. 
Knowing that in Hyouka the quality of the lighting is used to convey the changing outlooks of the characters means that when I watch Oregairu I will be better equipped to recognize what the lighting it is communicating. 

The third way to get better at deciphering this language is to read the thoughts of others. 
Of course no one person can understand everything an anime is trying to say, so it's helpful to read what other people think. 
For that purpose, at the top of the readme are a list of resources:

## Random Thoughts

#### Why change the way I interpret art? 
After all, isn't art subjective? Isn't one person's reaction to a piece of art just as justified and valuable as another's? 

The answer to this question might be evident in what I have written before, but it will be useful to answer this question more directly. 
The key is that the art that interests me is a form of communication. 
Just like with any other form of communication, it is possible for the creator of a piece of art to communicate better, and for viewer of that art to understand better. 
No one would be surprised if I said that a foreign language is hard to understand, or that it is possible to misunderstand the meaning of some sentence. 

#### Shouldn't art be interpretable to the layman audience/general consumer? 
This is a bit more complex question than it seems at first glance. 
What I mean here is if a piece of art is somehow not as good if only skilled people, people who have practiced interpreting art and have knowledge of the medium, can understand some large portion of the ideas being communicated. 
The answer depends of the goals of the creator the art: if the creator wishes to communicate to the layman, then the created art probably shouldn't take effort and knowledge to understand. 
The layman consumer, in my experience, simply doesn't have to skill or knowledge to understand complex art without a great deal of effort. 
There are those laymen who will put in that effort, but those individuals are the ones who eventually gather the skills and knowledge to not be layman anymore. 
Just a slight disclaimer: I consider myself to be a layman as well. 
I am trying to learn, but it takes time and effort. 

On the other hand, if the creator wishes to produce art that communicates complex information with skill and subtlety, then trying to also allow the layman to understand may be an impossible undertaking. 
It is definitely possible, but the creator must either rely on a very limited vocabulary that the layman will understand, or they have to somehow get the viewer to think about their work, which is again difficult. 

My personal thought on the subject is that ideally art can communicate complex information to the layman. 
Art is so amazing in part because it is consumed by so many people, so if you can get more of them to understand what you're trying to say, then thats better right? 
An artist who wishes to create something truly exceptional, something truly complex and subtle, might be able to get away with having only a select few understand, but anime is made for popular consumption. 
Although there is a place for 

#### Why is showing better than telling? 
We feel The conclusions we draw when engaging with art more viscerally when we come up with them. 
This is because there are some things that cannot be communicated well by telling. 
For instance, the word 'anger' in 'Sarah was angry' does not capture the emotion, but letting a reader come to the idea of an emotion on their own in 'Sarah slammed the door and leaned against it, trying to slow her breathing' is much more effective in evoking the feeling of anger. 
I wonder the effectiveness of showing is limited to emotions. It seems not. Take 'It was fall' and compare it to 'The leaves crunched beneath his feet'. 
The latter of these sentences is more effective because it communicates the idea that it is fall, and also communicates sounds and feelings as well. 

The balance between showing and telling that a creator must strike is intertwined with the interpretability of the art to the layman audience. 
The more you tell, the easier it is for the audience to understand. 
However, in general telling results in less emotional resonance. 

##### What about philosophical ideas? Should those be told? 
This is an interesting question, because in general explaining a theory is communicates it more clearly than showing it. Philosophical ideas are not usually emotional, and do not require sense imagery or the like. 
However, having a character explain philosophical ideas is often not most effective way to communicate the ideas, and defiantly not the best way to support them. 

#### Are there other ways of engaging with media? 
Hah, the answer is yes, of course. One of those ways is to evalutae the ideas that the media conveys.
This doesn't really interest me though. With this project I am trying to improve my ability to 
recieve the ideas that the creators may be sending, not to evaluate those ideas. 
Another way is to figure out how the ideas that the art communicates reflect on the creators of the 
film, and the culture that consumes it. That is, I believe, the typical area of analysis found in 
the typical film studies class. They use film as a lense into the time in which it was made. This is 
where we have feminist or queer or marxist readings of the film, which highlight some parts of the 
peice of art to enable use to figure out what the creators or the society believes. 

It is true that what a peice of art communicates is pretty important to my enjoyment of said art, but 
I should always keep in mind that I should never dismiss the art becuase of what it communicates. 
This is difficult for me, especially when especially when I beleive that the art lacks subtlety or 
complexity. In those cases, it is all to easy for me to disengage with the art. 

So note to self: if you are watching an anime, and you don't like some of the ideas communicated, 
just deal with it. If you want to get better at understanding the art, dismissing art because 
it doesn't agree with your ideas, even if you're pretty sure the art's got it wrong, is not the way
to go. 

#### Should I care about the _ideas_ a piece of art communicates?
Again, the answer to the question above is yes, of course, but let me give a little more explanation 
to the question. My goal here is to get better at understanding what it is art communicates. Often, 
evaluating that art based on the quality of it's ideas gets in the way of understanding it. True, 
often the art I enjoy the most is the art the communicates ideas that I find interesting or 
insightful or compelling, but when I have qualms with what the anime communicates I can't engage with 
it (see Sankarea). 

The reason I wanted to better understand art in the first place is that art has a great ability to 
change what I believe and who I am, and min-interpreting art has lead to negative ways of looking 
at the world for me (happened with origairu, and with the anime I was binging before my 5 year 
hiatus). The second reason is that I feel I am not getting the full experience if I am missing 
half of what is in the work. 

#### How important is authorial intent? 
Often when we creators create art the communication relies on the knowledge that the interlocutor is aware that the art has been created by a human. 

I'll come back to this later. 

#### Subtlety 
I think that from the point of view of the consumers, the usual distinction between showing and telling is better understood as subtlety vs obviousness. A creator often needs to hear "show, don't tell" to be able to create better art. We need to understand that when art is subtle, it does some things better. When we see something we don't understand at the first glance, that makes us think. And when we come to the answers to our questions by thinking about them, the answers seem more significant and they speak to us more. While the themes and messages and things of obvious art may be just as good as the subtle art, it is tempting to not think about them at all, since no effort is required to understand them. Often the simple pleasures of the art in overpower our desires to critically engage with the art when the art is obvious. Another benefit of subtlety is that the art is that the art is often more realistic. Seeing character development, themes, meanings, messages, and intent are all really difficult in the real world. The events shown to us in art are carefully selected to give us the important information, but in the real world that information may be hidden or not even presented to us. Thus art that hides the messages in some way is more realistic. 


### The ever present dilemma:  
#### Should I tell people I like anime?
I don't, but should I? Some people may say, "Yes, of course! You should never be afraid to tell people what you really like, who you really are. If you hide things from the people around you, then you will never be able to connect with them. And if you don't ever give them a chance to accept you, how can they?" 

There are reasons, however, for why I may not want to tell people. Anime is not respectable, and in general is thought of as a perversion. That perspective is, in my opinion, pretty justified. The prevalence of pointless fan-service and shitty writing pervade the anime world. Moreover those who are open about the fact that they like anime are also likely to be the ones who like the anime made with the least skill and subtlety, which means that anyone who doesn't watch anime probably only has seen the worst of the those who do.  

For me, actively hiding my anime interests is not the greatest feeling. The stress of someone finding out my secret and thinking less of me for it is not ideal. However, since I haven't told people, I do not often feel the need to justify anime to others, which seems like a horribly difficult task. In the end, hiding this from people will prevent me from having close friends, so telling others is probably the best course of action. Of course the fear of (arguably justified) judgement cant be disregarded either. What a dilemma. 


