It is in communication with flip flappers, and Madoka. 

These dreams are a place where real world problems are made into defeat-able antagonists. Our protagonist Ai Ohto has to learn what it takes to save a friend. Specifically, in this episode, she had to learn not to stand by when her friend is being bullied. 

Dreams as a way of dealing with psychological trauma. 

"Crosswalks are still scary when you are moving with the crowd." 

I get the feeling this show is going to be about the strength that it take to save someone else, and also to save yourself. 

She is going to come to the realization that nothing she does will bring her friend back, and that she has to go on with her life. 
