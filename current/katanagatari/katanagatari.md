This is a show about how we can be bound by the weight of legacy, and so charge headlong towards our ends. 

It is a show about the cycle of violence, and how love can perpetuate it. It is a show about how the stories of those who fall are inherited by those who survive, and how the stories can consume us with the desire for vengeance. It is a show about finding purpose, a purpose to fight, a purpose to defend. 

It is a show about what makes a human different from a tool. A show about a weapon learning to act for themselves, and a human learning to act for others. And conversely, it is a show about a weapon learning to act for others, and a human learning to act for themselves. 

It is a show about our obsession with swords. Our 'romance' with weapons, if you will. 

Episode 1:

In the first episode, Shichka is shown to be without any purpose. His only show of any drive to act is when his fighting style's honor is challenged. He has inhereted that mantle from his father, who died a year ago. The reason he "falls" for  is that she was willing to ask for help from the killer of her father. This is not love, it is respect. 

Those who use the swords are said to be poisoned by it: they do not want to relinquish it's power. The possession of a weapon comes with inheritance, with a burden to bear. And the swords are gotten through blood. No one who wields one of the swords will live beyond it. 

There is a visual boundary between Togame and Shichka throughout the whole first episode, until he tells her that he has fallen for her. While the words of the episode and the actions don't quite indicate their antagonistic nature, as they seems all chummy the first time they talk, the framing does let us know that they are not really on the same side here. They are antagonists. 

Togame is 'bound' by rope, but also in a metaphorical sense, bound by her ambition. She doesn't want to work with the son of the one who killed her father, but has to, if she is to make her father's goals a reality. She is like a spider, spinning webs around herself, but might happen to get trapped too.  

Episode 2: 

Clothes are explicitly discussed as symbols of being bound, or being free. Togame buys all these clothes, but never wears them, preferring the purple ropes, and a circle binding her waist. She is also 'binding' Shichka with her hair. She is wrapping him up like he is prey, or like a she is a snake. 

We then get the fact that Shichka has no moral compunction against killing others. 

The discussion is also explicit about finding purpose. The lone swordsman has found purpose in defending a dying home. But this 'found purpose' is a double edged sword, and it ultimately makes him end his life for no reason. 
In a way, a weapon also uses the one who wields it. It is the way of things that a sword held is a sword carried. 

Episode 3:

Again we have another bound by the poison of the sword. This "poison" that they are talking about is the unwillingness to let go of the past, of the connections to the people that had it before. 

We also have more about Togame's clothes. She wears them so that she doesn't forget the past, so that she continues to be bound by it. And her hair is the same way. It is a constant reminder of the day that everything was taken away from her, and so is a symbol of that legacy. And when the 3rd sword owner became the chief of the bandits, soaked in blood, her clothing looked the same as Togame's. 

It is terrible that the owner of the sword had to die, but the real tragedy here is that she is the reflection of Togame. Her father died in the rebellion, and she became obsessed with swords. Togame could very well be one of the maidens "saved" by the power of the sword. 

The show is constantly (literally) asking the question, why do we fight? Why do we kill? The Maniawi fight for money, or so they say. And the wielder of the 3rd sword fights for the maidens of the shrine. But why does a sword fight? 

The show is about the inability to let go of inherited bonds, and stop the cycle of violence. 

The show is saying that we fight for love, and the only way to stop the fighting is to let go of love. 

Episode 4: 
