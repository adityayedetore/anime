### Elegy for Ganymede 

The effects of time on people. Being stuck in the past, without resolution. The grief of loss can keep you stuck in memories. 
