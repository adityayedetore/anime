### Cowboy Bebop Episode 01 
### Asteroid Blues 

This is the 3rd (4th?) time that I will be attempting to watch Cowboy bebop.
For this show, I will be taking extensive notes. 

The first scene marks Spike as haunted by memories of violence, pain, and blood. 
He is like the cut flower in the puddle, bright and aluring, but waiting to die. 

the same person who was in the dark in the beginning of the show, Spike, has the lights turned on on him. 
The everyday matters of living with other people and eating takes him away from his memories of pain and violence. 
But we can see that he is constantly training his body, in preparation for that pain to come back. 

This show does such a great job introducing the space faring aspects. 
And the exposition about them being bounty hunters is done really well too. 
Not only is it inter-sparced with interuptions of character moments, but also it is naturalistic. 
It makes sense to have them talking about this, this is their job. 
And we get to hear more about Spikes way of getting the job done: he doesn't care about the welfare of others any more than he cares about himself. 

Wow, this 3d is used really well. The moon/asteroid is done in 3D, and you can get rotation that you would never be able to accomplish with traditional animation, but it doesn't look out of place at all. 

Interesting. Whistling takes on a meloncholy aspect, especially when it echos like this. 

The music places this as the boondocks. I wonder if that was intentional. It is a interesting combination, with what looks like a slightly more decrepid version of our cities functioning as the boondocks in a future civilization such as this. If it was intentional, it is a great way to show that this setting is in the future. 

So it's more of an old cowboy town than a boondocks town. That's fitting. The choice of drugs is interesting. This is a drug that changes how you see the world. Asimov says "Yeah, keep those eyes open!". 

Hmm, very noir heavy. He was killed once before by a woman. 

Spike is hungry, and so is the mystic, and so is his plane. The man he is chasing is throwing up. 

The first woman he meets, feeds him (sorta), and saves his life. 

Wut? The same three old men, and the same server? Do we have some cloning going on here? 

That fight scene is masterfully done. We see Spike's confidence, his disregard for his life (even maybe a desire to die), his acrobatic skills, his penchant for drama. 

Now that is a powerful image. 
"We will never get out of here. We will never see mars". 
She is taking control of her life by killing herself. 
She gave up on life, gave up on hope. 
The drugs, as terrible as the were, were that hope. 
They were the life that was keeping her going. 
But they only led to the distruction of the one she loved. 

Spike sees himself in that. 
But for some reason, there is something that is keeping his flame lit. 
Is it related to the rose? 






