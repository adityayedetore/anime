### Heavy Metal Queen

The price tag of life. 

Tension between life and death. Living on the knife's blade, or dying on it? 
