### Waltz for Venus 

Lets think a bit, shall we? 

One big question this show is asking: Why value anything if it can become worthless with the pull of a trigger? Why value life, when death takes it away? What's the point? 

Spike tells himself that he no longer cares for anything or anyone now that his love is dead. We see that this is both true and false. He does care about some things, when he is acting instinctually. Spike is also trapped in a spiral with death. Being human means being hungry, and acting to preserve himself. He is a hunter with no home to come back to. 
