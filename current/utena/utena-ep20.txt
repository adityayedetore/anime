Wakaba's desire to be special is wrapped up in her desire to be special for Sionji. 

Interesting. Going from meat to caterpillar to leaf. She is looking at the meat as she says "If Saionji gets back into school..." 

The leaf is doing double duty here, it is symbolizing the fact that Wakaba is no rose, but rather just a leaf. Ordinary not special like Himemya and Utena. It also sumbolizes the transition to their most primal selves, the deepest and darkest feelings. This anime doesn't fall down on the side of saying that releasing those feelings are harful. Often, they are good things. But that is only when they are properly defeated by Utena. 

This show is all about what we can do when our desires, identity, ideals, and morality is at odds with the world. Do we accept the chains, and die without being born? Do we change ourselves? Or do we change the world? The first season was the war of different ideal of what it means to be a prince. This time, it is different seeds that grow into villians. 

