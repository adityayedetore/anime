## The Rose Bride

I started this show a while ago, and I dropped it. I think that may have been because I was unable to understand it. But now, I think I've gotten better at these things. This definitely is the type of show that will require notes though, so I think I am going to take them episode by episode. I'm prepared for thematic complexity, and for things to not make sense at first. Lets see how this goes!

The first line of the opening song is "Lets live heroically, lets live with style". Here we are already combining two ideas that are often seperated. To be a hero is often dirty and dark, but here it is to live with style. This evokes the stage, where heroes can be stylish. 

Flowers, obviously. Also the white vs pink rose. And like bobudh said, her 'power' is not a male power, it is a female power. 

Obviously a bit about societal expectations, and trying to break out of them. Trying to 'rebel' against them.

She is trapped in a bird cage. 

Also she seems a bit innocent, in a strange way. Utena I mean. She seems to assume that things are right in the world. That slap shows her that this is not the case. 

Her name is Anthy, which is Greek for flower. It also has something to do with eternal beauty, or so I gather. Also her name literally has 'princess' in it: hime. 

The song says that this is a place where death, birth, life, eternity, and the end converge and mix. 

A difference is being drawn between a 'prince' and a normal person. Thus the honorific for older schoolmate is an insult. 

How did I not like this? This is fucking awesome. 


