"Cast in the name of god, ye not guilty". I wonder if this is ever going to be explained, or not. 

Again we have memories as power, and also as dangerous things. 

Dorothy was raised by this man. So it seems that androids don't have their memory wiped. 

He's the frickin batman. 

He turned into the knight in shining armor here, and saved the damsel in distress. Exactly what he said he wouldn't do the last episode. Clearly he either doesn't know himself, or he is trying to affect a cool indifferent air that he doesn't have. 

This 'be who you are' is a little undercooked. Maybe there is subtext I'm not getting, but I don't see Dorothy having an identity crisis. 

Dorothy is in a bad place though. In the space of two episodes, she has lost two fathers. She doesn't seem affected by this, strangely enough. Maybe part of her arc will be developing emotions. 

It is interesting that after these two first episodes, almost all loose ends are tied up, except the one that Roger commented on, which is that Soldano called Dorothy his daughter. 

We also have some stopped time, flowing time, and starting time symbolism here. One thing is that without memories, we can't tell that time is passing. So maybe regaining our memories allows time to start again? 
