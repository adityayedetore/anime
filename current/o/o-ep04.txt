If what happened 40 years ago hadn't happened, man would still fear the dark. The fear comes from not knowing. He then averts his eyes from that fear and acts as if he hadn't any memories of his life, of his history, of the very beginning. 

Repressing emotions, and memories of the past, because uncovering them is too much for us to take? When is it good to forget? When is it good to forgive? 

She is a living representation of trying to bring back the past. She is a android, a zombie. 

EXPO'o4. A dry eraser. 

Hmm, this was quite the strange episode. Dealing with events very quickly. 

Of course, memories have to do with identity. Dorothy is also trying to find her identity. She rejects this monster, she can't be like that. 
