## A guide to learning how to understand art
### A step by step outline

In art, there are things that you can understand from just looking. Other things require some thinking. We call those other things the subtext. To understand art, you need to learn to see the subtext. 

Here is one path towards doing that:
1. Find art that you enjoy, and that is acclaimed by critics. 
2. Try to find other's analysis of art you enjoy.
3. Start taking lots of notes while you watch and read. 
4. Find books that talk about how art is created. 

*Find art that you enjoy, and that is acclaimed by critics.*

To start this process, you need to find art that has enough subtext. Not all art does. Art that is acclaimed by critics often has subtext, so that is a good place to start looking. 

*Try to find other's analysis of art.*

This requires a bunch of googling. You probably won't find much analysis of art that doesn't have subtext, and if you do, it probably won't be that useful. 

YouTube is a decent place for this, since what you find there is entertaining and often bite sized. One thing to keep in mind is that there are five types of analysis you will find on YouTube. We are looking for analysis of the themes, messages, etc., analysis of the techniques used to get those things across, and sometimes even evaluation of how well the art gets across it's themes, message, etc. 
What we don't want is evaluation of validity of the themes or messages. 
When first starting out it is all to easy to fail to engage with the art because the art is saying something that you believe is incorrect. 
When analyzing art in the way I want to be able to, it doesn't matter if what the art is saying is not right: what matters is that we understand what the art is saying.
You can probably go ahead and watch those videos if you want, but just know that they are probably not what we are looking for, and not how we should be thinking of art at this stage.  

*Start taking a lot of notes on what you watch/read.*

Really this should be done at the same time as the first. You need to take copious amounts of notes on what you are watching. This is going to be difficult and time consuming. For a 20 minute episode of some show, an hour and a half spent pausing every two minutes to write a few sentences or rewind is normal. The benefits here are many. This will help with remembering what happens in the show or book. Also, it gives you time to think about what you are watching, and to ask yourself the types of questions that were listed above. I've found that some shows are more easy to take notes on than others. Some shows are subtle, and need some picking apart, and these shows are pretty easy to pause and write about. The shows that are not subtle, on the other hand, kinda just suck me in and I forget to write anything. As I have written above, subtlety tends to make us think, which makes us want to pause. Obviousness does the opposite. So maybe look for shows that are subtle, even though they might be a bit more difficult to understand. 

*Start reading some books about how to create narratives and character arcs and things like that.*

Understanding the craft of the creators gives insight into what the creators are trying to say. Note that by this stage you should probably have been thinking less and less about the authors themselves, and thinking more about how you are interacting with the text. We are not really trying to understand the creators here, but trying to understand the piece of art. 

### Some more detailed notes
#### How to begin
Often, the first step to learning how to analyze art to realize that the art has been created with intent. People start to understand that artists often have a message. They start to ask themselves questions like, "Why did the artist choose to make the curtains blue? Why not red?" Doing this is not actually the most effective way to understand art, and most people move past this stage as they learn more. 

The reason realizing art is created with intent is often the first step is that before we start trying to improve our understanding of art, we must be convinced that there is a lot that we are missing by not attempting to understand art. 


#### The importance of remembering
To be able to understand the art, remembering what happens is really important. Misunderstanding art often comes from misremembering it. 

Before getting to higher things like theme or message, we first need to understand what is happening in the art at a basic level. We need to understand the chain of causes and effects that the story depicts. 

### Techniques for understanding 
After each episode: 

* Think about the themes that were touched on. Themes are ideas (love, family, freedom, ...) that are repeated in a piece of art. Then, compare that list with the list made at the end of the previous episode. 
* Sum up the purpose(s) of the episode. There are probably many, but try to think of the main ones. Was it to introduce a character? To introduce the conflict? World-building? Fan-service? 


