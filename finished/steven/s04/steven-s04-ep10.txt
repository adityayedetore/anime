Crack the Whip

I don't think this episode had any one specific theme that it was going after. For amethyst, this was about self confidence and her way of living and fighting. For Connie this was about gaining self confidence through battle. 

=======
Steven vs Amethyst

The story here is fantastic. 

The previous episode showed us how competition and bullying could bring us down, and how winning can bring us up. 
This episode showed us how Competition between friends can help our own self confidence when we realize our own strengths. 
