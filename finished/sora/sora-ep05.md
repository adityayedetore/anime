Her love of Kimari is the love of a older sister. She likes to take care of Kimiari. But now she is seeing Kimari grow up, and grow past her. Symbolically, this is depicted with her giving Kimari the bucket of water to fill her sand leaf boat thing. What this indicates is that she is the one who helped Kimari unlock her potential. 

Black hair gets strength from defying others. 

She lent Kimari the game, and eventually Kimari surpassed her. This girl's "Well, don't push yourself too hard. If you fail, you'll just regret it all the more" really means, I want you to continue to be dependent on me, so don't give things your all. And when it comes down to it, this girl tries to stop Kimari from succeeding by pulling the plug on the game. 

Hmm, these symbolic flowers, are they what grows from the symbolic water? 

Really, this is helicopter parenting. 
