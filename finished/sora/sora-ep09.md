Something about competition something something, how there is always another chance, how people are surprising? 

It is about trying again, and the ghost that is driving us to do amazing things before we die. 
