Things I enjoyed: the animation was superb, as were the character faces. Things made me laugh, and tear up, and all the characters were cute. 

Things I didn't like: melodrama. The question to ask is, why does it bother me so much when characters don't act like real people? What does it even mean to act like a real person? One example of a character not acting like a real person is when a character doesn't know something that they should know. 

I think the ideas were often not explored with enough complexity for me to get to interested in them. 

The show is supposed to do more for you emotionally than intellectually, but since it didn't do much for me emotionally, I didn't get much out of it. 

For the animation: 9/10. For the personal investment: 7/10. So that averages to an 8/10. 
