This time I'm going to see the heavens. Is she going to see the black haired girl's mother? To see the stars and remember? 

"It's not a prison, a place trapped in ice. It's a place still hidden, filled with possibilities, Earth's most amazing treasure chest. 
