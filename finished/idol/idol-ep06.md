Ok, so the goal with this episode is to note things down whenever I like them. I am trying to figure out why I did not like the great pretender, and maybe this will help. 

Ah, ok there is one thing I like. I like the slight humorous tension between the agents' enthusiasm and the producer's awkward attempt at cheer. 

I love the animation of the op!!! It is some of the best animation I have ever seen in anime. 

I love these reactions! They convey so much! When the dude asks "Why did you choose these three" they all straignten a tiny bit, and look a bit alarmed. It is obvious that they hadn't thought about this before, and they are interested to hear the answer. Then when the agent says "they balance eachother well", they all smile and straignten up even more, looking confident. This is a charming character interaction.

A character that gets trapped in her own manga fantasy (looks a lot like a female version of Jojo's bazzar adventure?), and makes weird expressions while people are watching. I feel for her, and this is a slight cringe in a good funny way. 

And this is really good! There is great conflict when people are chosen and people are seperated, and that is something that we can all relate too. And this is a great place to show people's drive! And also, that scene with green eyes was really charming, it is enjoyable to see people who care for each other and do things for each other. 

The great pretender had NONE of this! A miscommunication! Something as simple as that. And people having problems that they are struggling against. The producer is good at his job in some ways, but he doesn't know everything. 

And there is stuff built on the fact that he doesn't know the strengths and weeknesses of his idols! That is fantastic! And as the producer gets to know them, we get to know them too! It all fits together! Did the great pretender have any of that! Did it? Haa? 

It gives me warm feelings to see people supporting each other. The great pretender had none of that. 

And it's about fucking talent! Talk about a show made for me! And this builds off her other scenes where she showed her talent... ugh this is so much better. 

Characters struggling against themselves, against the circumstances, failing, succeeding, all the great pretender had as one twist after another. 

So so much fucking better! That reaction, first as slightly making fun of her/laughing at her funny act, then being impressed by Iori's intensity, that is fantastic!!! That is the face of someone falling in love! And it took all of 1 second! 

Realizing that they were doing something wrong before, then learning through the course of an episode... the great pretender had none of that! 
