oops, forgot to take notes. 

It made me feel a bit awkward with him running after her. Hmm, why is that? 

Ugh, the expressions here are so good. This one says "I don't quite believe you, and I don't want to look like I'm paying attention, but I am paying attention, and I believe you a bit". It's fantastic. 

"You'll stand on a stage with everyone else" I think that's exactly what she doesn't want. She wants to be in the spotlight, alone. 

That is pretty great. "I want this concert to be a success". That is different from her previous desire to only sing, and also shows their growing friendship. 
