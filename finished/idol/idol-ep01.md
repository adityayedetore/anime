### The Idolm@ster Episode 01 
### The Girls Start Here

I literally know nothing about this anime, except for the fact that Bobduh likes it. Seems it is based on a video game: interesting. This title, suggests something to do with sexism: are we going to get an exploration of the sexism of the idol industry? I don't think I'd mind that. Lets dive in!

Ooh, I'm a fan of this animation. This is exactly the type of animation that I like. Also, that pan down after she falls, that draws attention to the camera. Are they going to go somewhere with that? Are we setting up the male lead as a camera? 

Ah, interesting, the questions are written out. Is this the style of idol shows in japan? 

This is like a documentary! And there is someone behind that camera! That's pretty sweet! So these people know that they are being videotaped. That colors the way that they act, and what they say. I haven't watched many documentaries, but these girls act exactly like I would assume people would when in front of the camera. 

The face for the camera, vs the daily face. Hmm, we are being put in the shoes of this idolm@ster pretty often, but not all the time. 

* Akizuki Ritsuko 765 producer. 
* Futami Ami and Mami, twins,
* Hibiki Ganaha, has hamster, 
* Yuhiko Hagiwara, scared of the cameraman

Hmm, so maybe he sometimes sets the camera down? But no,

All of these characters have their own struggles. This is a pretty great feat of characterization.  

I think that the documentary style lends itself well to this sort of quick characterization, due to the fact that causal and story discontinuities can be easily excused. 
