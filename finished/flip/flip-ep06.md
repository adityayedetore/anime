### Pure play 
### Flip Flappers episode 6

Whats that rush Cocona? Do you want to finish collecting the gems so badly? What is waiting for you on the other side?

The op speaks of relationships and time, and looking to the future. The visuals are of rain and reflection. 

The previous room that Papika thought smelled bad was the nurse's office, a place of sickness. Is Irodori sick? She doesn't have any right to paint her nails? 

Why are they immediatly taken off? 

They are in the painter girl's world now. 

And now they are literally in her memories. Irodori has a conflict with her parents. They seem to be controling, and do not allow for much freedom of expression. They do not think her colorful art is valid. But she runs away to her aunt's house. Her aunt is a school teacher who was loved, but has since retired because people say that she is too old. Irodori loves her aunt. 

The nail polish: her aunt tells her that the nail polish is for when she is older and taller. 

There is something that is deemed unacceptable about her aunt here. It may be that her aunt is know to have made some transgression, but what was it? Is it not acceptable for an old woman to have a child? No, that doesn't make sense. Maybe she was alleged to have done something bad to children in the past. Or maybe she is a lesbian? No real evidence for that though. 

The color pallet has completely changed, from warm to cold. She lives in a household where her parents argue and don't seem to care for her. 

Her aunt has memory loss. And she is dying. This seems to be a classic tail of loss and the resulting grief. Is there anything new here? This tale of love and loss does reflect on cocona's circumstances. She lives with her grandmother, who is older as well. But that has made corona hyper-aware of life, time, and death, so much so that she only watches through the window of her life, and is unable to take action. But for Iro, it is different. It doesn't look like she is aware of death yet. 

All this pain is couched in terms of identity. Iro wants to cut off the painful parts of herself, but to do that is to cut herself. 

Being an adult here means to not run away from yourself. 

And yes, this ending does seem a bit unsettling. 
