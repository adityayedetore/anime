﻿### Serendipity 

"anata wa sugu ni anata o  ""tada no mono da"" to sagesumu           |  You keep calling yourself ""nobody who matters"""
watashi wa sonna anata o  dakishimeru tsumori sa                     |  And that just makes me want to hug you
guuzen  sekai ga  katachi o kaeta to shite mo                        |  It doesn't matter if the world suddenly changes
anata ga ireba  nanni demo nareru ki ga shita                        |  As long as I'm with you, I could handle anything
masshiro na yume o atsume ni ikou                                    |  Our hopes for the future are still hazy
bouken ga iro o tsumuide iku yo                                      |  Our adventures will fill them with color!
majiwaru  fumihazusu  kawaridasu  mirai ga temaneiteru yo            |  We'll get mixed up, we'll stumble, we'll change, the future beckons!
noriokurenaide                                                       |  Don't be late!
tooku demo zutto soba ni iru                                         |  No matter how far apart we are, I'm still be by your side
tsunagu kokoro  zutto hanasanai                                      |  Our connected hearts will never let go
