### FLIP FLAP FLIP FLAP

Kossori socchi o nozoi tara socchi mo kocchi o nozoite ta        | When I secretly looked your way, You were already looking at me-
Hitomi no oku ochi te yuku koko wa doko na no ka na?             | I fall deep into your eyes… What on Earth is this place?
Sotto te o tsunai dara totemo yawarakakatta                      | When you gently took my hand, I noticed how soft it was;
Onnaji yō ni kanji te iru to nazeka wakaru n da                  | I could somehow tell You were feeling the same thing.

Anata ga mi te iru mono                                          | These dripping tears
Utsushiteru dripping tears                                       | Reflect everything you see.
Tokei o sakasama ni tadotte i ta Tripping                        | They were tripping, tracing the clock backwards.
FLIP FLAP FLIP FLAP tōi koe wo                                   | FLIP FLAP FLIP FLAP A far-off voice…

FLIP FLAP FLIP FLAP tojikome ta                                  | FLIP FLAP FLIP FLAP …was sealed away,
Kagi no kakatta hako o hirai tara                                | But once we open up the lock on that box,
Hora, hirogaru yo                                                     | It will expand to cover this space.
FLIP FLAP FLIP FLAP hoshi no hikari mo                           | FLIP FLAP FLIP FLAP Even the lights of the stars…

FLIP FLAP FLIP FLAP tobihane te iru                              | FLIP FLAP FLIP FLAP … are bounding about,
Iko u yo fushigi na mabataki ga                                  | So let us go! 
Michibiiteru sekai                                               | To the world that mysterious twinkling is leading us to.
Anata to futari de FLIP FLAP                                     | You and I, together: FLIP FLAP!




