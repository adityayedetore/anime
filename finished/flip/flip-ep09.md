### Flip Flappers Episode 09 
### Pure Mute

"My mindset is totally unlike theirs" I can't help but feel that this line must be a awkward translation. 

I get what Bobduh was saying now. The creators of the last episode didn't really think about the overall narrative flow. Now I have to go back and remember what happened at the end of episode 7. Ok so some Mimi person had to leave them, with a baby, and in some dramatic fashion. Cocona now believes that Papika is mistaking her for this Mimi. 

Whose world is this. Also whats with the hair/mop. 

Now there is a literal window that Cocona needs to break through in order to save her friend/more than friend, Papika. 

Admitting her want to be with Papika is what allows her to break through the glass. Why is it always that a relationship is what allows people to come out of their shell? 

She was told to become close to Cocona. What is so special about this girl. 

Love conquers all. The reflection of shared experience, of the bond. 



Fuck. I didn't even get that this is Yayaka's world. 

Hmm, this is unlike many of the other Bobduh's writings. Yes, there are many things I missed, in fact I missed almost everything. But even after reading of it, I think the narrative and symbolic advancement is thin. 

"While Cocona has struggled to find a path she wants to follow, Yayaka seems to have the opposite problem; an inability to escape from the one path she’s been granted." Ok sure, but this isn't really made clear. A barren world, is that really the best world that can be used to display a lack of choices? 

"after all, Yayaka’s most fundamental desire is to hold on to Cocona without losing her home, and that sentiment is perfectly expressed through “I’m not giving up on you or them.”" Really? Like this is not clear at all. 

Yeah, not my favorite episode. Let see about the next one. 
