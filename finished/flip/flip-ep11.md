### Flip Flappers Episode 11 
### Pure storage 

I've not been that happy with the last few episodes, lets hope we get a good one here. 

Lots of glass broken in Cocona's house. Does this symbolize the fact that she has fineally broken free of her paralysis? Seems unlikely. 

She has turned into a god. She kills people by smothering them in blooming clovers. References to Castle in the sky maybe, and maybe Akira. "I am the only one who may touch her." Overprotective much.

Also refernces to Princess Mononoke. 

I am not a fan of these character designs. Or at least this shitty nyunyunyu girl. 

Ok so mimi was pregnant. Who was she having sex with. Like this doesn't make any sense. 

So this is like inception. She goes into her own mind, and then plants some seed. 

Just like with your father, people have multiple faces, and so does pure illusion. And they're all real. 

Why does Salt want to kill mimi. That doesn't really make sense. But I guess the point is that Mimi rejected people. 

Ok, so people's motivations are complecated, and they may seem two faced. But that doesn't mean that their feeling are false.

Ok, what I didn't get here is that the fact that the last few episodes 'didn't make sense' on a narrative level was a purposful choice for this show. It is commenting on people's critique of Evangelion, claiming that the plot devices are not important. In shows like these, the sci-fy narratives and elements just serve a backdrop for the actual drama, which is character and theme driven. But by flipping off plot devices, Flip Flappers has created characters whose motivations do not hold water. Character conflicts happen when people's beliefs interfere with them achieving their goals. Without logical conflict, it's hard to draw the line between the internal struggles of the character and the struggles of the character in the world. 

Lets be a bit clearer here. Character conflict can take multiple forms, but basically it happens when something stands in the way of some character getting what they want. When logical narrative is discarded, it is hard to see the motivations for their actions. Like why does Salt raise a gun to Cocona/Mimi? Why does Mimi decide to give over herself to the dark side, and why does that cause her to gain powers? Like from her perspective, why would changing her personality make her able to protect Cocona? Why can't Papika tell Cocona about Mimi earlier? 


 
