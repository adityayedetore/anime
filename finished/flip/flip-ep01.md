### Pure Input
### Flip Flappers Ep 1

Hour glass, girl's faces, uterus, countdown of time remaining. A countdown that begun with her birth? No, the countdown of an examination. 

Omg I love this action. The movement is done so well. They see eachother as bright lights. 

She asks, "Where should I go?" She is indecisive, or is not able to think about her future for some reason. 

Interesting, the area around her school seems frayed around the boundaries. It is almost like it is a dream, or not a real place at all. She wandered off, and now she is suddenly in a strange place. Her friend has little worry about this, and isn't really even thinking about it. 

Hmm, palpation time? Is that heart palpitations? Are they sick?

They were transported into a pipe. If that is not a reference to mario, I don't know what is. 

Damn what is she wearing. These running animations are fantastic. 

"Is this a dream? I've heard of lucid dreams before." 

The world is telling Cocona (Cocoon?) to make decisions, and for some reason she no longer acts like a kid. What happened in her past that made Coco so depressed? 

This scene looks a lot like Nausicaa, underneath the jungle. Is that where they have gone? To a place untarnished by human poison? Omg and she has the same nuts as in Nausicaa. And another scene just like Nausicaa! The snow drifting down from the sky. What is the meaning of the connection? Is it just a homage, or is it something more? And the beating heart and brain at the center of the robot, that is just like the titans. That heartbeat might actually be taken directly from the movie. And it's the oohms! Holy shitt, the music, the flying, everything! The oohms are falling into the lake!! 

"You'll die!" We have one that isn't afraid of death.

Going into and out of the Pure something something falling into black water.  

Oh shit, now it's laputa! A blue gem, one eye robots, that music!A

This is about time, and life, and choices, and breaking glass barriers. I am so excited to watch the rest of this, I get the feeling that it is going to be such a great ride. 
