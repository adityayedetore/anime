### Flip Flappers Episode 10 
### Pure Jitter 

Papika is remembering/thinking about her past with this 'Mimi'. Yayaka has chosen Cocona over her old life, and has been hurt in the process. The stretcher that Yayaka is laid out on recalls hospital beds. Cocona is understandably worried about her friend. 

Also I'm confused, the scratch on Cocona was on her left cheek last episode. 

Toto is also on a bed, dealing with the injuries from before. These twins are cold. They don't seem human. Not great writing/voice acting. 

"The time of rebirth and liberation is upon us" 

Papika tries to talk to the sleeping Cocona. 

The face in the dream finally turns, and it is a girl with red eyes who looks like Cocona. It's Mimi. So Cocona is being haunted by the image of someone who looks like her/Papika, and is someone that Papika loved. So this was the illusion, someone who represented both old age and youth. 

Lol, so she regained her memories. How cliche. 

Food solves all problems. Nothing deeper going on here. Those flowers though, they are the same flowers that were in Yayaka's flashback. And they are growing out of the head of a zombie. So they are kinda like his brains. So somebody's head is filled with flowers now? Is this trying to say that now Yayaka's head is not empty, but filled with flowers? If so, it is a pretty roundabout/clumsy way of saying so. I'd put those flowers into the reflection of her eye or something. 

They are kids genetically programmed with amorphous powers to be able to go to pure illusion. 

"If you collect the amorphous, you are able to control pure illusion." Pretty boring reveal. These narrative turns are not great, especially after reading Gene Wolfe. Since the pure illuions are people's psychological mind-spaces, that means that they are able to control people's thoughts. 

"If it brought about a world where nobody had to get hurt or hurt others... If choosing my rightful place meant loosing it, I thought I'd be happier letting someone make that choice for me." This talk of choices and things is a lot like what Cocona's difficulty is. Cocona has a lot of choices, but is paralyzed. It seems that Yayaka doesn't have much choice, or something like that. 

Great, starwars references. 

Aaaaand, the big reveal is that inside Cocona is one of those amorphous. I was like, shocked. 

This music is pissing me off. 

And this ku ku shit is also pissing me off. What, was she genetically engineered to be a dog? 

Salt and Mimi and Papika(na) were part of some experiment to go into Pure Illusion. Mimi was the only one who could do it, salt was the son of a scientist, Papika was the first one to be able to go with Mimi. 

This Mimi likes sweet carrots. 

Cocona wants to feel special to Papika, but since Papika liked Mimi too, Cocona doesn't feel special. What, is she a 5 year old? 

Her grandmother was faking, it seems. This episode is doing everything it can to piss me off. 

Its the ghost of her mother. So the birth mark is the inheritance, which is power, something something. 

Let me take two seconds to get my thoughts in order before looking at what Bobduh has written. On a literal level, what did this episode tell us? A while back, Papika and Mimi were experimented on. Mimi had the power to go to pure illusion. Papika was her first friend. Somewhere, Mimi had a child, and had to leave the world/step through time. Papika and Salt, who had a crush on her, didn't want her to go. Then, at the present time, we Cocona is mad at Papika because she believes that Papika wouldn't have approached her if not for the fact that she is somehow related to Mimi, and Mimi is the one that Papika loved first. This is all true, and it is also true that Papika loves Cocona. The evil organization breaks in and tries to capture Cocona, but this is all a part of Salt's plan. Then when Cocona discovers that her grandmother was lying to her, and is part of the evil organization, it turns out Mimi was living on in Cocona's birth mark, and Mimi takes control of Cocona, and protects her from danger, breaking her house in the process. 

Now, the character reading. Cocona has always been unable to make decisions for herself, and is unwilling to act when there is a possibility of hurting others. She is also pretty nice, meaning that she cares deeply for other's emotions. This is what makes her "nice". In this episode we see her rejecting Papika, who is the one who has been drawing Cocona out of her shell. What is the conflict being set up here? Cocona is being protected, behind glass again, and is so unable to grow up. So she has to overrule her mother, and take back control of her body. 

Ok, so it seems I missed the flower symbolism, but lets be honest it was never that clear in the first place. Visual themes and symbols are not simple in this anime, and require leaps in logic or lots of thinking to unpack. It doesn't help that the camera rarely highlights these symbols. 

This show better get better like right now. 
