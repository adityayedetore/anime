00:00 The episode opens to a scene that parallels a opening scene from a few episodes ago. Now its not zombies discussing the mundane as they bury the bodies of the dead, but it is humans discussing the mundane as they bury the bodies of the zombies they killed. The image of the woman reaching for her lunch with fingers coated in blood is horrifying. 

21:22 I have little to say here. Maybe I should rewatch the episode. 

