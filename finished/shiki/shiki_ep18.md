00:53 "It's good to have a job to do. In a way its what gives life its purpose", says the vampire as he buries corpses of the humans his kind killed. The cognitive dissonance here is huge. I can't really wrap my mind around it. In a way, acting as though burying the dead is normal, even happy work both normalizes the situation and makes it more horrible. It puts the viewer in a state of flux, where they are not able to decide where to align themselves. Are we the dead humans, or the living vampires?

02:14 The vines wrap around the bars, grow and die, held upright in a state of immortal death. 

05:12 We find out that our friend Cool is a wearwolf. He is rejecting who he is though, refusing to drink blood. The cognitve dissonance is continued here, with the haunted looks of Cool's father and Blue's horrible, rediculous outfit. With these character designes, when Shiki takes itself seriously, we can overlook the camp inherent in horror. What would be a campy scene of silly anger is flipped on its head and made into a scary scene due to the story. We dont take the visuals seriously, becuause we have been given no indication to, so nothing seems to be campy. But we take the characters and the story seriously. Its remarkable how this series does this. 

10:38 And the village elders have gone forward with the celebration. I understand why the doctor has given up on the village. The cut between the absurdity of the celebration and the doctors' betrayal emphsizes this. The differnce between his and the monk's betrayal is that while the monk is betraying humanity, he is true to himself. The doctor is betraying everything he stands for, everything he believes. 

14:07 I take it back, he betraying everyone. Humanity not 100% by choice though. 

21:17 This scene is beautifully done to make us feel for Femme. Her fear is conveyed through the impression of a huge cliff. She is helpless and crouded around. Her desire to see the festival is understandable. The doctor is mercyless and cruel. Someone else bit the Doctor and instructed him in ways different from Femme. 



