Ok, so there are a few threads here. The first, and the thing that I found the most compelling, is an exploraation of the awkardness and mundanity that traps us in daily life. The scenes of this always impressed me, as I've never seen anything like this in anime before. 

Another thread is the metaphor of the Kaiju, and Akane's desire to have control over her surroundings. She was unable to deal with the pain that everyday interactions caused her, and so wanted to make everything perfect. But of course, to really be happy, she had to not change the world around her, but rather change herself. 

I think there are some suggested thigns that were not explored in this anime. For instance, clearly Akane and the black haired girl have a history. It may even be becuase of this girl that Akane is the way she is. They were childhood friends, and something happened. That much was clear from the ed. 

Overall, half of it was enjoyable (the mundane half). The other half was meh (the Kaiju half). 
