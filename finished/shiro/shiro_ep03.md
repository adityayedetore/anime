## No More Recap Episodes!
### SHIROBAKO Episode 3

03:03 Efficent charcterization of the great artist as a put upon and calm and hardworking and focused person, and Ema as quiet and slightly awkward and in awe of her, and shows us that Ema looks up to this girl as a rolemodel, which tells us that Ema still has a ways to go before she becomes the best of the best. In the scene, the artist is asked to redraw some specific scenes, and though others says it will be difficult, she quickly sketches out exactly what they had in mind. Then Emi sees her working in her cubicle, approches, but knocks over a trashcan on the way, and apologises in a rush. The animator pays her no mind though. 

03:21 I am not exactly sure what Aoi's job is here, something like a manager/runner girl, but it is the exact position for the viewers of the anime to be able to better understand what goes into anime production. In this scene, we see Aoi listing all the things she has to do today, and at the same time this shows us how anime production works. 

17:53 Aoi is overworked, and worried that she won't be able to get the episode done on time. And now there is a problem that is compleatly out of her control. The FTP server is down, so they will not be able to upload the finished episode. 

21:22 "No one can make an anime by themselves. It's about teamwork". And we have seen this to be true in all the previous episodes, and this one especially. Thats why this doesn't ring hollow when one of the show characters explicitly says it. We see that all the characters are always talking, always on the phone. No one person can create an anime. 

### And Done!

Another great episode. I think I have to approach shirobako in a differnt way than I've approached other shows. The communication here is not about subtle or complex emotions, or subtle or complex ideas. What is being communicated here is the way in which anime is produced, and how characters navigate work life. The question I should be asking myself is "What does this tell me about how anime is produced?" Of course, there are questions about the caracter's feelings and beliefs and emotions, like any other show, but those are not very hidden from us. Generally, if a character has an emotion, the show will be obvious about it. 
