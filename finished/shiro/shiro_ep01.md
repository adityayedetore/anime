# Exodus to Tommorow!
## Shirobako Episode 1

00:00 The swear on donuts, not the most ceremonally serious of items. We are introduced to Ema, a introverted girl who is good at drawing. The dream is to move to Tokyo and for the 5 of them to make an anime together. Apparently Ema's partents do not want her to beome an anime artist. 

02:28 To convince her parents to allow her to go to animation school in Tokyo, Ema wrote them letters every day. She is not very good at speaking, apparently.

05:09 So far, this seems a bit like a slightly better version of Bakuman. I sure hope not. 

Hmm, seems to not be. Its a good show, Ill grant that. I think I would enjoy it, for the insights into the way that anime is made, and the character drama. I already like Aoi. But its not really what I want. I want complex, subtle writing that I can slowly peice apart, in order to improve my ability to pick up what the creators are putting down. This show conveys a carefully constructed story that allows all of its varied characters to shine. But I want a deeper look into a few charcters and themes, I want complex communicaiton that isn't always clear the first time through. Maybe I'll watch this show when I am feeling like enjoying myself, but for now, I think this gets a pass. 
