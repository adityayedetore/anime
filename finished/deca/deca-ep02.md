### Deca-Dence Episode 02 
### Sprocket 

Episode naming convention is parts of a machine. Last was called igniton, this is called sprocket. 

The reality of battle: pain and loss. 

Interesting that the soldiers are not respected for what they do by the common people. The real loss that comes with death is not the person themselves, but the relationships. It is not only the pain of a person dying, but the pain of the relationships being cut. 

And now is the reveal. Nice. Apparently the whole thing is a entertainment facility, "where one can experience entertainment like never before". This relates to the idea of those above seeing the people in this story as insects. But when you touch the scorpion, it stings back. 

There are gods that play these lives like they are in a video game. "Real simulation --death-- awaits". An exploration of the treatment of lives like they are disposable and meaningless. I guess maybe the bugs that the guy is collecting is the people that are playing this world like it is a game. Or maybe those genetically modified people are the ones who are playing the thing like it is a game. 

You know, this is pretty much exactly the sort of setup one could use to explore the relationship between storytelling and life. For this 'game' to be interesting to the players, there needs to be conflict. Having it be a game for some and real life or death for others allows an exploration of the similarities and differences between them. Will be able to bring up the point that people don't actually want to be in a story, because of the amount of pain and conflict. Hmm, living life without fear of death. 

Oh, lol, he's a player. Also this could have some commentary on the isekai anime. 

Hmm, having some of the characters on heaven, and others on earth. I wonder how they are going to play this. Are there going to be two storylines going on that effect eachother and intertwine, or is it something different? Maybe they are going to focus on the story on earth. 

Looks like they are going to two parallel plot threads way for now. 

Ok, so there is a way to have actual stakes for the players here. And it is tempting, because it allows them to power up. 

Ok, so this episode is going to be focused on Kaburagi, and why he has given up 'playing'. Before he was still serious, but he liked the reward of playing well, and was willing to do frivolous things just for the sake of pleasure. This world is one in which the higher ranked players are given amenities and whatnot. It seems like maybe they are being controlled too. So his place is being usurpted by nice guy. Maybe this will make him do something he regrets, or force him to consider the reason that he still does this. 

But there still is conflict for some in this world. This nice guy was just like everyone else: replaceable. But once he distinguished himself in the game, he became special. He's fighting against being insignificant. The people who break their limits are "scrapped as bugs". 

Hugin - ruler with laggan glasses. "The world must be rid of bugs". 
Murin - interesting ball face design

Nice Guy is caught cheating. The system punishes him and the people around him for it. Now Kaburagi has to work for the ruler to eliminate 'bugs', which I gather are the humans that find out about the truth of their world. "Both you and I are company property" 

So the eyes being like the facebook default avatars is not an accident, maybe. Maybe represents the fact that they are actual people, or that they have souls or something. Pretty ironic if that is the case though. 

Hmm, so it is intrrogating the biggest flaw of the totalitarian system: the possibility of uncovering the truth. Practically, it is very difficult to keep a truth like this from people, who are always curious and smart. And when they find that the truth has been hidden from them, they will be unhappy, and try to reveal the truth about the world. In this world, anyone who discovers the truth must be eliminated. So her stupidity is really saving her here. Ah, it's all the humans that are managed through chips. Ah, and in any system there will be those who fall through the cracks. A totalitarian system that is based on concealing the truth is so fragile, because it can break when just one person finds that truth. And since it is impossible to control everything, there will be people who fall through the cracks, and so are not properly taken care of by the system. 

Noice. 

Themes:

* The desire for conflict vs real conflict. 
* Fighting a system that views you as insignificant and replaceable. 
* Totalitarianism and the possibility of uncovering the truth
* The problems with a system that subjugates human life

Purpose: This episode showed the Heaven side of the plot, namely Kaburagi's side. This led up to the two plots converging, so now I think they will be more intertwined. 
