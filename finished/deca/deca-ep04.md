### Deca-Dence Episode 04 
### Transmission

Holy shit! "They treat this so differently, it is like a game to them" is what I was about to say, thinking that it is going to all fun and games. But then, blood. 

Hmm, heroes. They are able to inspire individuals. The existence of the higher level story, that of Kaburagi, makes this story very interesting. All these conflicts, they are like the conflicts of insects. One insect inspiring another. Wow, if they keep with this theme, I am really going to love this show. "Who told you that? If you are an insect, then these two are boogers" OMG that was a funny joke. But is it that being the main character gives her higher status? She is a scorpion, not the usual bug. So is this saying something about the ability to rise above through story? If so that is pretty meta, and pretty cool. 

That running animation is fantastic!!!! 

And yes, what we are seeing on the screen is just an advertisement for the next manufactured conflict of this game. This is literally when you put two insects in a box together to get them to fight. This switching between the two frames, it comments on the futility of these actions. That which is important for the bugs is just a passing fancy for those who watch it. It is also interesting that this is the way we look at stories and games as well. But when there is a story, we are sucked in, so that the world of the story becomes our world, and the people become real. 

Oh shit, they are meant to be sacrifices. This is even worse than putting insects in a box: this is putting in a box and pouring water in it to watch them drown, then letting others pick them out so that they can feel good about themselves. 

Oh, so one of the friends leaves her after seeing that she is dedicated to a losing cause, and the other sticks with her and supports her. The different reactions in the face of nihilistic existentialism. Oh, but he doesn't support her. He know even more clearly that this is a loosing battle, and so wants her to not fight. So the question is, why fight when there are overwhelming forces that are manipulating you? It is even worse: how do you let the ones you care about fight when you know that they are throwing away their lives? 

The blood and the milk. He is killing a dream. Omg, the huge light, and tiny people, they are bugs in the framing too. 

"What I wanted wasn't to change the world. What I wanted to change is myself". Yes. The motivations that we have for doing things are selfish and personal. That is why we will always rebel against being controlled, even if it is for our own sake. To be told that you cannot do something is a blow to the self. To struggle and try to break out of those bonds is something that someone, somewhere will always try to do. And sometimes, some will succeed. 
