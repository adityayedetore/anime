### Deca-Dence Episode 03 
###

Just rewatched the ending of the last episode, this is so good. We are touching on the importance of individuals to other individuals, and how a totalitarian, controlling system doesn't work well to serve individuals that fall through the cracks. Kaburagi was alienated from other people by the system, but now he has a connection, and something to live for. 

Hmm, this third episode was kinda disappointing. Not a terrible episode for any standards, since it was entertaining, and I do like that ending, but it seemed bereft of the thematic exploration that I saw in the last two episodes. Lets start with a summary, and then we can move on to the analysis. 

It started with a recap of the last episode. 
