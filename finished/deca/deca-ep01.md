### Deca-Dence Episode 01 
### Ignition 

This looks like an action anime. It'll probably be the first action anime I've watched in a long long time. Lets see how it is!

Kadokawa, hmm, they made Haruhi didn't they? Are they an animation studio? Broadcasting studio? I should look it up when I am done. 

A line of ants, broken by the excavation of something. Harmony broken by excavation of the past maybe? 

Live in a post apocaliptic world, where humans have to live underground. On the surface are monsters. 

She lost her arm, and probably her father, when desired to come to the surface to explore. How is this going to change her, I wonder? 

Linmei - Purple hair, green eyes. 

Fei - Brown hair, brown eyes. 

Natsume - Our protagonist, blue eyes, turmeric hair. Clearly she is not loving this life of supporting the fighters by butchering the monsters they kill. Hmm, I take it back, she would like to do anything to help the war effort. 

Currently her conflict is that people believe her to be useless with that arm. Her actual flaw, we'll see what it is. 

Ooh, born into a race of warriors. So there is a genetic based cast system here. 

Kaburagi - Black hair, green eyes. 

Those trucks look like ants. Maybe the smallness of human life? 

She's like a Miamori Aoi that isn't yet overworked and world-weary. I'd suspect that the theme of finding a reason to work is not going to be present for long, given that this is a action anime. 

The older hardboiled man, whose thin walls are melted by the younger one. Classic. Also she seems to not be interested in boys. 

That was pretty great. She does things physically without thinking, so she provides some physical humor. 

She wants to change things, has big ambitions. Natsume, that is. Also, her hair is like a scorpion. I wonder what that represents. 

That is one fucking ugly monster lol. 

We are learning things about the world, and about the characters. I mean, it was pretty clear that this guy was a big ol' softy from the start, but also they are setting the monsters up as possibly misunderstood. And that line about "what next after fighting, you are throwing away your life" maybe points to the theme of trying to find a reason to fight? Also we have learned that this Kaburagi is bionic in some way, or at least his eyesight is. Maybe his left eye, and that would be what connects him to Natsume, what with both of them having replacement parts. Also we are setting Natsume up as possibly being kind and wililng to change her mind about other creatures and people despite having strong emotions towards them. Pretty classic Shounen protagonist stuff here, but nicely articulated. 

Oh interesting, he is a spy of some sort, and his job is to remove "bugs". Ants, Scorpions, and now bugs. I wonder where they are going with this. But also this Kaburagi is not fully commited to this cause. Since he is a main character, that reluctance to do his job probably indicates that the job is morally wrong in some way. Also this Deca-Dence fortress looks like some wacky bug. 

Hmm, we have a white tower as the command room. 

Minato - leader of the combat effort. 

Kurenai - Blond hair, red eyes. 

Cool! The fighting style is so cool. 

Interesting that the blood that flows through the deca-dence is the same color as the blood of these monsters. Oh, so maybe this is that oxygen thing. And the monsters are coming to get it.

So, unsurprisingly, this dude is a fighter. 

So, they use the blood of these creatures as the fuel to fight them and to keep on trucking. A pretty unstable existance. Can't live with them, can't live without them. 

Oh, so there is an power that is controling/using the deca-dences for collecting the oxyone. They are computer based. 

So we have some symbolism here. Natsume is the scorpion, and the rest of the people are the ants. This suggests that they are thought of as less than human, and their emotions are not considered by those who are controlling them. Also there is this trying to find the bugs thing. A lot of threads are being set up. 


Themes: 

* Powerlessness of insects
* A reason to work/fight
* Genetic superiority

Purpose: Introduces us to the world, terminology, and the new characters. Sets up relationship between two main characters. Sets up plot threads. Done pretty well, I must say. 
