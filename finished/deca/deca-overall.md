### DECADENCE!!

What a fantastic show. This is the type of show that makes it clear that rating systems are meaningless. I could rate it for entertainment, ideas, animation, creativity, themes, structure... and none of these would be a definite number. 

I especially liked that scene right before the climactic battle. The 'system' was created in order to prevent devestation between humans, and it's philosophy is that everything that happens is 'part of the system'. Even the new 'system' that takes places after the totalitarian one is still a system, and it will have it's problems. This is true in a way. We need a 'system' in order to live as groups. But it is also clear that this is not the whole truth. There are systems that crush the individual, and others that allow it to be expressed. Even if there is always going to be system with problems, that does not mean that there will never be change for the better. 

I also like how the scorpion symbolism (is this symbolism? There is probably another word for this) is kept even in that final battle, along with the (loss of the) mechanical hand symbolizm. This is a smartly thought out and well put together show. It's the type of show that would be fruitful even on a second watch, I believe. 

Anyways, a great show, and I am glad that I finally watched it through. That ending was great. 
