### Chuunibyou demo Koi ga Shitai! Episode 03 
### The Heretical... Pigtailed Girl

Effectively reintroduces the central plot of the story, and sets up Yuta's sympathy for Rikka. 

Hmm, watching a show about embarrassment, this is a good time to think about the things that embarrass me. I am embarrassed by my one like for anime, and this is exactly the sort of anime that I watch that I could be embarrassed about. Hmm. What does the fact that I enjoy anime like this say about me? Well, I guess it shows that I like cute things. That's not too informative. The question is, would anyone get the wrong impression of me if they knew that I watch anime? Now that I think about it, maybe those stereotypes might fit me. A bit antisocial, few friends, no girlfriend, a bit perverted, etc. The only stereotype that doesn't fit me is the assumed immaturity. I guess the problem with telling people is the perception that anime is bad, and I consume bad things, so I am bad. That's the thing though: now that I think about it, They are not wrong. I mean, I don't cosplay or buy anime paraphernalia. Hmm, but do I dislike fanservice because I dislike it, or because I think that is what people object to in anime, and therefore I try to distance myself from it? I guess one of the reasons I dislike erotic fanservice is because it is too distracting, and seems like a cheep way to keep the viewer's attention. 

The Karate club dresses up and pretends to punch things that don't exist. 

This attempt to get Yuta to join the club is really cute, but I can't see what it does otherwise. 

What is going on. Hmm, she is trying to use her delusions to conquer a fear of heights. 

Ok, so this episode is about the fact that other clubs dress up and pretend just like Yuta is so embarrassed that he did, and as is looked down on in society, but they are not looked down upon. An internal and external double standard. Now that I think about it, it is an interesting choice that none of the classmates in this anime seems to give the Chuunibyous any grief for their weirdness. Yuta and Nibutani are the only ones, and they were Chuunibyou in the past, so their feelings on the subject are personal. 

What is this show doing other than being cute? Well, its being pretty cute, so I won't complain. 

Set up a false love triangle, just for kicks. 

This ending frames Rikka's Chuunibyou delusion conflict of internal identity, or at least suggests that she is struggling with her internal identity. This is pretty opaque as well. What does the apple symbolize? Why are there four apples? There are five members in the club. One strikes the other three, and they roll out of balance. What is going on here? 

Themes: 

* _Socially accepted/unnaccepted delusion_
* _Childhood imagination vs. Adolescent self conciousness._
* Fantasy as entertainment
* Self cringe
* Forgetting your past self
* Social acceptance
* Identity
* Fated love/soulmates (a little)
