### Chuunibyou demo Koi ga Shitai! Episode 08 
### Exiled... Just the two of them 

"Be destroyed real. Blow up, synapses. Banishment this world!" Literally, I reject the world, I am escaping into the world of fantasy. Hmm, there is an interesting line between fantasy and delusion. Are they any different? Hmm, maybe the difference is that when people engage in fantasizing, they do not believe that what they are imagining is real. But a delusion, they believe that is real. So in that sense, if we take this exclamation on the literal level, Rikka does not believe her fantasy to be real. this Chuunibyou delusion of Rikka's is not a delusion. But it is possible that she does believe in the fantasy. But is it bad to believe in a fantasy? As long as it doesn't make your life worse, I guess. The thing is, when you do believe in a fantasy, there is always the chance that something will go wrong because of that false belief. In general, false beliefs cause more friction with the world than true ones, because choices made on the basis of those beliefs may result in unpredicted and undesired outcomes. So, for Rikka, does she actually believe in her lie? And if so, is it making her life worse? 

"Acting this way, always being selfish like a child, is it fun?" I mean, she is a child, so... Also in what way is she being selfish? And now there is different physicality, one that is a lot more distressing. It is true that I did feel a bit uncomfortable with the easy physicality of Yuta, but being backhanded in frustration is not the same. Through this fight, Rikka has accepted that her house is gone. Or at least I think she did. But she still believes in the invisible boundary lines. 

Hmmm, now the people are on the same side in this opening. It is much easier to follow them. Is this an indication that Rikka now knows them better, and is more comfortable with them? But the scenes with Yuta and Rikka are still divided and flip back and forth. Interesting. 

So Yuta believes that Rikka is not in the grips of a delusion, but rather creating her own fantasy. "Its not running ways. It's not trying to turn away. But to think matter of fact words are enough to settle things matter of factly... Still, everyone says it is a matter of fact. They say 'That's reality.' But that is not enough." Basically, facing the facts is hard. This is the way that we are facing the facts, in our own way. It is that, or constant pain. Is the really enough? Hmm, both of these Chuunibyou have been shaped by loneliness. 

As always, trying to give others advice is the best way to learn.

Hmm, maybe this is about the normalcy of some of the things that anime makes to seem special. Like girls bathing, or dating. Did she not remember before that Yuta was the person who had shown her the Chuunibyou ways? That is better than her going for him because of their past, I think. So the function of fantasy is more than just personal escapism. It also allows others to escape. 

So she is so delusional that she mistakes the feelings that come with adolescence as excitement. Like that is a bit much, but this is anime, so it isn't real right. I mean I guess that people could kid themselves, denying that they they have a crush on someone else, but has anyone ever mistaken it for another emotion. And like is the word love or romance not in her vocabulary. This is definitely cute though. And I think it is more about what Yuta's reaction to this situation is. He believes that since she is Chuunibyou, there is no way that she could have a crush on someone else. That's also pretty stupid. I think denial due to "Why me, I suck, anyone who likes me is kidding themselves" is much more realistic. So currently they are just really good friends, but there are feelings of love developing. They haven't accepted their feeling for each-other, probably because they don't have much experience with this kind of thing. They are going through the motions of those who are dating, but in a close friends way.    

Themes: 

* Socially accepted/unnaccepted delusion
* Childhood imagination vs. Adolescent self conciousness.
* Fantasy as entertainment
* _Fantasy as escape (from loneliness/pain)_
* Self cringe
* Forgetting your past self
* Social acceptance
* Identity
* _Fated love/soulmates (a little)_
* _Stages of Grief_

This is getting to be quite the long list. These aren't being repeated enough to make them themes of the entire show. So far, thematically love has been a backdrop for the exploration of the social and personal uses of delusion/fantasy during the transition from childhood to adolescence. Oh but it's Yuta's perception of the childishness of Chuunibyou that makes him think that there is no way that Rikka is falling for him, and stops him from seeing her as someone that he could fall for. And it's Rikka's actual childishness that prevents her from realizing that she is in love with Yuta. So to accept Rikka would be to realize that despite the Chuunibyou, or rather regardless of it, one can be more than a child. 
