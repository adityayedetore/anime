## Inside Identity 
### Chuunibyou first ending theme 

INSIDE IDENTITY                               |  INSIDE IDENTITY 
Ibasho wa doko?                               |  Where do I belong?

Kanchigai ga hajida toka                      |  What is about misunderstanding is shame 
Sunaosa ga itai toka                          |  Or that being honest is painful 
Dare ga nanto iou to                          |  No matter what people will say to me 
Tadashisa nante wakannaize                    |  I don't care about what's right or not 
Futsuu ni nagarete iku nichijou ni            |  In this everyday ordinary life 
Murishite najimaseta                          |  I'm forced to fit in 
Shizunjimatta koseitachi wo                   |  I bite, drink, spit and laugh 
Kande nonde haite warau                       |  At those people who lost their individuality 
Konna boku wo wakatte hoshii                  |  I really want somebody to understand me 
Iitaikedo ienakutte                           |  I want to say something, but I can't say it 
Nande dare mo wakatte kunnai no to            |  Why is it that there's so many people 
Omou no wa zeitakuna no ka na? Na no ka naa?  |  Who cannot understand? Why?

Kanjoutekina taiyou wa                        |  This passionate sun 
Netsu wo age sugite dounika narisou           |  Somehow, considerably raises the heat to the maximum 
Sakebitakute shouganai                        |  I can't help but want to scream 
Gamushara ni motome teru IDENTITY             |  Recklessly seeking, IDENTITY 
aa (INSIDE MY FEELING  INSIDE MY JUSTICE      |  Ahh (INSIDE MY FEELING, INSIDE MY JUSTICE  
Jamashinaide ibasho wa doko? )                |  Don't be in the way, where do I belong?)
