## Sparkling Daydream
### Chuunibyou demo wa Koi ga Shitai! first opening lyrics 

(Let's look for Sparkling Daydream...)    |   (Let's look for Sparkling Daydream...)

Shibaraku mitsume atte kara               |   From the time our eyes met,
Sorasu made ni nani wo kangaeteta no?     |   to the time we looked away, what were you thinking?
...Ki ni naru                             |   ...I want to know.

Kuchibiru togaraseta watashi wo mite      |   As you looked at my scowling face,
"Doushita no?" tte                        |   I was waiting for you to ask me,
Kiite kuru no matte miru no               |   "What's wrong?"

Gomakashita ato no hitorigoto wa          |   After he brushed me off, I mumbled to myself.
Hazukashii kara                           |   Maybe he was just embarrassed,
Kikanaide kureru?                         |   and didn't ask?

Ato ni hikenai hodo                       |   After that, 
Hikare au koi                             |   a love I couldn't suppress,
Mou hajimatteta                           |   had already begun.

Yume nara takusan mita                    |   If it's in dreams, I've seen you a lot.
Sameta mama demo mada aitai               |   Now I'm awake, but I still want to meet you.
Kimi ga sousaseta                         |   You made me feel this way.
Koi wa yokubari dane                      |   Love is greedy, isn't it?

Tobihanesou na kokoro no                  |   As my leaping heart ticks,
Yuku mama ni yukou yo                     |   Let's go.
Risou mo mousou mo genjitsu mo            |   My ideals, delusions, and reality
Subete kimi o jiku ni mawaru              |   All revolve around you.
Atarashii sekai e                         |   Towards a new world.
