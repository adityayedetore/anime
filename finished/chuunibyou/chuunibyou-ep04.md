### Chuunibyou demo Koi ga Shitai! Episode 04 
### Regret of... the Mabinogion

Skillful buildup and release of expectations, the tension of humor. 

Every time Yuta is thinking of Nibutani, Rikka interrupts. Also the sort of fantasies that Yuta has, they are like delusions. In fact, his struggle to decide if Nibutani's alleged feelings are real or imaginary results in delusions themselves. 

Hmm, filled with gags. 

Ok, so this conflict. That which one hates, is loved by others. For Nibutani, accepting these people means accepting herself. Now he's just being mean. 

Seeing someone else with the same affliction allows you to look at it from an outside angle, and maybe shed some light on it. Now, the affliction isn't actually Chuunibyou, but shame. 

It is also a parody of enjoying things like this. Hmm, suspension of disbelief? Is there anything to the fact that when we watch art, we have to at some level pretend that we believe what is going on? 

Also dreaming is fantasy as well. 

