### Chuunibyou demo Koi ga Shitai! Episode 09 
### A Confused... Chaos Heart

This is a comment on the cultural festival attitude of the other anime.  

Hmmm. This on the surface seems pretty simple. Rikka has no experience with love, and so is unable to see that she is in love with Yuta. Hmm, I guess it might be here because most people are unable to admit to themselves that they are in love/have a crush on another person. So when they are thinking "Wow, that is so obvious, how can she not realize that she is in love with this dude?" we might be being ironic, since we do the same thing and are unable to self introspect to see the truth. Not a problem for me though, I'm an egotistical, self concious monster, and I'll readily admit that I _love_ (thinking about) myself. I do appreciate how cute this is though. Now she is using her Chuunibyou to guard herself against her strong feelings towards Yuta, but they are too powerful, and it is not only pain, but also pleasure. 

Ah, her knowledge of love comes from reading manga/some other form of equally imaginary entertainment. So this is definitely a comment on the delusions of love then, and how it interacts with the fantasies that we consume. Which is ironic, because in no way does this seem realistic. 

I didn't pay enough attention this episode. But next episode for sure. 
