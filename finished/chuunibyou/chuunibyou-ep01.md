### Chuunibyou demo Koi ga Shitai! Episode 01 
### Chance Encounter... with Wicked Lord Shingan

I've watched and re-watched this anime, recently even. I think I watched it a few months ago. But at that point, I hadn't started taking notes on what I was watching. Now, hopefully, I'll understand more of what is going on. I'm going to stick to just one episode today, and really try to get everything out of it that I possibly can. I am also going to write about theme, purpose, and characters at the end of the episode. I remember liking this show, though towards the end I became a little lost as to why things were happening the way that they were. I'm also just in the mood for a romance anime, so without futher ado, lets start the first episode of Chuunibyou demo Koi ga Shitai! or Love, Chunibyou, and other Delusions. 

The narrator of this intro frames Chuunibyou as affliction arising from a rising sense of self-awareness due to puberty combining with the whimsical fancy of teenagers. This positions those with Chuunibyou as on the cusp the transition from the carelessness of youth to the self-conciousness of teenagers. The "Found Footage" of this narration suggests that this is not a construct of the show, but rather a true affliction of real pubescent kids. It is saying that you, watcher, may be experiencing some Chuunibyou yourself. 

Introduction to the main character, as one who experienced a phase of Chuunibyou. His catchphrase, "Be enveloped in the flames of darkness and disappear!" is clearly one that pushes others away. His Chuunibyou is a self destructive one that inhibited his social relations. 

This overwhelming embarassment is something I can relate to. I'm sure many of the events I remember as embarrassing weren't actually so, but looking back on them, I realize what they would have looked like if anyone truly looked at me in those moments, and since I am looking at myself, I can't help but cringe. I've watched ContraPoint's Cringe many times, and to be honest I remember very little. I think self cringe is a moment when you realize how you look to others, and since it doesn't match your image of yourself, the cognitive dissonance is painful. In our protagonist's case, looking back at his Chuunibyou times he realizes how over the top and childish he must have seemed to others, and that conflict with his self image is the problem. 

Anime is full of angels dropping down from the sky to completely overturn the lives of our protagonists. Here is another one, and the scene recalls to me Castle in the Sky. The music is Gibli like, and that evokes a sense of wonder. Though the scene is slightly undercut by humor, it is definitely wonder inspiring. This is a important point in our characters lives, and the music and camera work and lighting make us know so. 

Oh yeah, this opening, I love it! I knew, but I had just forgotten how cute it was. Omg much cuter than I remembered. The side by side panels are disorientating, but in a good way. There is definitely something here about seeing only half of things at one time, and that relating to the eye patch that our Angel wears. 

Little bit of humor there. "How's this?" "Umm, normal?" Its normal? Sweet!" "Your weird". His attempt to be normal makes him weird. 

He is throwing away the remnants of his delusional ways. Trying to forget who he was. Hmm, his rejection of himself is driven by a desire to make friends, and to fit in. Hmm, they are both looking upwards at each-other. 

Ugh! Had to pause just to take the edge of this cringe. 

Ha! That was a reference to Attack on Titan! I think. 

Here we get our first reference to the invisible boundary lines. "Rather, that person doesn't exist." Here we are probably talking about her father. 

She knows how to look lost enough that people want to help her. Her sister is a part of the invisible boundary line administration be bureau. So she has control, or controls access to, the invisible boundary lines. She is asking for help to break through her sister's control, and find the invisible boundary line. 

That is probably a video game reference. Yep, it's the Konami code. 

He is very very physical with her. After just the first meeting, he playfully messes with her physically, doing things like putting her in a choke hold or the chop to the forehead. Interesting choice here. It's physical comedy, but it feels a bit strange. The childish side thinks it is cool to do the fancy stuff of Chuunibyou. Also, Takanashi is so quickly eating with their family. 

This show is not cynical of Chuunibyou. So when Rikka says that assigning special meaning to objects and making up fantasies have power, in the context of the show, they probably do. They definitely are a way to escape from a difficult reality, but are they anything more? 

I overlooked the ending before, but now I see that it asks the question "What is my Identity?" For someone with Chuunibyou, that is probably a fraught thing to think about. Is the persona we adopt to deal with the world our mask, or is it a part of ourselves? And if that persona is so different from how we would act usually, what does that make us? 

The title... It puts love on the same level as the delusions. This particular expression of what is going on in Chunnibyou may not be what the creators had in mind, so if I try to compare love to that I may draw a blank. But I still think it will be fruitful to think about why exactly love is a delusion, and what the other delusions in this show may be. Ill come back to this one. 

Themes:

* Self cringe
* Forgetting your past self
* Social acceptance 
* Identity 
* Fated love/soulmates (a little)

Purpose: This episode introduces us to our two main characters Takanashi Rikka and Togashi Yutta. It was very cute, set up their histories, and allowed them to interact with each-other. It also introduced a bunch of the side characters. 
