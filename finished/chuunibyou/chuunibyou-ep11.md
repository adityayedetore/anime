Since delusions are a large part of the joy in life, trying to get rid of all of them is painful. And it is impossible to tell what is a delusion and what is real, and so its easy to go overboard throwing things away. Like we might end up throwing away things like love and passion, and what makes us interesting. Specifically, she no longer uses sparkling lights as a symbol for her father watching over her. And she is no longer able to see the beauty in it. 

There is a thread of responsibility here to. To grow up and to make adult decisions is to get rid of childish flights of fancy, and to not encourage them in others. But that sucks the joy out of life. 

Also about paternalism. "My parents think it for the best, so it must be right, right?" 

This last episode got pretty specific about the themes. Thats good, I think, especially since the ways in which the mundane world is filled with Chuunibyou is pretty subtle in the show. Anyways, great show 9/10. 
