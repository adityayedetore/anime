### Chuunibyou demo Koi ga Shitai! Episode 07 
### Reminiscences... Of Paradise Lost 

The dialogue to start this off lets us know that these two have gotten pretty comfortable with each-other. 

Nice job building relationships. That hand scene was pretty good. Also, looking out a train window, except not because you have a covering over your eye. 

Basically, this house reminds her of her father. And it was never her home, and she never felt loved here. 

This reveal recontextualizes the search for the barrier as a search for Rikka's passed father.

Ok, so unlike Rikka's sister, and her grandparents, Yuta supports her. He says, "we can rebel against them, and find those invisible boundary lines". At the same time, he is acting as his former Chuunibyou self. This shows that he is supporting her delusions. 

"This is reality. Papa isn't here. This is the present." They in fact hid reality from her, kept her in a delusion when they didn't tell her about her father's coming death. Likewise, because they didn't tell her that the house was taken away as well, they kept her in a delusion. So they are telling her, "Face the truth", but they aren't allowing her to do so. Moreover, her Chuunibyou is the way that she is coping with this. What is a little delusion if it allows her to deal with her grief? Hmm, maybe this isn't true. She doesn't seem to have made it out of the denial stage. 

Themes:

* Socially accepted/unnaccepted delusion
* Childhood imagination vs. Adolescent self conciousness.
* Fantasy as entertainment
* _Fantasy as escape_
* Self cringe
* Forgetting your past self
* Social acceptance
* Identity
* Fated love/soulmates (a little)
* _Stages of Grief _
