### Chuunibyou demo Koi ga Shitai! Episode 05 
### A binding... Hard Study

I think at this point I'm not getting much out of the thematic side of this anime. Most of the scenes are almost completely devoted to a cute humor, or building the relationship between the two, and not to thematic complexity. There are some things here, like accepting both your adolescence and childhood desires, and a subtle exploration of the ways in which even our socially accepted forms of expression involve fantastical delusions. The brunt of it is humor, though. I think I should analyze the humor and how it does what it does. 

Ok I have no I idea why it is so cute or so funny, but it just is I guess. 

Ah, so the setup of the threat of the club disbanding allows Yuta the reason that he is staying the in the club in the first place. Now that he has found that Nibutani is not the person that he thought she was, that part of his concious reasoning for joining the club is no longer present. It is possible that that was the only reason that he eventually did join the club, but now there are other reasons, like his friendship with Rikka. So to accept the club would be to acknowledge either that he enjoys doing the Chuunibyou fantastical stuff, or that he stays because of Rikka. Hmmm, whenever there part of the conversation or a scene where things about the machinery of the plot or characters is revealed, it is quickly passed by a punch of humor or distraction. This allows the show to keep a light and airy tone even when there are introspective elements, which are not the most conducive to humor.  

Lol whenever Yuta is thinking about or looking at Nibutani Rikka gets in the way. Jealous, but in a friend way for now I think. 

She says "I'm lonely" and he's like ok I want this club to continue b/c I care about you and so I don't want you to be lonely, so he's gunna teach her math for real. 

More fantasies this episode: playing Grown ups with children, and sleeping. 


