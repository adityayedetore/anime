### Chuunibyou demo Koi ga Shitai! Episode 02 
### Priestess.. of the Melody 

That opening sequence effectively portrays Rikka's sister as an overpowering, terrifying force of evil. 

Those words, "Lets look for that sparkling daydream", as the horizon turns vertical. 

Embarrassing dreams, delusions, but they are thought of as normal. These dreams come from the same mix of childish fantasy and adolescent conciousness as Chuunibyou. 

She heard him talking about the cheerleader in his sleep, and so sprayed him to wake him up. And also what is she doing in his room in the morning. 

"Incidently, where is your father?" ugh the feels. 

If anything, I'm the type that doesn't stand out. That is who he is now, but not who he was. Or rather, that is who he is trying to be. When she says that they are similar, what she means is that they are repentant Chuunibyous. "The dawn of a fruitful, offline life?" you still have some of those tendencies, a penchant for the dramatic fantasies. 

That is a cute scene. Also, a dead body with one eye? Is that her father? And why does medicine make her think of her sister? 

"Are you childhood friends?" Hmm, this straddles the border of showing and telling. Yes, they are acting like childhood friends, but her commenting on the fact makes the comparison so much stronger. That in interesting. I think in another show that might have felt ham handed, but here it just felt like a natural conversation. 

Hmm, are stuffed animals delusions. And are fake weapons delusion? So anything that is false, but you pretend that it is real. Hmm. 

This animation is fantastic. 

This scene is so funny! But "This is recorded from two years ago" ? What? Why was her sister here two years ago? 

"The end of the earth, meaning here? I mean, planet earth is ROUND!" Wha? What happened to your invisible boundary lines? I thought those were the horizon? 

He might have rejected his ways, but some of the things he loved, they still call to him. Like the model guns. Yes, we assign meaning that they don't have and make fantasies of them, but that is not a bad thing, is it? 

Themes:

* _Childhood imagination vs. Adolescent self conciousness._
* _Pretending objects have significance_
* Self cringe
* Forgetting your past self
* Social acceptance
* Identity
* Fated love/soulmates (a little)

Purpose: honestly I watched this yesterday, so I don't remember much of it's purpose. There was definitely a lot of relationship building for Rikka and the Yuta, like introducing them to eachother's families. Also make Yuta realize a bit that Rikka actually is cool. 
