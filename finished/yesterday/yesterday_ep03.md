## What is love? 

### Sing "Yesterday" to Me episode 3

Interesting how Haru has teenage qualities about her, in her exuberance and way she goes about life, but also seems to see into things with an adult's eyes. She realizes that sneaking around Shinako, without addressing those problems would result in pain for everyone. Her declaration of war, very childish, but also probably an important thing to do.

The fact that she has a motorbike is a symbol for how she exists on the border of two worlds. She only needs a motorbike because she has to provide for herself, but she it is a child's bike, not a car or something else. 

When Haru goes home, we see the crow on one foot, and then we see Haru on one foot, all dressed in black. Her mother has shown her a new father of hers. Just like the crow's foot is broken, it Haru's home is broken. When she talks to the coworker in the bar, we can feel the pain hiding beneath her words. Haru does mind her coworker asking, and doesn't want to talk about it. 

Haru wants her mother to care about her, to want her to come home, to want her to go by her name. But just like Shinako has lost her love, Haru has lost her father, and so lost her family. 

Ok so the Sakura plants are a symbol for something for Shinako. Generally they symbolize youth, and beauty, and love. But to Shinako, they symbolize death and loss. She can no longer see the beauty in them, and can no longer love anyone but the one she's lost. 

This was Haru's episode. Probably because of her lack of a family relationship, and her father's death, and other things, she has always kept people distant from her. It's the porcupine's dilemma, though not as extreem. She is really worried when Uizoumi doesn't show up. Has she been left waiting like that before? Is that how her father died? 

I like that both of the female leads have problems, but don't expect those problems to be solved by the main character, and the main character has no savior complex, no desire to help them. He loves Shinako for her strength, and sees no reason to 'save' her. He's really not like Araragi there. 

Haru has taken the first steps to change herself. She wants to run away from being hurt, doesn't share a close relationship with her mother for the possibility of pain, even though she wants that close relationship. She didn't tell Uizoumi her phone number so that she could always break the relationship off at any point. But by never allowing others to come close, she was isolating herself. But by talking to Uizoumi, she took the first step to changing herself. By sharing the information about herself, she has taken another step. She is very much unlike Shinako, who is unable to get over the pain of the past. Haru is struggling with it, and starting to conquer it. 

Oh shit the title of the episode is a reference to the song. Lol. Haru doesn't want to be hurt no more. 
