### The Man Named Minato
### Listeners Episode 5

Our protagonist has gotten a job in a photography gallery. His dream is to be a photographer, so this is one step in the right direction. 

The titular minato. He is from Haru's year in high school. What does this mean? 

This is a serious photographer, with strong opinions. Is he going to be able to get a rise out of Uizoumi? Is our protagonist going to have to defend what he likes, and thus realize how much it means to him?

Shinako is drinking, and that is not like her.  "Did something happen?" "It's not like that." But something did happen. Shinako was finally able to cry because of Rou's brother's death. She might have just accepted his passing. If that is the case, the this may be monumental for her. Uizoumi is not aware of any of this, though. "It's just that after summer ends, there is fall. Then winter comes, and then there is spring. I just started thinking about that." Has she broken out of the cycle of thinking about the past, and is now looking towards the future? Before she was living in the eternal spring of youth, with the sakura petals that always remind her of her love's death hanging over her. But now... she has been able to accept his death. Has she? 

"In other words, I've stopped going around in circles." Oh my god, she has. That's awesome. I'm surprised how much I care about her. Does Uizoumi recognize the significance of this? 

Ah this is a showdown. Pull out your gun Uizoumi! Take the shot! Take it! Uizoumi is getting annoyed over his passion being insulted by this Minato, and I thought that he would be moved to words by that. But if he is affected by Haru being with Minato, all the better! He is starting to like her, if only a little. 

Ah I'm starting to feel bad for this Minato. He's not a bad guy at all, it's just that Haru can only look at Uizoumi.

This dude is basically Uizoumi with his shit more together. Uizoumi didn't pay attention in school, and probably didn't learn much. Minato dropped out, because wasn't interested in school. They both love photography. They even look similar. 


