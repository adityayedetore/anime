### Sing "Yesterday" to me episode 9
### A Christmas Carol

Interesting intro music. Should I prepare for comedy? Awkwardness? Its the same scene from the end of the last episode. Shinako, what are you doing. Maybe you really want people to continue to like you, despite you not liking them back. I understand the feeling, but that is going to make your life a lot more difficult. 

Ok that music is back, with his apology. "I say what I mean, and it shows in my attitude. And you end up having to accommodate." Basically saying here that the reason that he did what he is apologizing for is that he is a (horny) teenager. 

I guess it's pretty difficult to get over your own nature. In like episode 3 Haru realized that she has difficulty with close relationships because she doesn't want to get hurt. Here she is having the same problem again. 

Do Haru's boots suggest that she is always prepared for the rain?

I think it might show a little bit of progress that Shinako is able to think of Yuu and smile. But she is also still stuck in the past. Also there is probably some importance to the ginko nuts that Shinako has picked out, but as I am not japanese I don't know what that is. 

I am not a fan of this voiceover segment. Ok, but he is looking at pictures. Maybe these panels in the show are relating memories to pictures. He stopped taking pictures of Shinako because it started to feel awkward once he started liking her. 

Wow, that smile is pretty good. Rikuo, you don't need either of them. Be your best self on your own. 

What is she, 15? Maybe since she never allowed herself to have feelings for others, she is feeling them belatedly. 


