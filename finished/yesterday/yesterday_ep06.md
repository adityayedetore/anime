### The Woman Named Yuzuhara
### Sing "Yesterday" to me episode 6

The title of this episode is a throwback to the title of the last episode. In episode 5, we saw a man who had a lot going for him, and who Haru could have loved in another world. That man was a reflection of our protagonist. 

Just like Minato was Haru's classmate, Yuzuhara was a classmate of Uizoumi. But they had some sort of relationship beforehand. She has lived in this town for a while, but has died her hair, so Uizoumi doesn't recognize her. Maybe more than just her hair has changed.

That conversation speaks a lot to the history that Uizoumi and Shinako have together. He helps her set up her tv every time she moves? That is a history that he and Haru, and he and Yuzuhara don't have together.

Is this anything more than a conversation? It shows that Uizoumi is pretty soft, but other than that, I don't know what.  

Interesting, the sakura plant in Shinako's house has lost it's flowers, and is now spouting new leaves. Is this another indication of the fact that Shinako was able to get over her dead love? 

Oh no, Yuzuhara, that is so sad. She is willing to use her body to pay for staying in Uizoumi's house. Thats horrible. Did something happen to her? 

They don't know the first things about each-other. They really never were close, and even talking about things close to their heart is awkward and painful. But starting to learn  these things about each-other is where it begins. 

Those faces are great. 

Hm, anger, jelousy, is there anything deeper here? Also, what actually is so great about Uizoumi lol. 

Shit, "I have a hard time saying no when someone says they like me" no no no, I'm so sorry Yuzuhara. 

Possibilities, possibilities. They make me so sad. I really like this girl. It's possible that this is how it always goes for her. 

"He's just a nice guy. I was using him." Yuzuhara's words here cause Shinako reflect on her relationship with Uizoumi. She had been thinking to herself, "Am I just using him?" and now she sees someone who actually is using him. 
