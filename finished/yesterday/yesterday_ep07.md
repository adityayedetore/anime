### SING "YESTERDAY" FOR ME Episode 7 – Premonition of a Couple

Rikuo and Haru are on a date. He asked her out to dinner. Oh wait, no, she is forcing him to come, and also forcing him to pay. Lol. 

Rikuo has started to work at a studio, or will in the near future. Good for him. 

What a dude. Rou talks to his friend about how he is too comfortable right now, and everything is going his way. So he decides to make some conflict with Shinako. He asks her out on a date, gets rejected, and I'm guessing is about to throw a hissy fit. 

Ah, well, I guess I shouldn't have been reading mage in a barrel blog on this show. Now that Rou's childishness has been pointed out, it really is a bit painful to watch. 

The camera is pushing the pair of them to the edge of the frame here. Why is it doing that? "Rou is falling for an illusion". "You are being overprotective of him. He is not as much of a child as you think he is". Rikuo is saying that Shinako should do to Rou what she did to him. She should flat out reject him. Maybe Rikuo doesn't understand that Rou actually is a child? Maybe he is right? Or maybe he is just lying on purpose, to make Shinako feel better. I like that theory, especially given his shifty eyes at the end.

So what she does is give him hope? Why lol. Thats exactly the opposite of what Rikuo asked her to do. 

I like that! He asks his friends about what Shinako means by what she said, and they say he was rejected. 

The work in the two places are symbols of accepting the chance of failing and self imposed stagnation. To fully work at the photo shop would be a pretty big step in the right direction. 

He is smiling. I don't think this is going to turn out well for him. 

Hmm, his first job with the camera is thematically intertwined with the idea of marriage. Accepting his passions and turning them into a job is being related to moving on in life. And moving on in life means getting married, and having kids. 

Cute. He wanted to ask Shinako if she wanted to get married at some point, but then realized how that would sound. 

Ah, unsurprisingly she hasn't thought about it, or even thinks she doesn't want to get married. 

Interesting. Before, when she would recognzie that Rou is like his brother, she would make a comment or joke about it. But now she starts crying. That, oddly enough, is positive development, given that she wasn't able to cry over Rou's brother's death before. 

Rou, wat are you doin. You really are not very good at this, are you. 

Lmao, she is at Rikuo's house again. "It's about Rou." "No, that's not what I came to talk about. That's not what I don't know how to deal with". Well, given the camera here, I'd guess that what Shinako doesn't know how to deal with are her growing feelings for Rikuo. But how is that connected to the last scene? Does she feel like she relies on Rikuo too much to simply break off their relationship, but her growing feelings are becoming a problem? This scene was bookended by the moon being covered by clouds. Maybe that represents a troubled heart. 

"I've been looking to a dead person for an excuse to run away. I've been rationalizing things in my head so that I don't have to come up with answers." I mean, I really don't know what was going on in your head before, but it seems to me that you are wrong about this. Yes, you may have been running away from relationships, but you were also experiencing overwhelming grief. Now that you have started move past the first stage of that grief, you seem unable to remember how empty you felt before. You are wondering how you were missing your feelings towards Rikuo. But you weren't ready for those feelings yet. It's not your fault. 

Ok, so Shinako has just told Rikuo that she wants a relationship with him. She freeking invited him into her house. You are making things real hard for Rikuo right now, Shinako. I think he has gotten over the rejection, and gotten over his strong feelings towards you. And now you choose to let him into your heart. Not that I blame you, he is pretty nice to you.

They changed the ending theme. They changed it to a new game, lol. Not being very subtle here, are they. And it's Haru, who has apparently seen them heading into Shinako's apartment together. A cute way of depicting it, but not really the most emotionally resonant. 
