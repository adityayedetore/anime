### SING "YESTERDAY" FOR ME Episode 8 – Innocent Blue


Still going with that moon theme from last episode, are we. And... Shinako chickens out. I think this is better, a bit more drama for the show. If he entered the room then, the show would be over, I think. 

Shinako now drinks, and talks to her girlfriends about this. Nice. 

I love this shot of Rikuo sitting with his head in his hands. 

Ok, thematically, I think there should be a link between the reason that Rikuo and Shinako aren't together, and the problems that Rikuo are facing. That way, to overcome those problems would enable him to be with Shinako. Rikuo's problem in this relationship is that he is not being pushy. Shinako's problem is that she is comparing her feelings for Rikuo to her feelings for Yuu. Rikuo's problems with his job life is that he is not being pushy (sorta, but he is still succeeding with regards to the job stuff, so I dunno). 

Rikuo thinks that the reason that he is so passionate about his relationship with Shinako is that he still hasn't caught up to her in terms of work. She has a stable job that she loves, while for a long time Rikuo was wallowing the stagnation of the convenient store florescent lights. But that is not the case. It is rather because he has slowly fallen out of love with her. Haru, on te other hand, he is slowly caring more about. 

"He takes forever to make up his mind, but when he does, he goes ahead without asking anyone's opinion or anything. I wonder if he will be able to get by with just the studio job." "Maybe he has decided that he can." When she says this, does she realize that Rikuo may have decided that he can get by without meeting her? That he has decided to move on to greener pastures? 

This awkwardness is certianly making me smile, but its really not that thematically satisfying. Unless I'm missing some stuff, of course. Shinako has developed feelings for Rikuo, but he has slightly fallen out of love with her. He has now become passionate about his work, and also believes that doing well there will re-spark his love for Shinako. 

When Rikuo says, "I have realized that I love playing around with cameras", that is what Shinako feared. She believed that he has found a new love, and it seems like he has. And also she is jealous of Nonaka Haru.

So he is telling Haru that he doesn't want a relationship with her anymore? Ok, I mean, I don't really feel the tension, since there are like 10 more episodes in this show. He is telling her that he is choosing work over her. Why exactly is he doing this? 

Lmao, same reaction to a noise. Is Shinako going to get dumped too? That would definitely be funny. 
