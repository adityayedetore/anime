## As the river flows, shinako returns home
### Sing "Yesterday" for me episode 4

Everyone's got their own struggles, and it seems that blue eyes is having a tiny bit of difficulty in school. He is feeling slightly out of his element, with the students around him being much more skilled than he is. But he is doing what our protagonist wasn't ever able to do: pursuing his passion, art, in college, with the ideal to make that his livelihood. 

I can't help but feel that the translation isn't very good here. I can't understand the brunt of this conversation, even with repeating it. 

Ah, what a good conversation. And the shot too, the fact that this yuu is alone and off center in the frame indicates a emptyness, the absence of a person. And that person was his brother, who is still the "170 cm" that he was when he died. Shinako is with him, but really this only emphesisis how alone he is. 

His story is that he was less skilled than his brother, but drawing was the only thing that he was better than his brother at. So his desire to draw stemmed from his desire to get out of his brother's shadow. His brother was always at the center of attention, not only because of his skill but also because he was sickly and needed to be taken care of. Now that his brother is dead, why does Rou continue to draw? 

Rou is saying "I love you, I don't want you to look at me and think of my brother." 

There is an object between them, but they almost look like mirror images of each-other. An quick insult from Rou, and that distance between them is broken. Ah, they really are more like each-other than they think. So is Rou doomed to work in a convience store? It makes sense that Shinako thinks of our protagonist as a brother. 

Ok, well that is significant. She wasn't able to cry the day that Rou's brother died, because she really never accepted that he left. Why is she able to cry now? Has she finally move on? What does this mean for her and our protagonist's relationship? 


