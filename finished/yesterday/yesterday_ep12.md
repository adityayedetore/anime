### Sing Yesterday for me Episode 12
### The long way

How does keeping this a secret play into their character development. 

As always, shinako runs after rou, leaving Rikuo behind. 

They both realize that the (familial?) feelings they have to the respective young people in their lives is greater than the feelings they have for each other. 

This is pretty simplistic. I think that the most interesting parts of this show were when the characters had internal problems that they had to overcome. Those character arcs were done by the 7th episode though. After that, the conflicts turned into the characters coming to terms with their own desires and feelings for each other, and that part I was much less interested in. Everyone got a character arc but Rou. Uizoumi got past his non-committal attitude, Shinako accepted her love's passing, and Haru stopped running from the possibility of pain when relationships break. Rikuo was able to get a job doing the thing he loves, Shinako moved past her grief, and Haru reunited with her family. They were able to get the things they need as they realized more about themselves. But this was more of a byproduct of their relationships with each other than the cause of those relationships.

Its interesting that one of the reasons I liked this anime in the beginning is that the characters didn't rely on each other to solve their problems. Rikuo was no araragi, and he didn't feel any compulsion to save Haru or Shinako. But the fact that the character arcs were divorced from the relationships meant when those character arcs were over, there were only the relationship troubles left. I think I was looking for more character development after that point, which means that was looking for something that was not there. That probably prevented me from enjoying the relationships themselves, though I'd say only 3/4 of those relationships were enjoyable. Rikuo x Haru, Rikuo x Shinako, and Haru x Shinako were all fine. Shinako x rou was ok, though Rou's childishness was a bit much compared to the maturity of the other characters. 

