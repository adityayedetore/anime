### Sing "Yesterday" to me episode 1

"In this town, the crows wake up before the people". Crows are scavengers, birds, intelligent, symbolize death and freedom. Our protagonist feeds them extra food from the store he works in . He has just graduated college, and is working a part time job. Nice guy. The girl's name is Haru. She seems carefree and happy. "You can think of me as a crow" she says. 

There seems to be purposeful framing of memories as in manga like panels. A past that seems like a story, not real. 

Our protagonist doesn't want the responsibility that comes with a real job. He doesn't want to commit to things. Is that girl that he saw in the memory panels his ghost? She left him because now that they have started working, they can't be together. "Just friends", it seems. 

Is no one going to comment about the crow sitting on Haru's shoulder? Ah ok good our protagonist mentions it. So Shinako represents the realities of accepting work life, Haru the happy naiveté of youth. 
"I can't compete with her ... I think that might be why I like her" I don't think you are completely correct here, but it sure is interesting that you think this way. You are saying that you like her because since she is so put together, you can just give up and not compete with her. That shows how much you have given up on life my dude. 

He doesn't want to get a job in his passion because that will turn it from something he loves into work. Also he doesn't think he is that good, so he has given up. Its a feeling of listlessness. 

Ok he met her 5 years ago? That's a bit lame. Hmm so Haru and Shinako know eachother? Shinako is her teacher? So the crow on her shoulder is a symbol of hard times past. Ok so this is about taking care of eachother, and caring for eachother. 

I really like this dude.

Ah, so the manga like shots from before are a reference to his photography hobby. Those were captured memories from the past, but these are new memories. 

"People hate liars, but people like them. You lie, you play along with what people say, and live a BS life. You have nothing to hurt people or be hurt by. You become worthless. And so you become sick of being with other people." Pretty on the dot here Haru. But you're wrong that you become worthless. I mean, I'm not worthless, right? Right? 

An anime about transitioning from being a young adult to being an adult, about memories, about being afraid to follow your passion, about characters who know that they have to change and yet have difficulty doing so. 
