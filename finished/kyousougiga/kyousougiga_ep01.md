### Kyousougiga Episode 1 – A Family's Circumstances and Its Background

Well, I spent too long trying to figure out if this is the show I should be watching, instead of just watching the darn thing. Recommended by Bobduh, I am confident that it will be good. After I watch, I am going to try to compare notes with Bobduh, and see where the differences are. 

Well, that intro is definitely intriguing. A Sensei who longs not for the future, which is uncertain, nor the present, which is chaotic, but for the past, which was relaxed and filled with golden fields of rice, and the smiles of children. 

The girl's name is Koto. She asks if the past he remembers was just a dream. Sensei gives Koto a marble containing the stars, which "Shines and so is divine, perfect for you". So the stars are a reflection of the past, and are also the divine. 

Priest Myoue. Can create objects by drawing them. Sort of god-like if you ask me. The villagers are rightly scared though. He created a black rabbit named Koto to be the god of Kyoto. This rabbit falls in love with him.

God: My eyes exist to see the future, and cannot see the past. Gives body to Koto. Myoue falls in love with Koto. They have one child, and he creates two more. Oh, the child is last. And the second creation is a demon in the form of a young girl with blond hair. Loving family. 

Chased away, leave peacefully to the world he created, the world that Koto is the god of and Myoue created. It is a world in which there is no strife, nothing is born and nothing dies. It is a true wonderland. 

But lurking in the shadows, in the corners of Koto's dreams, is something coming for them, to destroy them if Koto continues to stay alive. But Koto and Myoue's love is stronger than their fear. So they leave, with a promise to come back. 

And he has grown up, looks like his father, and is annoyed by the constant texts he is getting. He is unhappy with the responsibility he has been given. His father's promise is that when he comes back he will bring the end and the beginning. 

Koto comes with a giant hammer that contains the same colors as the orb given to her by Sensei. She works on behalf of the shrine to find the other Koto. She has come to find her guardian. 

"Once, when many planets intermingled and the boundary between man and god grew vague... this is a tale of a particular family, a tale of love and rebirth." 
