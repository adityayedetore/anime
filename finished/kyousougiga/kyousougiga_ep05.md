### Episode 5 – The Present (8) / Priest Myoue A

Based on this episode title, it looks like my theory that Yasu...

Ok I need to get these character straight: 
    Koto - the mother 
    Myoue - the father 
    Kumaru - the first child
    Yase - the second child, a demon 
    Yakushikumaru - the third child
    Koto - the young sister with the big hammar
    Inari - Koto's teacher, also probably her father

Anyways, what I was saying was that based on the title of the episode, it looks like my theory that Yase, Yaku, and Kumaru are haunted by the past, present and future respectivly is on the right track to some degree.

"True to your station in live" I do not seek salvation in the next life. I merely strive to be true to my station in this life. The time to take responsability and act is now, I guess. 

After his mother and father left, Yaku tries to commit suicide. But he lives in a world without death. 

Now he has little sisters to take care of. He had this dream of the past, but his current life is so far removed from that world of pain, blood, and fire. What happened in between? What made that child the person that we see today? 

Ok, so they are all waiting for their parents to come back. Yase's trouble is that she believes that what is lost is gone forever, so she doesn't want to loose anything. Yaku is constantly hoping and believing that they will be back, but is also always betrayed. And Kumaru... I'm not sure. 

Maybe he has dissociative identity disorder. He just reverted to a childhood angry state without realizing what he is doing. 

He hasn't gotten angry in a long time. He'd given up on the anger, and is now just stuck with the hope. 

Those beads are in the form of DNA. Its inheretance, literally. Ok, so there is responsability that comes with blood. Yaku had to take up that responsability before he was ready for it. It binds him, and can never be released, just like those beads on his hand. "I'll entrust Priest Myoue to you before I go". Sounds like he entrusted Yaku with a literal person there. It's a symbol for inheratance and blood relations, surely. 

The dog that is always waiting for it's master. Yase believes that the dog is just stupid, since it never searches. Kumaru believes that the dog is waiting to die. And by dog, of course, I mean Yaku. 

She is traveling the wrong way? Is what she searches for behind her? 

Yaku doesn't want the responsability that was entrusted to him by his blood, but taking care of Koto would be part of that responsability. So to accept her would be to take the first steps to rid himself of his problem.

The train represents something that is stuck on its tracks, going in circles. Yaku waits for the train to bring his parents, but it is filled with things that have been abandoned and forgotten. The one thing that shines in that dark place is the pomegranate. 

And this is the field is the field that Inari talked about in the opening of the anime. 

Koto, who bears the name of his mother and the eyes of his father, brought 'the beginning and the end' with her. 

The winter in Yaku's heart has turned to spring.

I love how Yaku uses the symbol of his inheretance binding him only to discipline Koto. 

The pomegranate represents blood and rebirth, and Yaku gives it to his sister. 

Oh fuck, he's adopted!! Wha? Maybe Yaku feels he doesn't deserve the inheritance? Saying to Koto that the rabbit is their mother is acceptance of his parents. And he tried to commit suicide because is parent burned in a fire. 

Oh, but his problems run deeper than just anger at his adoptive parents and the responsability that his father left him. He is still in other kinds of pain. After all this time, he still believes that it is death that he wants. 
