Kyousougiga Episode 4 – The Second Daughter and Her Wonderful Monsters

I just love this opening so much. The last episode we learned about the first child. He is the real decision maker here. He is the reason that the family was found out. He is willing to disobey rules for his own sake. He is very smart. This episode is about the second child. 

What a fucking asshole. 

Ok, so they represent differnt ways of being stuck. Yase is stuck in the past, thinking that once the things that are connected to your memories are gone, they can never be recovered. Kumaru A 


I'm re-watching this ep. 

From this opening sequence and the voiceover, it is clear that this is an anime about finding family. 

Something tragic happened here, more than just the family fracturing. We have sometimes been seeing flashes of blood, and the reflection of Yakushimaru in a silver blade. In the op, at this image the scene seems to shatter, like glass shattering. This probably represents the fractured family, in addition to other things. 

The title of the second episode is "the little sister arrives" so clearly Koto is the little sister. 

Its called the station opening. Like a train station, which is the classic place of saying goodbye. 

We see a family of birds sitting on a telephone wire. Though this is the day of the station opening, they are together. But Myoue's family is not together. There are other scenes of people, some of them on their own, but the last is a father with his children, as they celebrate the fact that people the unused stuff is flying away. It is interesting to think that the this world in which nothing ever breaks, so people are forced to throw away extras. 

The three of them are together, but not really. 

The stuff wont be fixed if it is broken with the hammer. Interesting. In a world of stagnation, only Koto is able to bring about change. But here change is not life, but instead the breaking of it. She is like the god of distruction, come to finally put an end to this world. Also, at the same time, Koto is acting like a younger sister. Going around, breaking things, needing to be deciplined, asking "why" all the time. 

We can compare Yase's reaction to Kumaru's. He doesn't care about the cups, or what they stand for. In the beginning, without context, it seems that Yase's anger and reaction is childish. That is what we have seen of her so far. The thought that is in our head is that she is probably just a foolish person. But of course, that is not the case. We can tell from the way that she reaches for the cup that she has reached for it there many times. 

Of course, she does the one thing that Yakushimaru told her not to do, and goes to visit Yase.

It is interesting that all the stuff in this world instantly heal, but the people, or at least Yasu's monsters, do not.  

After loosing her mother, Yase is unwilling to lose anything else, no matter how small or insignificant. Given that, the fact that she lost her cup must be devistating. But really, that cup and all her stuff are keeping her in the past, unable to move on. She doesn't realize that the objects that she is attaching so much weight to are just that: objects. It is her memories that are really what she is able to hang onto. 

These scenes with Yase and her mother are doing double duty. They are making us understand that she is unique in her tastes and abilities, and giving us a stronger picture of her character. They are also showing us how much Yase values the unconditional love and support that her mother gives her. Growing up as a deamon, or a girl, it must be difficult to be constantly told to be small and pretty, and to not show emotion. But her mother doesn't blame her for her emotions. 
