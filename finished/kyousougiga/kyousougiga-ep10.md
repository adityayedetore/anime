### Kyousougiga Episode 10 
### A Manga Movie About People Who Have A Fun, Busy Life!

The pain of trying to live through your children, reconciling with siblings, choosing life over death, inheritance of responsibility, inheritance of personal problems, running from problems, finding a home, keeping that home, all made more complicated and dramatic by the fact that the father is (the child of) god, and the mother is the incarnation of a bodhisattva. 

There are also themes of living the past, the present, or the future, and preservation and stagnation, and destruction and change. 

The fighting is a metaphor for the fighting that family has. 


Wow this was a great show. There definitely things that I missed, but wow. 

Looks like I didn't miss much! As Bobduh says, the show made a lot of it's themes clear towards the end, and revealed a lot of the hidden features. That is pretty great, and it was done in a satisfying way. I really enjoyed this show. 
