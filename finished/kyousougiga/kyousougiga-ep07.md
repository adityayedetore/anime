### Kyousougiga Episode 07 
### Mom's back, and oh, Dad's back too. 

Yakushimaru loves his mother, and his younger sister, and his older siblings to a lesser degree, but he hates his father. His father forced him into a house he didn't want to live in, and showed him no compassion. He does love his father a bit, but he hates him more. His father shaped him into a form of himself, by burdening him with his own burdens. His father is like a god on earth: the feelings and concerns of humans are not relevant to him. Yakushimaru is even jealous of his father, because his mother loves him so. 
