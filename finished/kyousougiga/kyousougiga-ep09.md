### Kyousougiga Episode 09 
### Let's think of a solution together

Ok, so this is definitely the kind of show that I need to take notes on to be able to understand. Let's figure out what is going on, shall we? 

Gears mark hidden, inner mechanisms. Set against the backdrop of stars, this image invokes the idea of the hidden workings behind the universe, and the idea of a creator. 

Koto wants to keep her home, her family, and the world, all at the same time. The thing is, the existence of her new found home, Kyoto, is causing the destruction of everything around it. And this destruction is her fault, as it is her power to destroy. 

Chapter 25 - Inari A

Hmm, this Myoue A and Myoue B and Inari A and Inari B stuff suggest that people have two sides to them. This Inari is the reincarnated father Myoue, the one with the red eyes and the powers of creation. In some ways, it seems that he is the father of himself. That makes him Koto's father, and also her brother. 

Koto comes out guns blazing with the problem of evil. If God created it, and doesn't want it to exist, then how is it still existing? How was he able to create it? The rules can be broken. If the rules make sure that I cannot be with my family, then how am I able to be with them now? "Just because it is a world that didn't exist at the start, and the shrine doesn't want it to exist now, I can't agree that this place shouldn't exist!" That really speaks to Myoue's problem. 

And now with that proclamation, it is spring, with the cherry blossom petals falling from the sky. That suggests renewal, and birth, and coming into existence. "Even if bad things happen, I don't want to renounce the world that I see with my eyes!" "I want to know the truth of myself, and the world I've found". And then the petals turn into confetti. 

Inari's job from the start was the creation of life. So something being born by someone other than me was something of an accident. You were a accident lol. At first he thought it was gross, because he didn't anyone like him existing in the world. He doesn't like himself. But now that she is so great, he wants to use her to fulfill the things that he was never able to do himself. "Reveal the truth of which the heavens are silent, and show me the answer". He might be looking for the meaning of life, what happens after death, if god exists, anything really. 

Chapter 26 - Inari B

Interesting, when I first saw this, I thought that he unlocked something in them. But it is entirely possible that Inari locked away something. And the fact that it is in the center of their chest, that suggests that it is their heart. 

It is intersting that Koto is the embodiment of destruction, but is able to heal people and forge connections between them. Inari is the embodiment of creation, but is only able to hurt people and break the connections between them. 

Oh, that is interesting. After a scene in which the false monk tells Myoue what he needs to hear, that he is strong and that he can suck it up (more or less), and then Yase tells him that she always had faith in him, we have Inari narrating that this world was kinda like a womb. It was a wonderland, where nothing could be destroyed. It was the eternal summer. But then, without the winter, the spring can never come. Now is the time of the winter. 

Birth, the weight of responsibility, inheritance, reincarnation, family, extended family, family drama, broken family, adoption, memories, stuck in the past, stuck in the present, stuck in the future, god, creation, destruction, wonderland. 
