### Kyousougiga Episode 2 – What Came was a Little Sister

Omg fantastic op!! I've heard it before a bunch of times, didn't realize it was this anime. Lets watch it again! 

This is the City of the looking glass not just for stylistic reasons it seems. One of the characters is reading "Through the looking glass". 

Looks like she believes that might makes right. She is strong. She loves her teacher. With those red eyes, and her Sensei's face, looks like the two of them are somehow related to Myoue. 

He has a drawing of Koto, the rabbit. The drawing. And the looking glass world. Ok Koto is her mother. And Sensei is her father. 

Searching for family, searching for the past, searching for memories. 

So let me get this storyline straight. Myoue, A priest with the power to create things by drawing them runs from the city of Kyoto to a small village shrine. There he intends to while away the day. But a rabbit that he has created to be the god of another universe he created falls in love with him. The rabbit is given a body by a god who can see the future, but not the past. They have three children, one natural born and the other two created by Myoue. The people who drove Myoue out decide to come after him after a while. The family decides to flee into the world that Myoue created, which is one without any strife, nothing can be broken, nothing can be born, and nothing dies. But the rabbit's time is running out: the deal with God was only to stay in the body for as long as it took for Myoue to fall in love. The god will come soon to collect. But the rabbit, Koto, is too much attached to Myoue and her children to go quietly. So Koto and Myoue leave the world of the looking glass, and are on the run. Speculation: They are rebirthed in the normal world, and have a child there. Or maybe more than one child. They name that child Koto. But something happens that takes the rabbit Koto's body away from her. So all that is left is the black rabbit. The sensei remembers the world he created, and wants to be able to go there again. Koto the kid wants to find her mother. So Koto has made her way to the world of the looking glass. 
