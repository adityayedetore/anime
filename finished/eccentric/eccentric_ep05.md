### The Eccentric Family Episode 5 – The Friday Fellows

Kinda like the hunger games a bit. Also, last episode we saw Benten taking down the other family from the inside, maybe she is doing the same for the friday fellows. 

He is still on the run, working in the old tengu's shop. 

Ok, so this is about the tension inherent in loving something and desiring to possess it. There are some things that once you have, they are gone. It is about power. Not only do we love beautiful things, but we want to make them ours, because we also love our power over them. "There is nothing contradictory in loving tanuki and wanting to eat them". Love is a form of posession. But of course there are different kinds of love. There is the love that makes you want to help and watch over, not to possess. 

Part of the reason that Yasuburo keeps helping sensai is becuase he is the one that caused sensai to break his back. In the same way, is it possible that Benten feels bad about eating Yasuburo's father, and so helps him out? It is possible that she didn't know the tanuki eating tradition, or did not think much of it until it she saw the way it affected one she loves. 
