### The Eccentric Family Episode 3 
###  Yakushibo's Inner Parlor

They have a cultural festival every year where they sail in ships through the sky. How nice. Looks like this might be the last year though, since Yasaburo's father is the one who usually organizes it. 

His brother (Yaichiro) hates asking him (Yasaburo) for help. Partially because he believes his brother is an idiot, but also because he believes that he needs to step up and take the mantle that was laid down with his father's death. Asking for help from his brother here would actually not be a bad thing. Mean of Yasaburo to do though. 

He is engaged. Interesting. I guess these things fall apart once his father died though. There is now a feud between the families. 

The small conflict here is between accepting the new ways produced by the humans, and sticking to the old ways. Tengu of the past would have turned up their noses at the thought of traveling by train, but now...

Cute, he has a crush on this girl, and so he gives her things. This crush is a bit of a problem for him though, not only because it is unrequited. Loving a human means that he has to accept the changes that humans have wrought, and he can't do that. 

His feelings towards the festival capture his current feelings towards life. It is better to watch from afar, because if you are in the thick of things you can't see what is going on. Yes, true, but you also are missing out. 

The beach, the birds, the water, and yet it feels like Yasa is making a deal with the devil. Pretty sweet, if you ask me. 

She is pretty badass, but also like a child. How terrible could she be, to acutally eat one of the tanuki? Maybe she sees them only as insects. This is interesting, it is a reversal of the usual dynamic between human and god. 

The celebration is not about reverance for the past, but instead enjoying the act of celebration itself. The tanuki way of life is about living in the moment. In that way, Yasuburo is a good tanuki.

An exploration of family politics.  Authority, social standing, tradition, feuds, arranged marriage, star crossed lovers, the elderly, inheritance, blood, class, hierarchy... This show has it all. 
