### The Eccentric Family Episode 1 
### Goddess of the Noryoyuka

Bobduh likes this show, lets see how good it is. 

We begin by relating the city of Tokyo to it's beginnings 1200 years ago. There have been deamons, tanuki and tengu, living along side humans all that time, it seems. Each flavor of deamon believes itself to be the center of the world, and the others to be simple accompaniments on their journey. But our protagonist, a tanuki, is a bridge between worlds. He admires both the other groups from afar. 

Don't know what to make of this op, but I like it! While the characters are in almost every scene here, there is an emphasis on the city location as well. Humans live in the cities, tanuki on the land, and Tengu in the air. But the land and the air have always been, so the city is a new encrochment into both the tengu's and the tanuki's territory. 

He is cross dresser! Thats pretty cool. It does mean that he doesn't care for human (or maybe even tanuki) social norms too much. 

They are "branded as the unfortunate children who failed to inherit the blood of Shimogamo Soichiro", which means we are probably going to be looking at some inhertitance and birthright type conflicts here. 

He is caring for his teacher. This tranformation stuff is reminding me of Naruto. Maybe the trope is gotten from other places in Japaneese culture. His teacher, or grandfather or something, thinks highly of him, but also believes that he lets his talents go to waste. 

Ok, he has a crush on a tengu. Nothing really unexpected here. Forbidden love, whats new. 

Grumpy old teacher, loves Yasaburou, is frustrated that he no longer has students to teach. 

"This is just the prop for cupid". Relating yourself to the god of love, are we? And shooting an message laden arrow at someone at their place of work, how romantic. But your arrow missed her heart. 

How much power does this lady have? Everyone left when she came in. But here we have it, the difference between humans and gods causing problems for their relationships. 

They literally have to be torn apart? What the hell? This is one weird relationship. 

"Once I wrestled with a difficult question of how one should live as a tanuki. I felt I understood how to live an interesting life, but I wasn't sure what else I should do. And then, somewhere along the line, idling away my time and doing nothing, I realized that there is nothing else I need to do besides live an interesting life." No ambition, no desire for self improvement, no desire to better the world. If you always make decisions based on what is the most interesting, you are going to land yourself and the ones you love in a world of hurt. 
