### The Eccentric Family Episode 10 – The Behind-the-Scenes Dealings of Ebisugawa Soun

"Who would want the responsibility of being the Nise-emon? It is antithetical to the life of a tanuki to be tied down. Any upstanding tanuki would ask themselves, is it worth throwing away this easy, comfortable life style?" We hear these words from Yasuburo as we see shots of the two families, centered around the running candidates. So for what is becoming Nise-emon worth it? At the end of this little montage, Kaisei stares down into the well, and we see the tanuki eaters of the Friday Fellows. The answer, it seems, is to make life better for family, by removing the threat of being eaten. 

Yaichiro is not trying to become the Nise-emon for the right reasons. He simply wants to live up to the high standards set by his father. He knows this, and Yasaburo knows this, but they both accept it for what it is. 

The difference between the brothers is pretty clear here. One who sleeps in, another who wakes up early. 

Yajirou doesn't care who is elected, as long as the tanuki society calms down. This is also not the proper way to be thinking about it. Both Yajirou and Yaichiro have peices of the right idea, but not the whole of it.


