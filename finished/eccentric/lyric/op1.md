# Que Sera, Sera

Tsunaida te wo furi hodokeba                | If you shake off our connected hands
Itazura ni  kimi hohoemu                    | with a mischevious smile
Haru wa arashi  zawa zawa suru              | There are spring storms
Mou hanabira chirisubonda                   | The petals have already fallen

Kuchiguse wa ke sera sera                   | Que sera, sera is a favorite
Mata tsubuyaku koe ga suru                  | that I am singing again

Kanojo wa itsumo sayonara iwazu ni          | She always dissapears with the wind
Kaze to kieru                               | without saying goodbye.

Mirai no koto wa wakaranai keredo           | I do not know the future
Ima koko ni aru kiseki wa boku no mono to   | But the future here is mine
Shinjiteru                                  | I believe
Yakusoku nante shitakunai keredo            | I don't want to make a promise
Sono te wo kazashite tashika na mono        | But we hold up our hands 
Te ni ireyou                                | And reach for it 
Sore wa bokura wa kagayakasu                | it will make us shine
