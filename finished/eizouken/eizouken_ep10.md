00:40 In this scene, the trio are reletivly relaxed, listening to music they found online. Mizusaki is sprawled out on the sofa, legs kicking in the air as she plays the music outloud. Mizusaki is all about capturing natural movements. I wonder if the animators drew this scene with that in mind? Her relaxed movements seem so natural, I'm sure some time must have gone into them. 



1:15 Mizusaki has not been accepting many modeling jobs, now that her parents have accepted her desire to become an animator, not an actress. Kanamori tells mizusaki to keep modeling, because her fame is the main selling point of the eizouken productions.Another discussion of the improtance of factors external to the final product itself in the reception and sustainabiltiy of that product. And by product here I mean anime. I wonder why this idea is being repeated so oftern? Is it going to be a source of conflict in the future? Is it a point that the creators really want to get across? Or is it just a natural story devlopment from Misusaki's parents acceptance of her career path? I wonder what the actual relevance of this is in the anime production world. Will external factors that could be related to popularity really sink or swim a anime? 



01:58 Kanamori was usual pointedly remark to Asakuza that there is so far no story for them to animate. A running conflict here. The animators care about the animation prior to the story. But without the story, the have nothing to animate. I wonder if they are going to get a writer? 



02:58 Love the floppy neck here. The 'vice' class president is constantly looking down her nose at everyone, assured in her power and superiority. 



03:10 "The school is not ok with you making money elsewhere" says the powerful student counsel. Is this episode going to be about animators or animating studios working for others other than their main branch? 



05:00 In an overly long classroom the trio faces off against the adminstration of the school. It seems that though the student counsel does have power, and often excersizes that power seemingly against the interests of the Eizouken club, there seems to be a higher power that is much more distant and uncaring and out of touch that holds the real sway. Here the distance is literal. 



06:18 "I think you have what it takes to be a teacher" says the 'vice' student counsel to the false head. She says that Immediatly following a self contradictory response from one of those said teachers. It's not a complement, I believe. The fact that the vice president chooses to align herself in this moment even a bit against teachers and the student counsel head actually makes here on the side of the Eizouken club. The the enemy of my enemy is my friend. It is subtle, but it is an indication that the vice pres believes that the Eizouken club is in the right here. 



06:21 Not to mention the floppy neck seems to be something that is tying Kanamori and the Vice pres together: both seem unable to keep their head straight. 



06:27 The vice president seems to have the same neck position as Kanamori in this instant. This visual parallel points to how their strong personalities overlap. The vice president then says "The bottom line is, the parents are not ok with the studnets making money". She undercuts what the teacher had been saying, revealing the real reason. This no bs attitude is something she shares with Kanamori. She is also literally the closest to the film club trio, and the only one's whose facial features can be seen. If the literal distance here is a measure of metaphorical distance, then she is again closer to the film club than any of the others. She snorts in agreeement with Kanamori's words too. 



08:08 The film club has been prevented from making money using external sources. Is this a refernce to the fact that the anime industry is often stifled by higher powers? Higher powers that beleive they are doing the best for the anime industry, or a specific anime production studio, by 'protecting them'? It certianly is possible, though I don't know enough about the anime industry to really assess that judgement. 



09:56 Here putting on airs and describing the workings of the production of anime, I'm sure that Asakuza is representing a real person. I wonder who that is? 



11:01 And here we get a literal statement of how important the story is to producing the anime, in terms of priority. "The story determines the entire direction of all the staff, so if it is not done then everyone will be falling behind" says kanamori to Asakuza. 



12:55 Now that they are on their third production, it feels natural that they would be talking using the terms of production in world. While possibly before if Asakuza had said "It is the general director's job to check up on all parts of the project" then it may have felt forced, but now it seems to flow. Not to mention that Asakuza is playing around slightly, pretending to be a real general director, so that undercuts the objections to telling us the information about the anime production process, rather than showing us. 



13:27 I have really little clue how this scene is advancing the narrative, but I love it none the less. That place, with brick and no roof and a clock tower that still rings dispite being of another age is just so awesome. My lack of skill at analyzing visuals is second only to my lack of skill at analyzing sounds, so I have no clue how good or bad the sound design for this part is. However, the anime got me to pause and listen to the sound again, so I feel its doing its job right. 



19:40 And now the 'vice' president and Kanamori are even closer, physically. The vice president's questions indicate her concern for the club, and couriosity about how Kanamori is going to deal with the problem. But then the president steps into shadow, and implies that the film club may be in trouble without if they enter the real world, where they dont benefit from the protection of the higher powers: the student counsil or maybe even the cold and distant teachers. 



23:26 "I forgot that they were in their own world too". In some ways this political manuvering and argumentation has a direct influcene on the artists, and in other ways it doesn't. Artists are in their own world, a world that they are constantly creating for themselves, and so the machinations of the real world are in some ways distant. 



Another note, this anime is labled "comedy, adventure, slice of life, drama" by crunchyroll. Hah! Though I don't think I could come up with better categorizations. 

