00:52 "Why are they fighting in the first place?" Azakuza needs to come up with a reasonable reason for why the sides are fighting, or else the production cannot move forwards, and also the fight scenes will have little tension. 

03:14 Ah, we just had a conflict that resulted in phyical fighting. The president of one of the clubs is caught stealing money, and so is surrounded and eleminated. Harmony was broken since that president failed to obey the rules that bind the clubs, and so war and punishment occured to restore the society. This is one type of conflict. 

03:47 Ah the vice president is so badass. 

05:05 What the hell is up with those mohawk peoples? I looked them up, they are some german metal band or something. Not sure what is going on here. 

05:16 After a scene of conflict, Asakuza sees a person who she thought to be an antagonist without the context of the antagonism, and she realizes that she had misjudged the old lady. This is where she gets the idea about a misunderstanding being the source of conflict. 

07:45 Kanamori is unwilling to engage with the makebelive of Asakuza. 



09:45 Its been 10 minutes already and I have no idea what this episode is about. Asakuza is still unable to find the reason for the scenes she wants to draw, and the episode starts with that conflict. She asks, "Why are the fighting in the first place?

" She is unable to come up with the idea, and so is avoiding thinking about it by playing around. In the meanwhile, another club was arrested on charges that they spent the school's money without premission. How do these things connect? The vice president seems to have taken an interest in them, and is following them around. Also we get to see another side of the distant old woman of the previous episode, who was portrayed as an antagonist. Hmm, is this episode going to be about the nature of narrative conflict? What about it though? Ah, when we saw the scene with the other club being arrested, there was little tension because we didn't know what the conflict was prior to the event. And the queen was being carried away by the ants. 

