# Shibahama UFO Wars!
## Eizouken Episode 12: Final Episode!
00:00 The different ways people react to stressful situations, perfectly depicted. I love it. I love mizusaki's character acting here, it is exactly the embodyment of the feeling you get when everything looks to be falling apart at the end. 

03:50 And things do fall apart like this: the key is to keep a calm and level head, and sort through the madness. 90% of all these problems turn out to be fixed in the end, and you can only blame yourself if you didn't try your best. And that is what they are going to do. 

And ... its done. What a great anime. Its an anime that started a journey for me too. Just like Asakuza realized animation is created in the first episode, that very episode made me realize that art is created as well. I realized that art _communicates_ in addition to entertaining. This anime, I'd say, has stared my journey towards more knowledge any any other peice of art ever has. 
