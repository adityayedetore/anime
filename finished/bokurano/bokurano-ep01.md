I have a pretty good idea of what this anime is going to be like, considering that I read a bit about it from Bobduh. 
It is going to be in the vein of Evangelion, and probably will be in conversation with it in some way. 

This first image of the bird eating the worm suggests at the cruelty of nature. 
They say that people work for the good of the whole. That it's their duty as members of society. 
One works for the good of all. Isn't it the other way around. The whole works for the one, right? 

We are being set up with the conflicts of pre-teen youths here. Budding, but not fully formed desires, cruelty, family, how we are starting to define ourselves. 
Things like this are just edible prey with natural predators. 
It would either die from being eating or being my toy. That's the only difference right?

This is also an instance of the cruelty of this Kokopeli.

And the adversary is a giant crab, in some ways. Is this going to be a show about humans fighting against the cruelty of their circumstances, and the cruelty within? 

I was wrong when I watched the first time. The one who is delighting in this is not the one who killed the crab, but instead the one is mean, and who likes the swimmer. 

Hmm, all the chairs around him are adult chairs. Is this a reflection of the times, or is it a indication that the people before him were adults when they went through this? 
