### nhk Episode 04
### Welcome to the new world!

They are telling us about why people like gal games. Not because they are real, but to escape reality. Having such a unlikeable character espousing this philosophy functions as a critique of the gal game consumers. 

Relating this Misaki to the gal game girls is sort of 4th wall breaking. Thinking about it, if gal games are fake, and Misaki is like a gal game character, then Misaki is fake. This points to the fact that she is simply an anime character, and to the fact that the critique of those gal game characters can be critiques of anime characters. 

Trying to find something that it is impossible to prove that it exists. She is comparing love in a relationship to a conspiracy. "Are you fine with not finding out the truth? Are you fine with not finding out if I love you?" 

In addition to an exploration of social anxiety, this is also a critique of otaku culture, or at least an exploration of it. 

A desire to save, a desire for unconditional love, a desire to spoil, a desire for innocence, a desire for control, a desire for things to fall into your lap. 

Going outside could be good for him. 

The ending is about jealousy for the innocence of a baby. 

