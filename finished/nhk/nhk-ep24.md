### nhk Episode 24
### Welcome to N.H.K!

As I said before, a exploration of the causes, symptoms, and treatments of the Hikkikomori condition. A story of people moving past the difficulty in their lives. Problems like depression, social anxiety, and schizophrenia, trauma, and low self esteem don't have solutions, and this anime doesn't try to provide any. Instead, it explores the ways people can temporarily hold them off, the ways they can become worse, and the ways they play into our lives. 

There is one solution that it offers though. It is the problem of agency and god. When in good times, it is easy to feel in control. But when everything is going wrong, to blame ones self is to risk spiraling into self hatred. Then some turn to god, to ask for help out of their situations. But then, isn't god also causing all those bad things? The solution is the N.H.K, an organization that works to make all the bad things happen in the world. But the key is that _we_ are a part of that organization. So we must constantly fight from the inside, to try to bring the organization down. So when bad things happen, they are not in out control, but we can also fight to try to keep them from happening else we are complicit. 

All in all, a good anime. I had to skip a few episodes, since they were a bit painful to watch, but still good. 
