### NHK episode 10
### Welcome to the Dark Side!

He really likes this girl. Cigarettes are nothing in the face of this addiction. For the attention starved, human closeness is a drug. 

These talking appliances are speaking for Sato's sub concious. He is afraid of being hurt, so he wants to believe that Misaki cannot be as kind as she is just because she likes him. 

I mean, he went up to her and broke it off in person. That's better than just not showing up right. 

Self destructive impulses for the sake of avoiding pain. I get that. 
