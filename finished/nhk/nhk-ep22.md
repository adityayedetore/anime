### nhk Episode 22
###

Is the desire to be alone being related to arrogance? "Human beings always want someone to look down upon." To have a relationship is to think that the other is in need of you, and that you are in need of them. So it is both arrogant, and weak at the same time. Both Misaki and Hitomi struggle with self worth, and both want someone to need them. 

    
