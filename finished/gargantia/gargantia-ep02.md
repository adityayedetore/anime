### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 02 
### Planet of Origin 

Unfortunate that this opening is not great, but that's fine, at least it is short. Or maybe not so short. The animation in this show is so vibrant though. I kinda wish the girls weren't all sunburned, but its fine. 

And she is always running. So much to do! So much to see in this beautiful life! or something like that. 

As we are seeing the meeting in which what is to be done about the boy is talked about, we are being introdued to the sub characters. The captain is formal, stiff, and serious. The mechanic is self important, hot-headed,  but likeable. The diver is pushy. The other protagonist is a bit naieve, but sees the good in people. 

This computer is bloodthirsty and warlike. Abnormal solar activity made earth a frozen wasteland apparently. Like wall-e. 

Interestingly, she also lives without worry of death, but in a whole different way, enjoying her life. I mean, its pretty understandable, given the difference in their situations. 

Honestly at this point I am hoping there is little violent conflict. This seems like it could be a relaxing anime. 

Shots of the cg mechanical movement, shows that this place is industrial level tech, all rusted. 

Meaning of life: taking care of frail, pail, sick younger brother who likes ships and reading. 

He may have come from the heavens. You know the legend. Ledo's machine has a halo, so it makes sense. Interpreting their life through a legend. Both sides believe the others to be fairy tails. So it is like both are meeting the supernatural. That evokes a sense of wonder. Maybe seeing the other's culture as a fairytail will become a theme. 

This is very cute. Cutesure shock. This is too cute omg. So food is the way to make negotatoins work. And now everyone is eating as they listen to the story. 

Double cute culture shock it is. Also maybe might be about appriciating things in life? 

And now we get the meaning of the name. It is this fleet of ships, that have lived on earth after the ice melted . They scrounge for parts in the sea, digging up the remanents of the past civilations. They are scavengers. That lightning looks a lot like the shots of the enemy. This is perfect, finding beauty in what was a source of destruction. So another theme is beauty. The path they follow is that of the lightning bugs, for practical or religious purposes, I am not sure. Ok it is to power their ship. They live on the brink, relying on the luck of finding this beautiful thing to be able to continue. Finding beauty even in the midst of uncertianty. Its good that he is not immediately able to accept it. 

So he realizes that by signaling his troops, he is going to bring doom to this world. But even in this world, there is conflict. And this time, it is against other humans. Not sure if that is going to be commented on. But there is still strife. And it is sexually charged as well. Well, there has to be a damsel in distress, doesn't there. But this anime is not taking Lido's side. He is cold, and doesn't realize the consequences of taking life. He has only fought the faceless evil, and now he is applying that same ability to fighting people. So the strife of the heavens are different in their scale and horror. Though the people of Gargantia face threats, they are nothing like the threats posed by the evil flowers. 

Themes: 

* _Finding Beauty (even in the midst of uncertianty)_
* _Learning to respect the lives of others (even enemies)_
* Cog in a machine
* _Culture shock/experiencing life_
* Finding a reason to live
* _Finding a reason to fight_

Purpose: This episode introduced us to some of the side characters, told us about the world, showed Ledo's cold side, and introduced us to the theme of beauty.  
