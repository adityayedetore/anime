### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 06 
### Festival

And again we have a piece of culture shock allowing us to interrogate a part of life that makes life worth living.  Money, and the things you can buy with it. 

I don't know why it bothers me so much when the camera objectifies these women. Maybe I just don't see it's purpose. Ah, like I said before, its becuase its so distracting to me. When that isn't a part of the narrative, it take up too much of my attention. 

Yay, no power fantasy!

And, he is learning to communicate his emotions. 

And a big part of life is sexual desire (for most people, and almost all men), so I guess this is warrented. And he can't watch, because his trauma pulls him away. 

I dunno about this dance, but this is a cute scene. Music, festivals, jobs, and of course a crush. Though it is not at all clear that this Ledo has any sexual feelings at all. 

Themes:

* _Finding Beauty (even in the midst of uncertianty)_
* _Cog in a machine_
* Finding a reason to live
* _Culture shock/experiencing life_
* Finding a reason to fight
* Learning to respect the lives of others (even enemies)
* Trauma/PTSD

Purpose: The last episode was the beach episode, and now we are getting the festival episode. Interesting that these cultural touchstones are the center of these episodes. I it is for two reasons: the things that we have seen before are easier to understand, and also it is reasonable to believe these cultural artefacts would survive where any people survive. 


