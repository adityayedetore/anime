### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 08 
### Separation 

The head of the clan dies, which leaves thing in disarray. This means that Lido might be allowed to go to fight those squid. And Lido finds out that he can't return to the fleet. He has been thinking of his time here as temporary, but now he knows that he might be here for good. 

We have some sexism here. 

So the death of the leader has cascaded down, and there's a domino effect. Now the fleet is separating. We are sad because this means that our protagonists will be broken up, and also because it means that Lido will be going after the whale squid, fighting instead of living. 

These mechanical separations are just a symbol for the human ones. 

That was great acting. That glance up after the outburst was perfect. So she is weighed down with the burden of leadership, also knowing that her father died in the leader position for some reason. Now a theme is the weight of responsabiliy to people. And the way to get through it is to rely on those around you. 

This is also great acting. She cares about him, but not (only) in the crush way. She thinks that Lido is throwing away is life, the life that he could enjoy, to fight for no reason. She feels loss, sure, but more than anything else she feels that by going he will be losing himself. This, I like. Her friend helps her through the "You want to be with him, but he's leaving", but didn't realize that part of her feelings are that leaving would be bad for him too. 

Instead of working on his flute, he is sharpening his blade. Nice clear symbolism and practical storytelling there. Shit, a child that looked like me made it. That is your clone, dude, not your younger brother. "I do not want to make Amy sad. So I will kill the squid" You are on the right track here, but not quite. This line shows clear progression for his character. Instead of fighting just because he is ordered to, just because that is his nature, now he wants to fight to protect. But he is still clinging to the belief that the only way to make harmony is to fight. The thing is, you could just, like, not fight. 

I wasn't focused on her character, how does choosing to respect her adopted grandfather and the leader's death show a progression of belief? She couldn't go pay respect to the leader becuase she didn't believe that she had what it took to take up his mantle, and so wasn't able to let go of him yet. But her talk with the lady made her realize that the previous leader respected her and trusted her. Also she believed that by mourning she would be showing weakness, and realized that that was not the case. And she is also using this opportunity to make a somewhat self-serving speech. Nice work, you'd be a great politician. This is an appeal to work together to stay together. In other words it is only with everyone's power that harmony can exist. Fracturing is the opposite of harmony (I think, maybe), so this argument makes some sense. And this is also good for her character arc, since it shows her relying on other people. This is really good writing. Did she expect people to clap at a funeral? 

Themes: 

* Finding Beauty (even in the midst of uncertianty)
* Cog in a machine
* _Finding a reason to live_
* Culture shock/experiencing life
* Finding a reason to fight
* Learning to respect the lives of others (even enemies)
* Trauma/PTSD
* _Harmony/policy of destruction/utilitarianism/reliance on others_
* _Burden of leadership_
