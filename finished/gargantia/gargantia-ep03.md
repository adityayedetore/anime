### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 03 
### The Villainous Empress

Ok, so this the expected fallout of killing all those pirates. I don't really like this "Ledo you baka" shit, but its a trope, whatever. 

Should I watch this op? The animation is pretty good, so maybe I'll watch it for a few episodes. 

The killing is framed as murder, and it is suggested that this will start a cycle of revenge. And some more of the same culture shock. Also, where did they get the chicken in the first place. Compares the life of humans to the life of animals. So that is about the theme appreciation of life. Ah its a bird, not a chicken. 

"Give fresh water to the one who catches the fish" it is about helping eachother. 

Really. Didja have to do this. Hmm, keeping women as prisoners of war, that't not good. Introduction to this antagonist, also the titular villain. Exposition of the conflict between the clans. And now they are skipping over the language barrier. 

Color pallet changes to greys when he is preparing for war. Maybe through fighting he will learn to appreciate life. Yeah right. 

I'm not a fan of this action. But he got the huge claw from the lobster, that's cute. 

He's trying, at least. But can we just not have these conflicts plz.

Themes: 

* Finding Beauty (even in the midst of uncertianty)
* Cog in a machine
* Finding a reason to live
* _Culture shock/experiencing life_
* _Finding a reason to fight_ 
* _Learning to respect the lives of others (even enemies)_

Purpose: Shows the conflict with the pirates, satisfies the people who came for giant robots fighting eachother. 
