### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 10 
### Island of ambition 

The blood on his hands, it is human blood. Now this is the proper use of a dutch angle, with Lido stumbling around like he is drunk. Now he is looking back and remembering all the scenes that he had blocked out of his mind that indicate that the Squid are like human. That voice acting is fantastic. 

Ah, I forgot to mention the theme of the overreach of science. The hubris of man. This goes all the way back to the originial sci-fy, the modern promethus. "This will bring civilization back to it's former glory". Instantly this turns into greed and selfishness. A taste of power corrupts. And this instantly results in internal conflict. 

It's like along with Lido, a virus was brought up from the deep. In it was an infection that turned men's hearts, and yes I mean men. 

Ah that hand from the beginning. Before the fingers were drumming in apathy, but now they are clenching and trembling. Realized what you have done, have you? Realized that it has all been a lie? He is looking up at the flower shaped ships, and they don't mean the same thing that they did before. He fears what humanity has done, and by extension himself. And now, the calm before the storm.

Now turned to empire building. How quickly they fall. But really, was this the fault of technology? No, this was lurking within us this whole time. 

This show is doing such a good job showing Lido's pain. This is the point where he realizes that he cannot fight without a reason, and moreover he can't murder without a reason.

Wow. "Negative. I have reached the conclusion that 'one cannot live while the other survives' on my own, without the interference of the Company. The reason for this is that they are beings that have rejected civilization as a whole. "Humans are weak, so they build technology and civilization. The squid are strong, so there is no need for them to work together to survive. Thus they don't need civilization, so they don't have any desire for harmony, and they are strong, so the could easily crush human kind." Not sure I understand this argument.

Themes:

* _Finding Beauty (even in the midst of uncertianty)_
* _Cog in a machine_
* Finding a reason to live
* Culture shock/experiencing life
* _Finding a reason to fight_
* _Learning to respect the lives of others (even enemies)_
* _Trauma/PTSD_
* _Harmony/policy of destruction/utilitarianism/reliance on others/civilization_
* _Compassion for the other_
* Burden of leadership
