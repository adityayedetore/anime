### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 11 
### Supreme Ruler of Terror

We open with Amy seemingly gotten over the pain of Lido loosing his way. She doesn't realize how bad it has gone for him. 

So some other commander has found their way to earth. This commander has created a cult that worships the squid? It is strange that they take the squid for their symbol. Who's dick is bigger. Also it looked like lightning, that is resonant with the method of attack of the snails. 

Good job making us sympathetic for blondie again. That was very effective. Instead of maliciousness, it is framed as incompetence. 

Giving him a relationship now. That is not a good sign. He's gonna die. 

The weak serve the strong. That's divine providence apparently. So if Lido fights the cult leader, that would mean to both rebel against his military commands, and the idea that his worth in life is to fight the squid.

Cult-hood and religion and power. So it is about dignity. To coexist with those who through away civilization would be to accept anarchy. The Squid reproduce as they desire. That cannot be called human. Defining happiness as maximal functional utility. But twisting it into a way that supports a military chain of command. 

I love that this results in a peaceful takeover. It is not like utilitarinism need violence. 

Theme: 

* Finding Beauty (even in the midst of uncertianty)
* _Cog in a machine_
* Finding a reason to live
* Culture shock/experiencing life
* _Finding a reason to fight_
* _Learning to respect the lives of others (even enemies)_
* _Trauma/PTSD_
* _Harmony/policy of destruction/utilitarianism/reliance on others/civilization_
* _Compassion for the other_
* Burden of leadership

Purpose: this was an episode that showed the effects of a view of functional utilitarianism that is distorted by religion and military service. When an enemy approaches, it is possible to control the many with religion and fear. This is the dark side of "The enemy of my enemy is my friend". 
