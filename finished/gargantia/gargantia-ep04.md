### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 04 
### The flute of recollection 

Yay, now that the fight from the last episode is over, we have a new group being welcomed into the fold. With these quick shots of Ledo trying to balance on the boxes, I suspect this is going to be a cute episode! Given the title, we are going to at least learn about Ledo's past, and maybe even Amy's. I'm looking forward to it!

I just realized that my notion that the women in these anime shows shouldn't be drawn with such skimpy clothing might reflect sexism on my part. The problem is when they are created for the purpose of being objectified without any reason for that objectification. If that is not happening, let the women in these shows dress however they want. 

Yay, he is being yelled at and trying to speak their langague. Steps towards acceptance! But it is only the chief that can speak to him, so there is some work to be done still. 

And now he is forced into indentured service for wrecking the hanger. So he's got a job with no pay. One more experience in the bucket list of life, check! Slacking on the job, check! 

The fact that he doesn't know what he is making suggests that this is part of the memory that has not been fully erased by the hypnotism. 

Dating (sorta), and family, learning more things about life. In the galatic empire, humans that are not useful are disposed of. So his perspective on life is pretty understandable. 

Understanding the use of life even if that life is not whole. 

It shows the robot after talking about just living. So maybe the robot is being compared to the human being, who won't just power down when they are not needed, but will continue to interact with people and continue to "live". 

Now he's depressed. That's not cute. Go back to being not depressed plz. But no, I think he is reflecting on his own place in the world. He defines the right to live by how much utility things have, but that doesn't work in peaceful times. 

Ok, so even if people don't have functional utility, they have utility in what they provide to the relationships of the people around them. We are kinda going through a laundry list of the meanings of life here. 

So even in the times of the hypnotic brainwashing of these soldiers, some human connection exists. It's pretty inevitable. But, though I'll be the first to admit that I know nothing about war, I'd think that the experience of the soldiers is nothing like this. It is filled with hazing, and that is what brings soldiers together. So in no way are the relationships this cold and innocent. 

Themes:

* _Finding Beauty (even in the midst of uncertianty)_
* _Cog in a machine_
* _Finding a reason to live_
* _Culture shock/experiencing life_
* Finding a reason to fight
* _Learning to respect the lives of others (even enemies)_

Purpose: We've got a more relaxed episode this time. An exploration of the things that give our lives meaning, like work, the hunt for sustance, family. Lido defines value based on functional utility, but here he learns that value can also be based on the utility we provide to others by way of our relationships. 
