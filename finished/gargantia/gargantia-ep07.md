### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 07 
### A soldier's fate

Now Lido has to fight against the brainwashing of the Navy. He sees squid, he fights squid. This has probably been engrained in him. But in this case fighting the squid may result in this woman being hurt. In this scenario, fighting is not the answer.

Oh, so the problem is that they are sacred. That's fairly flimsy. 

He believes that they may have indaded with him from space. 

After hearing this monologue from the leader, we come across the idea of fighting to protect vs fighting to destroy. It definitely is interesting that these squid may be related to the Hideous things, I wonder if anything is going to come out of that. "I want to keep fighting to protect the smiles of the people here. That is proof that I am alive." This is the proper motivation for Lido. 

No matter what the situation is, it is a privaledge to be able to see it. The desire to end conflict by means of extermination. This is about war vs attampts at harmony. 

"The whalesquid are terrifying when provoked, but they never attack otherwise" so this is going to be about harmony and war. "It is just a matter of time before they strike. At that time, how will your people respond?" Reactive or preventative fighting. 

Flirting with the devil for the sake of progress. Or rather, for the sake of revenge. Now we are getting an episode that is about the reasons that people fight. They fight to prevent harm, to get revenge, to restore harmony, and because that is what they were told to do. Some believe that fighting will restore harmony, others believe that fighting will break it. "The policy of destruction isn't wrong. It's necessary for the humans of earth too". They literally say that for these people "Survival strategy == co-existence and co-prosperity"

Nice, when he hears that the squid are coming, he accidently destroys the flute that he is working on. If that flute represents a part of himself, and a part of his humanity, then distroying it means that he is forgetting himself, and also losing a bit of his humanity. Also these scenes of the reaction to the whale squid are great. The whole place is closing up and down. 

Explanation would be nice here. Becua... And now they can't explain! This is much better. She can't even shoot, because that would draw the attention of the squid. Crisis averted. But it is the start of a new one. Lido doesn't realize that the fact that the squid didn't attack though they could means that they are generally peaceful. That idea can't coexist with what he has done, and how he has been trained. 

"I will not let the needs of the few to outweigh the needs of the many" so this is what it means to be a leader. This was a pretty great episode. Harmony, for the main part. 

Themes: 

* _Finding Beauty (even in the midst of uncertianty)_
* _Cog in a machine_
* Finding a reason to live
* _Culture shock/experiencing life_
* Finding a reason to fight
* Learning to respect the lives of others (even enemies)
* Trauma/PTSD
* Harmony/policy of destruction/utilitarianism 

Purpose: Reintroduces a conflict here. Lido has trained to be a soldier, and cannot escape from that past. 
