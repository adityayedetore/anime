### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 09 
### Deep Sea Secret 

"Is it true that no one has come back alive from it?" "That is just a rumor." More about legends, builds up the tension, makes them feel like they are in a place that they shouldn't be. Their power is 1/120th of the power of the space squid. It is possible to drive them away. No, I'm going to kill them all. As of yet, he hasn't seen negative consequences of the "policy of destruction". 

And we have the blond fighting to prove himself, and for revenge. We are getting the flashback. I don't think we need it, but lets see. Ok, we didn't really need it, except maybe to raise the stakes. 

Upped the stakes, and explained the fighting style in one go. See, why couldn't you have done this, Flip Flappers? 

A lack of natural enemies resulted in devolution. There is no such thing as devolution dude. 

Ok so it seems like my suspicions were correct, humans created these squid. Either they are being used to cause a fake war, or people tried to kill them so they evolved. Oh, so it is no consequences that will cause Lido to change his mind, but information? That would be awesome. 

People are afraid that scientific advancement could cause problems, like making war worse, or erasing culture. But religion and culture have evolved with science, and now happiness may be available for the first time. Ominous words if I've ever heard them. So they are doing eugenics. 

The "They are human, so to kill them would be murder" from the beginning is coming back again. This is pretty great. After recognizing humanity in the squid, now to kill them feels like murder. So he realizes that a policy of extermination is horrid xenocide. 

Themes:

* Finding Beauty (even in the midst of uncertianty)
* _Cog in a machine_
* Finding a reason to live
* Culture shock/experiencing life
* _Finding a reason to fight_
* _Learning to respect the lives of others (even enemies)_
* _Trauma/PTSD_
* _Harmony/policy of destruction/utilitarianism/reliance on others_
* _Compassin for the other_
* Burden of leadership
