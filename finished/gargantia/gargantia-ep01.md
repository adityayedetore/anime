### Gargantia on the Verdurous Planet (Suisei no Gargantia) Episode 01 
### Castaway 

The introduction to the setting: the far future, where people live on large space ships. The 'home' they have found is one that they have made, fragile glass petal looking structures, filled with the blue and green of life. This place is self titled a utopia But in this advanced future, there is still conflict, which requires the mass recruitment of the young to fight. The announcement sounds like a propaganda piece, though for good or ill remains to be seen. "We must fight to protect our home, our utopia".

Both the humans ships and the aliens look flowerlike. They are "lower life-forms". 

This is explicitly stated to be Hypnosis. We don't know if this is malicious yet though, as hypnosis could be the only that they have found that they could survive. That symbol though, the Vitruvian man, represents the beauty of humanity. Interesting that it has been explicitly changed from a man only to a man and a woman. 

The system moniters the pilots to the extent that their blood lactate levels are checked. Also this extent of monitering seems to be more fit for inspecting a machine than a human. 

The first thing that we hear this Lido say after awakening is "Where is the enemy?" which speaks to the all consuming purpose in his life. 

This scene is alluding to star wars, with the visuals holographic ships flying around a grid blue laden planet, and the statement that this is a "Surprise attack, with everything the alliance has to throw at them". 

Also one more note, the fact that all the heads looked the same, and they were in pods makes Lido seem like one ant in a swarm. That comparasion is made more apt when the commander says that they are attacking the enemy "nest". 

The soldiers are not given "citizenship privileges" until they complete 145000 hours, which is 16.5 years, and is likely this kid's whole life. The prospect of living like a citizen doesn't excite him, and he seems to recieve this news with apathy, just tapping his fingers on the mouse like control mechanism. Also the 'right' to visit Avalon. So Avalon is not a place that is for everyone, but only the priviledged. So this is not the utopia for everyone. The computer is even surprised at this kid's lack of interest. He will be granted the right to "Eat, drink, and sleep freely", and his response is "That seems like it is a bit beyond me". So this kid has probably had to do battle simulations his whole life, maybe even fed through a needle. 

"You have proven to be a superior example of humanity, fit to survive and reproduce" ah so that is where the symbol of their navy/civilization comes from. The ideas of survival of the fittest are distorted here, to something like eugenics. Implies a cold perspective on the value of human life, fitting in with the idea of humans as ant soldiers with no will. 

The paralelism between the previous hologram and this scene makes the latter feel almost fake, which might be how Lido is feeling. 

The battle may also be drawing a bit from Ender's game, or wherever ender's game got it from. 

As this is the beginning of the series, it is unclear if this will be a success or a failure. I'd suppose a failure, since that makes things more interesting. Or maybe a success, but with our Lido being injured in some way. 

Ok, explicit statement of this protagonist's problem. Hasn't experienced many of the things in life, no longer values his own life, doesn't look forward to any pleasure. 

He is throwing away his life, maybe not to save his squad-mates, but because it is what is life is for now. "I don't care if I burn out"

He has been ordered to leave his commander behind, so he does because he is a good soldier. The bonds of friendship do not overcome his oath to be a soldier. 

So he is flung into the outer reaches of space. His path is to learn what makes life worth living. 

Introduction to the other protagonist. Bright colors, contrast with the greys of Lido. In addition, the sounds of water and the rust on the ceiling mark this place as old, and not like the fancy high tech of the army. The other protagonist slides down a rail, in a rush, enjoying herself, but also carefree. The running animation is decent, but I am less sure about those eyes, they are a little to bright. She has been running to the crashed mech. The clothe designs speak to pirates, or at least people living by scavanging. 

Those horns, what are they? Lido had one in his ship. 

Revival was succesful. That is interesting for sci-fy reasons, but also it suggests that Lido is beginning a new life here. Caring kid. 

This is cute. The choice of not having them speak the same language, I wonder how they are going to resolve that. 

The artists make the women sunburned. Great. 

So his computer is discovering that this place has no need for external life support. He is surprised at this, suggesting that he has never heard of people living on a planet before. Like we suspected, this planet is earth. That fact allows us to better explore what led the world to be left. 

Both the themes of Cog in a machine and culture shock are setups for the theme of learning what it means to live. He's probably learn what beauty is along the way, and find a cause to fight. 


Themes

* Cog in a machine 
* Culture shock/experiencing life
* Finding a reason to live
* Finding a reason to fight

Purpose: The purpose of this episode was mainly to introduce the world and the characters. It also set up Ledo's path towards growth. As someone who knows nothing about living, nothing about anything really, he is a great way to look at life and what makes life great. 
