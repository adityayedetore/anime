"That is a little too honest" Irony. 

I think maybe this whole thing with the guy is supposed to display that our protagonist is unable to feel strong desires, and can't recognize her own desires in herself. 

The black haired one sometimes feels like she wants to get away from it all, but she fights through it and does the things that she has to do. The protagonist is somehow inspired by this. 

That was supposed to be a kiss lol. 

What an interesting relationship. 

Oh so she also doesn't realize that other people have desires. 

Hmm, to be 'grown up' is to be able to take a step back and look at emotions from anther perspective. That is what she is learning to do. 

It is interesting that I don't feel like they are in love. It's probably because our protagonist feels so little, and the black haired one doesn't express a crush in the usual ways. Really all I can say is that they are very interested in eachother, though it is not love. Honestly that is also a realistic depiction of a relationship. Friends can be very interested in eachother. That is a type of relationship that interests me. 
