There is something here about how putting words to things helps understand them, but it also obscures parts of them. I understand this very much: I have always tried to come to my own conclusions about things, and have in that process oversimplified. This has led to me failing to see many things how they are. 

Loved that Hyouka reference. 

Ok, so I gotta figure out how co-dependency fits into all this stuff. After the genuine scene, there is a pursuit of a 'genuine' relationship. But now, they are asking, what does it mean for a relationship to be 'genuine'? One thing that it is not is 'co-dependency' where people stay together because they use the other person for emotional support. 

Ok I'm re-watching this episode, and this time I'll take detailed notes. 

'She said something similar when the class was ostracizing her'. She probably said 'I want to do this on my own, I do not want to depend on you". Hayama tried to satisfy both the class and Yukino, and so failed to satisfy both. Since he failed, Yukino lost faith that she could rely on him, so she decided to be fully on her own. Still, she never really solved her problems with making decisions and taking action, and now she is feeling that she is back to her dependant actions. (maybe Yukino's decision for Hachiman to 'fulfill Yuigahama's request' actually does show that she is able to make decisions now without relying on anyone else's desires. At the core, Yukinoshita's problem is that she doesn't know her own desires, so becomes paralyzed when it comes time to take action. On the surface, this time also seems like that: Yukino is putting off the decision to Yui, since she doesn't know her own desires. But it is possible to interpret this scene another way: Yukinoshita knows that she wants Yui to be happy, and believes that Yui will wish for Hachiman to herself, and so believes that she is letting go of them both. It is true that Yukino is acting against her own interests, but she also wants Yui to be happy, and to be rid of Hachiman. Which is funny, because Hachiman knows Yui wants them all to be together forever, lol)

There is that word again, self-satisfaction. I don't think I fully understand it. Maybe it is like "I am doing this because I want to"? 

Hachiman is trying to communicate his motivations to Yukino. He is trying to _show_ her that he is doing this because he wants to, and thus is not 'co' dependency. But for Yukino, the important part is that she is dependant on him to motivate her, and act in her stead. 

Ah, so one parallel between the plot and the character stuff is that a bunch of words won't be enough to change people's opinions, because those people don't intend to listen to reason in the first place. The way Hachiman succeeds here is by appealing to a shared history and a sense of responsibility. Is this an indication that this is how he is going to reach Yukino? 

Ah this is sooooooo goooooood! I love the hidden meanings here, and the fact that they are hidden means that they are much more satisfying, complex, and realistic. Now after watching more shows I understand that the shows I enjoy most are not just subtle and complex, but have subtle and complex subtext that interests me. 
