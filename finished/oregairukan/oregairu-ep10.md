## Gallantly, Shisuka Hiratsuka moves forward

This episode I'm going to looking for things in particular: I am going to be looking for implications that are not entailments. Lets have fun with this!

Well, this is not exactly a within world implication lol. 
So, the fact that Iroha is able to delegate so well means that she has earned the respect of her fellow student council members. 
It is interesting, is this a implicature or not? 
It might possibly be an implicature on the part of the writer of the show. 
Hmmm. 
That is very interesting. 
So when we are reading, we learn to add another assumption to Grice's maxims. 
We assume that the selection of words and scenes has deeper meaning than what is on the surface. 
Wow. 
That is cool. 

Here is another instance of there being more meaning than meets the eye. 
The choice of him being sound designer recalls Yukino's job during the cultural festival, where they had become close for the first time, and when they accidently had been close in a public manner. 

"You'll never be able to get drunk". 

A forked path, suggesting that he has to choose. 
