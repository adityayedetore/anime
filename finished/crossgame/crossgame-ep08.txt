Lets see how many ways that they are alike: 
    - He has always woken up so late that he doesn't know that the batting center opens at 10
    - She wakes up late too. 
    - She want so play baseball. 
    - They don't like to lose. 

It's also that Wakaba and Momiji are alike. There are parallels going on here. 

Now the moon is full. The moon represents Wakaba. Is the moon full when the two of them are getting along? 

He doesn't have confidence in himself. And in this situation, he leans on Aoba. What a complement. 

This is how the pair of them are showing their love. The pitch is like a kiss. 

And her hand. That is like the scene in HxH, where Gon breaks Killua's hands while playing dodgeball. His love of Killua is enough that he is willing to hurt him. 
