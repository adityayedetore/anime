Ok so Aoba actually didn't like him at the start. He was lame, not worthy of her sister, taking her sister away from her, and they were too similar to not get in arguments all the time. 

This is a recap episode. It is setting up for how important Wakaba was to everyone, and how deeply she affected them. She passed on her dreams to them, and through the symbolism, we see that they are always thinking about her. She is always hanging around in the background of their mind, like the clouds, or the moon. 

