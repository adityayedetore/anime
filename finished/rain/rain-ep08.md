She is no longer getting embarassed at the sight of him, buyt only at his idea. 

He has gotten injured, so his spending his free time chasing after a girl. Now where have we heard that before? 

This part is just begging for a close reading. Previously in the series, the manager thought of himself as the servent standing out of the rain, but now that position is taken up by Tachibana, who has the pimple on her cheek. At it's heart, the rashamon is about taking a bad act and changing how you perceive it, to make it a good one. The act itself doesn't change, but the way you look at it does. After a calamaty, and when we have been thrown out of our passion, our beliefs are thrown into disarray. We do not know what to do with ourselves. But then, we can change the way that we look at things, and move on in life. 

Wow, we have a lot of things of symbolic natures here. First is the rain, then the pimple, then the Rashamon (and in it the act of walking out of the rain, struggling with beliefs, and changing the way you look at something, Tachibana's want for a sequal), then the drawn umbrella. 

Here is the turning point for Tachibana. She is no longer head over heels for the manager, and she is starting to want to return to running. She asks her friend, not the manager out to the summer festival. 
