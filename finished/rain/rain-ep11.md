Damn, this show is pretty rich. We had the mention of time, it being stopped, it running out, and it stretching into the future, what with the stopped clock, the stopwatch, and the calendar. Then we had a symbolic series of drawings on Akira's calendar, with the past being with the pink cat, and the future with the black cat, and with a drawing of Komodo.  

The sound of the rain gets in the way of Haruka's words reaching Akira. And due to her ability to run, Haruka is able to step out from under the banister into the rain to try and follow Akira.  

Haruka waited just a moment, but that was enough for her to miss Akira, and to miss the train. 

We have more mention of time here. "Time is money" 

Some more about time here, with the one minute novel. 

Wow, Akira is really in a bad state. She realizes a bit of the fact that she is using kondo as an excuse, at least on an unconscious level, and she is even falling a bit out of love with Kondo, but this isn't enough to take her out of the rain. Now she is choosing to stand there, maybe even consciously. That is an even tougher spot than she was in previously. I'm surprised. I would have thought that this show would have ended with her moving forward, but no, I guess not. Kondo is the only one who is able to move forward at this point. He has even started writing again. 

