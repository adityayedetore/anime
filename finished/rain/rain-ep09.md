She ran there and ran back over. That means that she can run now. 

Tears flowing like rain, over things lost. Does "We can't go back to the way we used to be" mean that there is no way for them to be together? No way for them to move on? It is left ambiguous what Akira thinks about this. 

The bar is literally filled with young people. 

Her name is midori, that is probably a pretty common anme but it does remind me of Norweign wood. 

They know greek. That is pretty intersting. This just a fantastic anime. 

Tsuki Ga Keiri. 

-------------------------

At the end of the previous  episode, Tachibana chooses to attend the summer festival with Haruka rather than with Kondo, when previously she really didn't ever make any attempt to spend time with her after the injury. What does this tell us about the things that Tachibana desires? 
* It tells us that Tachibana is less in love with the manager, more willing to engage with the things that she loved in the past (running, Haruka), or both. 

The last time Haruka was with Tachibana (in the bookstore) Yui (the girl with the blond hair) had just come back from the bathroom, where "she had almost peed herself" because of the long line. This time, the two little kid sisters of Haruka also need to pee. Is this a coincidence? 
* Possibly. In this anime, people are constantly getting wet, or having things break, or both. It might be rain, but just as often it's drinks, or sweat, or tears, or pee. This may or may not be important, but these are the sort of "coincidence" that we have to look out for. 

There is a brief cut of the two cat doohickies that Tachibana got at the gashapon machine, where they are shown as masks in a store. One of them is the black one that Tachibana keeps on her bag. The other one is the pink one that Haruka keeps on hers. Tachibana wanted the black one so that she would have good luck in her relationship with Kondo. Seeing that Tachibana wasn't able to find one, Haruka got it for her, and at the same time slipped her a note which said "Even without track, we are still friends right?" In light of these facts, what does the black cat thing mean to Tachibana? Why is the pink one important to Haruka? Bonus question: why are the feelings that the characters have for their respective doohickies in conflict? 
* The black cat one is a reminder of Tachibana's love of Kondo. The pink cat thing reminds Haruka of her friendship with Tachibana. On a practical level, they are in conflict because Tachibana can't do track and work at the family restaurant at the same time. So, the two cat things represent the tension between the things that are pulling at Tachibana. Her love for her friend/track, and her love for Kondo/not having to continue with the pain of falling again. 

When Tachibana sees Kondo at the summer festival, she runs over to him. And then after she is done talking, she runs back. Why is this significant? What does it tell us about Akira's phyical state, and why does that matter? 
* It suggests that her injury is improving. It is important becuase Tachibana acts like there is no way her injury will ever get better, and like she will never run again. But this might not be true. It tells us that it might be more than just the injury that is stopping Tachibana from running. It could be Tachibana herself not wanting to go back to running and to risk falling again. 

After the twins start crying, Haruka's tear drops from above onto her brother's arm. What does that remind you of? 
* It looks like rain. Once again, we have an explosion (emotional, physical, etc), and then the characters getting rained on.  Also, as we know from previous episodes, after most catastrophes, life goes on. But sometimes you get stuck, and you can't move forward. Though it is bright and sunny outside, rain still falls in your heart. So it is very fitting that it starts to rain after Akira says "We can't be the way we used to be". 

Why is it in character for Tachibana to say "We can't be the way we used to be" to Haruka?
* Tachibana is stuck in a rut, and believes that she herself cannot be the same way that she used to be: namely, she has convinced herself that she will never heal, and never be able to join track again. She has to realize that she can heal, and things can go back to normal. So when she said "We can't be the way we used to be" it was also in line with her own state of mind. 

I don't know much about Japanese food, but those ball things look similar to the food that Haruka's brother and the little kids were eating. Is this something to take note of? 
* Maybe. They might be completely differnt foods. But then again, if they are similar foods, then there is a further connection between this food and youth, over and above Kondo saying "this brings me back" (becuase we just saw young children eating that food). 

Physically, Kondo looks so much younger in this episode. How does this fit in, thematically? 
* Kondo looking younger is probably a reflection of the fact that he feels younger. This tells us that he is, at least subconciously, overcoming his belief that his life is over, and that he is an old man with no further purpose in life. The whole episode, and honestly the whole show (for Kondo, anyways) has a running theme of believing that your life is over, and that youth has been lost forever. 

"No matter the place, the moon will always reflect in the water". Is this true? Is there a time when the moon isn't reflected in the water? Hint: when is there water on the ground, and you can't see the sky? 
* When it is raining. When it is raining, the moon won't be reflected in the water, even if it is up in the sky. And whats more, the moon ever so often is completely swallowed by the giant snake that circles the earth.So what Chihiro said was not really true. It is a bit idealisitc. But in one way it is true: some time in the future, the moon will be reflected in the water. Bonus points if you can figure out why this is thematically relevant. 

Consistently, when Kondo is embracing youth, he is portrayed with fire looking imagery (or at least spark like things traveling upwards, or sunlight, or something related to fire). But when Tachibana is falling in love, it is often portrayed with a droplet of water sound effect, and/or some underwater imagery. Why is this relevant? 
* To be honest, I'm not sure. But it is consistent, so I get the feeling that it is relevant. 

What does the moon represent here? Hint: it is connected to the quote about the moon from before. 
* The moon represents the fact that even when love (and passions really) look to be gone, they are really still there, waiting to come back around. 

Tachibana doesn't wish to be closer to the manager, who is standing right next to her. Why is this important? And also, a previous episode Tachibana ended with Tachibana looking at the moon. What were the circumstances that time? 
* It is relevant because it suggests that deep in her heart, she would like to be with Haruka (and therefore continue running) more than she would like to be with the Manager. We saw the moon before in the episode about the wind, and how much Tachibana loves the sound of it. As she is walking home from the library, a gust of wind blew, and pulled the curtains on the moon. Tachibana stood there, listening. 

