I am having a bit of trouble deciphering this. So lets summarize what happened. 

Yutto got a hamster from his friend, and decided that he would have his father take care of it so that he could have an excuse to visit his father more often. We can infer from this that the relationship between the manager and his former wife is not cordial. They had a falling out, and they still aren't over it. And it must have been somewhat recently too, within the last five years, since Yuuto is so young. 

Then On the way over, we see two frogs, a middle sized frog and a small frog, which represent Yuuto and Tachibana. Tachibana finds out that the manager learned the magic tricks from a book, and sees that he has (or had) been a writer, with a whole lot of manuscripts, going all the way back to the 1990s. 

Then we have the scene where Tachibana is in the closet, and is getting really hot. 

Ah, I figured it out. So getting sweaty in that closet is like being in the rain, and jumping out is how she fell in love with Kondo to get out of the rain. So Kondo is like her umberella. She is using it as an excuse to not run again. 


