Hmm, interesting. So last time, the show portrayed this from her prespective. Now it portrays it from his perspective, and everything is tinged with grey, and worn, like whites that have been washed with the blacks. 

That is a way to do something about a situation for which there is nothing to be done. He went on half listening to the rain as it poured down on Suzaku Avenue.

That Rashomon, it's a story about if it is worse to take from the living or from the dead, and about how people react after calamity strikes and they are left without the ability to continue their passion. 

His car has stopped, and this girl is making him drive forward. 

