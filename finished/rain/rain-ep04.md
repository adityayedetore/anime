Ugh not in the mood. I'll come back to this one. 

I wonder exactly what is trying to be said about the asshole dude. Maybe all it is is a contrast between a good relationship, and a bad one. The creators are saying, Yes, the relationship can't really work out, not for a long time, but it isn't a bad relationship. Compare it to the relationship we just saw. 

She likes running after someone, but not being pursued? 

Interesting. She doesn't do it, because she can't run? 

So this is the scene where Tachibana imagines running up to the manager, and kissing him on the cheek. The yellow lines are like the starting block of a race. Thematically, what it's doing is tying the lack of her ability to do what she desires with her inability to start running again.

As for why she likes him, the best I can think of thematically is that he represents the fact that life goes on, even after the rain, and that broken things can be smoothed over with a smile and an apology. Maybe this will all become more clear in further Episodes. 

Her problem is that she believes that her injury has taken her out of the race for good, when in truth she could continue if she wanted. His problem is that he has failed to publish his writing, so has given up on achieving anything. 

Ok, I looked up the manga ending, and I believe it to be a pretty good one. Kondo fires Tachibana, though he has fallen in love with her. Tachibana gets over Kondo, and rejoins the track team. Tachibana fell in love with Kondo as an excuse to not do track, and so needs to fall out of love with him. Kondo, on the other hand, needs to fall in love with Tachibana to accept himself, and to accept that he still has something to give to the world. 

