Interesting. The fact that there are clouds in the sky, but it is not raining. Does this mean that we have a turning point for the characters? Also, Akira just timed herself waking up. Is that how she wakes up every day? Is that not a reminder of her days doing track? That is ambiguous, in terms of its meaning, though leaning towards her coming back to track. 

Damn, was that a hint of a feeling that she didn't want to be there? 

Omg, just realized, that the metaphor working with Kondo is that his flame has gone out. That is what the smoking is about. 

Omg, those hurdles are called "neverold". This show is absolutely brimming with visual metaphor. 

Kondo falls in love with her (though it is not stated if this is a fatherly love), and goes back to writing. It is heavily implied that Akira goes back to running. Honestly, I would have been fine with the ending being more ambiguous. The whole thing was wrapped up into a little bow a bit too quickly for me. But overall, beautiful anime. One of my favorites. 

