### Live Forever / Live Forever 
#### Listeners episode 1

This turned out to be a pretty good first episode! I like the art, I like the music, and I like the characters. What more could I ask for? 

Haha, that's actually not what I think at all. Yes the art is nice, and yes the music is nice, but those reasons have never been and will never be sufficient. Seeing all the comments online about dropping the show because of the "ugly" art makes me loose a bit of faith in the anime community. For a first episode, this definitely does not deserve a 6.4 on MAL. 

My take on the first episode is that our protagonist has grown complacent in his hand me down scrap yard life. The girl he finds represents teenage adventure, what with the obvious sexual innuendo throughout the episode. The climax of the episode is clearly a metaphor for sex. Also (in addition to being a strong and interesting character) the girl asks our kinda lame protagonist to run away with her, the classic teenage act of rebellion if there ever was one. And the whole use of _Rock_ music to fight against the monsters? Again, teenagers are the ones who pump music into their brains like they live on the stuff. I like ho this show seems to be wholly celebrating the adventure that comes with the teenage years, unlike many of the other shows I've seen. In Oregairu, Evangelion, Hyouka, FCLC, the good things that define the teenage years are at least undercut or more usually completely overshadowed by the struggles. It is refreshing to have a show that is basking in naieve teenage glory. The teenage years aren't all bad: in fact, now that I think about it my teenage years were great! I've seen so many anime now about the bad things that come with being a teenager that I have forgotten how much I enjoyed my own teenage years. Good job anime! You have me good!

On to the next episode!

Oh and also I really like this girl... 
