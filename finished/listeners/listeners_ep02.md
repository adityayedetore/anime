### Half Man / Halber Mensch
#### Listeners Episode 2

Not as fun as the last one, but this one wasn't bad either. This episode was about idols, and the ones who fail to become them. I don't think that the message was as clear as it was in the last, but the players are idols in this world, rock stars, but they are unable to play their music. For some reason their music has been taken away from them. Being a idol seems like a dream to the fans, but instead it is overwhelming being surrounded by so many people, and lashing out to push the crowds away often pushes away and hurts those you care about. Interesting concept for an episode. I guess the last episode was a bit better because everyone has gone through their teenage years, but few have become idols, or have become so obsessed with idols to actually do them harm by their presence. I really do like the interaction between Mu and the lame protagonist, and its sad that we only got to see it for half the episode. I also did like these Dada people, I'm sure they are a reference to some band or something. 

Not as excited to see the next episode as I was last week, but I think it is still going to be good!
