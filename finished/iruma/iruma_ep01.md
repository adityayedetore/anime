00:00 The rythum of these introductions reminds me of the introduction to HunterxHunter more often than not. I had previously thought that all the shows were referencing HxH, but maybe they are all referencing something else. 

02:10 Speaking of shonen shows, this will probably be my first shonen show since ... hmmm, I actually dont remember. Was it HxH? Wow, the last shonen anime I completed (maybe the only one) is HxH! I've tried other shonen shows, like BHA and FMAB, but always dropped them. I got further in the mangas, since I could skim them, but wow, thats pretty wild. I wonder if I'm going to like this one? 

02:30 Telling, not showing, that iruma needs to work becuase his parents are scumbags. I don't really mind, I'm not expecting anything subtle. 

02:50 Being almost killed by dead frozen fish falling on your face is so bazzar that it is funny. I like the slow motion dodge of the falling fish here, the dissonance between a life threat and that threat being dead tuna falling on you is pretty funny.  

05:23 Again, they can get away with a narrator explaining because of the setup of the natrator in the beginning, and because we are not expecting this to be serious. Humor in dramatic treatment of the absurd character trait that iruma has never refused anyone, and that being his main conflict here, not that a demon is asking him if he wants to be adopted. 

08:13 Introducing new characters and at the same time opening up the possibility that the Demon lord is more than he seems at the same time. While we aren't really given any indication that the Demon lord's plans are sinister, we are caused to wonder what they are. Cliche, but nicely done. 

11:37 Again, great job introducing characters, and at the same time bulding humerous tension. A

24:30 Absurdity, and defying expectations. Needs to walk the line between events that seem too normal, and events that will make us loose our sense of immersion. Making serious things seem trivial, making trival things seem monumental. Maybe I can use this series to better understand absurdist humor. 

