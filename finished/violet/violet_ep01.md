# "I Love You" and Auto Memory Doll
## Violet Evergarden Episdoe 1

I am excited to be starting this acclaimed series! Lets see how it goes. 

00:18 Holy shit the quality of the animation of this anime is so amazing. I can only hope the rest is as good. 

01:00 "Your eyes. They are the same color as your eyes. The way I felt when I saw this ... This feeling. What is it called? ". How is it that this scene so effectivly conveys that she is not human, and yet is learning to experience human emotions? Or at least that she is something differnt than human. Maybe it is becuase of the way that she speaks of what must be love with such a blank look on her face. We are setting up a conflict for her here: she is not human, and therefore has difficulty understanding the human emotions she is experiencing. Cliche, but done well I guess. 

01:52 Violet is devoted to this Major Gilbert. She suffered a brutal accident, that has left her motor control partially damaged. 

03:00 "She is a tool. A weapon. She will fight if you asked her too. A tool without a heart". Hmm cliche. However we the viewer is given conflicting images of this Violet. Is she a compassionless killer? Or a tramatized young girl? The fact that the Colonel Hodgins hallucinates blood on the hand of a doll that resembles Violet speaks to the tension between these ideas, and also introduces idea of PTSD, which is likely to be a large part of this plot. 

04:58 Violet's instance on knowing the state of Colonel Gilbert indicates the extent of her devotion, and her inability to read the expressions of Colonel Hodgins which tell the tail of Gilbert's death indicates her inhumanity, and her innocence. 

12:00 "I am the major's tool. So if he doens't need me, throw me away. Throw me away somewhere", oh violet, can't you see that is exactly what is happening? You are being thrown away, away to a loving home where you won't have to be dealt with any more. 

12:01 And the frailty and sadness of the request makes the the Colonel change his mind. He won't throw her away, at least not here. The irony here is pretty great: the Colonel Hodgins had actually just compleated her request, but undoes it because she asks him that very same request (confusing sentence but you get my point). 

20:11 Hmm, this seems pretty basic based on the first episode. That is probably why Bobduh dropped it. And by basic, I mean that the problem is not too complex. What is the lie that the protagonist beleives? My only worth is to serve Colonel Gilbert? What she wants is to understand what love is. And she has many problems other than the fact that she bases her worth on someone who is dead. She beleives other lies, such as that Colonel Gilbert is still alive. How is this going to become more complex? 
