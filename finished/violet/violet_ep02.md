# Never Coming Back
## Violet Evergarden Episode 2

00:00 Oh, and another problem that Violet faces is her inability to enjoy herself. Also, she takes things way to seriously. And has difficulty understanding metaphors. They are really going all out here on the human robot cliches, aren't they? 

01:44 So in this series we are going to learn more about Violet's past with Colonel Gilbert, as Violet slowly learns what the words I love you mean. She is also going to learn that she has worth other than as Gilber's tool, and probably how to better read and experss and feel emotions as well. Also, her given name probably wasn't Violet. 

01:50 Unfortunatly, so far the side characters have been much more interesting than Violet herself. 

06:03 I like this kid. Was he in the army too? 

10:35 The job of a Auto Memory Doll is to translate people's intentions to paper. To do that, they need to be able to understand the emotions of the people that they are talking to. This, of course, will be hard for a human robot, and is the perfect place for her to grow and change. I would 

11:30 I am always wondering if an anime would be better if the themes would be more on the nose, more accessable to the general audience. Here, everything seems obvious, and I would rather it be more subtle. The grass is greener on the other side I guess. In this anime, all the narrative beats are obvious. Nothing much seems to be hidden. I think the only things hidden are the things that have not been shown. In this anime, since Violet is brash and harsh in her dealings with others, and is a fighter, we have a scene where she overpowers a client for getting angry. It is the logical conclusion, the obvious conclusion. 

12:58 And here we have a scene where something is being communicated, but I feel it is too subtle. A woman comes into the Auto Memory Doll service, and asks for a love letter. She says "I'm not such a simple woman", while we see that though she has all these jewelery and fancy clothing, the tips of her fingers are scratched raw. What could the creators be trying to communicate here? I guess being subtle is difficult to do to the right extent. 

15:27 "Words can have different interpertations. What one says isn't the whole truth. Its a human weekness. They test others to confirm their own existance. It's a contradiction. " What does this mean? Does this mean that we don't tell others the truth because we want them to find it for themselves? And when they do find it, it means that they are looking at us and understanding us? I think this statement is a bit too vague, or else I am not compleatly understanding it. In what way does understanding the half truths of others confirm their existance? And is it not the case that when trying to communicate we lie not intentionally but becuase we do not understand things ourselves? How doe this play into that statement?
Is she saying that people don't tell the whole truth, either because they can't put it into words or because they dont want to? If so, what is all this about confirming their own existance? Is it just empty philosophy of a tv show trying to sound deep? 
21:04 Violet's innocence and inabiltiy to lie is affecting the people around her. This is more of a characteristic of a flat story arc than a positive one. 

22:00 What a tradgey. Violet I'm sure will not be happy when she finds out that Colonel Gilbert is dead. 


