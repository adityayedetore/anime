# Youth Romantic Comedy Is Wrong, as I Expected
## Oregairu Episode 1

00:12 The scoff and smirk: the instant mark of the cynic anywhere. But more than cynicism, this is the _act_ of the cynic. A public face to show others that you are better than them. 

00:59 Hachiman writes that he sees the teenagers around him failing: failing to communicate, to make meaningful connections, to be honest. But they all pretend like teenage years are one delightful journey, something to be happy about. While he says this, he is surrounded by classic high school events that he is not a part of: laughing with friends, playing sports, chating... these things around him that he isn't apart of hurt him like shattered glass, but he has closed off his heart and embraced being excluded. He has started to exclude himself. From that place of exclusion he can see what others cant, the contradictions of teenagers pretending to be more and fulfilled than they are, convincing themselves that they are more happy and fulfilled than they have any right to be. Since he sees these things he feels superior.

03:31 I don't know what the name of this cliche is, but it seems the teacher who is self concious about her age is a fairly common one. I wonder if the creator chose cliches as bases for characters, then developed a meaningful characters on top of them, sort of as a 'I'll show you how this can be done properly'. 

03:51 Why the 3 sakura petals when he first sees Yukino? Is she a representation of the possibility of that 'youth' life that Hachiman has forsaken? 

04:35 Unable to make eye contact, not actually very good at socializing, are you, Hachiman? 

05:37 "Since it's your request, I can't very well refuse". Yukinoshita has problems figuring out how to act on her own, and so requires others to tell her what to do.

06:51 He really is awkward. He growls as an introduction? Really? A

07:15 "What club is this? Try to guess? The literature club?" This is definitly a refernce to the the cliche of the literature club being the place where these sort of high school anime romances happen. Think Hyouka. 

08:36 Hachiman (again awakwardly) sat his chair down in the empty room far away from Yukino. There were a few shots to establish the distance between them. But now they are standing, and yukino has moved towards Hachiman. Now there a shot with the same framing, but with Hachiman and Yukino on the same side of the screen. This movement is more than just phyical. 

08:52 This shot of Sensei listening in on their conversation and smiling suggests not only that her plan, whatever it may be, is working, but it also suggests that hachiman and yukino are actually getting along well with eachother.  

09:49 "Why can't you accept someone for who they are and what's in their past?" this gets to yukino. Why is this? "That wont solve anyone's concerns, nor will is save anyone". In the world of the anime, she is right. Hachiman will later have to come to terms that his way of solving problems doesn't fix them in the long term, but only hides them under the rug. Its a matter of If a problem no longer exists, it is no longer a problem vs trying to solve the root of the problem. Yukino may be right, but she has her work cut out for her: actual change is much harder than changing the situation so that your problems no longer matter. Also, in a way, by refusing to participate in 'youth', hachiman has done just the sweeping under the rug of his problems that he is touting as a solution to yukino. 

10:58 And this scene is bookended by another sakura petal floating in through the window and landing on yukinoshita's book. It is clearly a representaion of the fact that this scene is exactly the 'youth' life that repulses hachiman so. 

12:18 "When I was in grade school my shoes were stolen 60 times" says yukinoshita to Hachiman on the second day of their club. Are these the sort of things that you'd tell a stranger? I may be reading into this scene too much, but yukinoshita seems awefully comfortable talking about her past here. Maybe it is her canned justification of her situation, or maybe it is the fact that some things need to be explained to advanced the narrative. 

12:57 "I hate the way you accept your weekness as a positive" Yukinoshita is struggling with her own problems, and failing, and struggling again. Hachiman, who has admitted defeat is the anthesis of what people should be like to her. 

13:17 "She would never lie to herself" aw shit, he is deifying her, erasing her flaws. His first impression is superficial, the facade she shows people, and this can only lead to pain and dissapointment. 

13:51 "Then I'll be your fri- Sorry. I can't do that", damn yukino, you have just made your life so much harder by not accepting his friendship. The friendzone can be a nice, comfortable, simple place. Things are much more complecated when you aren't friends. 

14:00 And another problem for their devloping relationship.

14:25 She is literally coming between them. She even interjects herself into their conversation. 

<!-- 15:22 Yui has entered the club in the first place because she wants to find a way to thank Hikki for what he has done. She likes him a little, because he is her white knight. But hachiman is there, in the club! Moreover, his first words to her are calling her a bitch. But this gives the vampire permssion to enter. Now that she is inside the house, the havoc will truly start. -->

17:27 "Can you please stop trying to match everyone around you?" Yukinoshita's remarks are so cutting. She understands the problems with 8man's methods at a glance, and identifies Yui's desire to fit in and make everyone happy when the second time it comes up.17:45 But Yui is different from the rest of them. She is not cynical, and appriciates Yukinoshita's words for what they are worth. That is amazing in it's own right, and it is her ability to see the feelings of those around her is what allows her to make the judgement that Yukinoshita is telling the truth not to hurt her, but for her benefit. 

21:43 "Its just to show my thanks. You helped me too"

