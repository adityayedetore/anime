# For the third time, he turns back on the path from whence he came
## Oregairu Episode 9

00:00 Summer is two days away from ending, and Hachiman is yet to do anything of consequence. After that first week, he has not seen Yui or Yuki. His calendar is filled with Xs on empty squares, indicating the lack of things he has done. The fact that he is lying down emphesizes his lazyness. 

01:42 Writing down the lyrics to the openings and their translations is such a good idea, I wonder why I havn't done it before. 

01:54 Hachiman sees that the only thing on his calendar was the summer camp, and the thing he remembers is the look on Yukinoshita's face as she drives away in the car that hit him. Again, it is Yukino, not Yuigahama that he thinks of. Feels bad, Yui. 

02:50 Actually, he has seen Yui a bit: she left her dog over at his place.

04:25 The reason hachiman is ever in the situations where he is confronted with uncomfortable situations has so far been Sensi and Komachi. Again, it is Komachi that is forcing him to go to the fireworks with Yui as a thank you present for taking care of sable, Yui's dog. Hachiman is dragged kicking and screeming from his 'normal world' to his journey. 

05:43 Unlike Yukino and Hachiman's conversations, 8man's conversations with Yui dont have the same flow. On the train to the fireworks, Hachiman responds "Do you need spice? We are not trying to be seasoned seaweed here". Had Yukino been here, she would have come up with some quick retort. But Yui only says, "What does it matter? You have a problem?". They dont connect in the same way as Hachiman and Yukino. 

07:38 Hachiman and Yukino connect with their words and thoughts, but the relationship between Hachiman and Yui is characterized by an invasion of space. Yui is constantly touching Hachiman, like when she falls on him on the train, and when she grabs his shirt while walking at the festival. Differnt sorts of relationships, differnt sorts of challenges for Hachiman's naieve philosphy. 

08:19 The backdrop of pan-san the panda reminds of Yukino in this scene. Some of Yui's friends are at the festival, and they wave her over. And while Yui goes to say hi, Hachiman turns back to pan-san, back to Yukino. When Yui mentions that she is there with Hachiman, her 'friends' smirk, and brag that they are there with their girlfriends. This is painful. And Hachiman exits the situation. 

09:52 Hachiman feels bad for Yui, beleiving that it is his fault that she had to have such a painful conversation with her 'friends'. So to cheer her up, he suggests that they get candy apple, which Yui had wanted before and Hachiman had vetoed. 

11:04 Hachiman is not able to take a complement. Yui says he is nice, and he goes on about how he really is nice for not taking revenge on the world for all the injustice it has done him. He is diverting atteniton from the complement, so that he doesn't have to accept it to Yui, and so that he doesn't have to accept it for himself. 

11:49 Interesting that unlike in the scenes with Yukino, in which others always interupt Hachiman and Yukino's conversation and place themselves phyically inbetween them, Haruno is not placed between Hachiman and Yui, but instead off to the side. This also does make things distinctly uncomfortable for 8man though. 

12:30 This shot with an empty spot next to Haruno emphisizes the fact that Yukino is not there. Must suck to be her, unable to go to the festival. 

13:15 "Again Yukino wasn't chosen", says Haruno while looking into the fireworks. Yukino was passed over for inhereting positions in the family, and moreover was passed over by Hayama (I think). 

14:02 Haruno says "I am studying the sciences at a local university" and Yui says "thats the same career path as Yukino", and Haruno's facade cracks just a tiny bit. Haruno didn't seem to know this information. This implies the level of distance between the sisters: she doesn't even know her sister's career plans. Moreover, Haruno disaproves becuause she maybe rightly believes that Yukinoshita is simply doing this because she isn't able to choose a career plan for herself. It is a strange mixture of freedom and restraint. Haruno is able to, forced to go see the fireworks. Yukino is unable to. Haruno is forced to study the sciences in universtiy. Yukino is free to choose for herself, but paralyzed as to what to do. 

14:14 "Haruno-san, do you hate Yukino?" "Of course not! There is absolutly no way I couldn't adore my little sister who has always chased after me." And the mask is back on, but now that it has slipped a bit the cracks are showing through. Haruno envies Yukino for her 'freedom', and hates the fact that Yukino does nothing with it. 

14:59 "I love yukino! She is kind, and cool and honest and reliable" "Good! thats what everyone says at first. But then they get jelous of her and reject her. I hope you don't do that". Yui has reason to be jelous of Yuki: the one she likes, hachiman, likes Yuki more than he likes her. She's in a tough situation. 

15:20 "What about you Hachiman? Do you like Yukino? " "My mom taught me not to be picky about my tastes." That is the only thing that hachiman says this entire conversation, and it is right at the end. What can this be translated as? Does it mean that he will like anyone? Why have him answer this way? Hachiman maybe was thinking about how to answer this sort of question this whole time, that sounded like a canned reponse. 

16:21 While heading away from the fireworks, Hachiman and Yui walk Haruno to her car. The same car that ran over Hachiman. "Thats right: Yukinoshita had nothing to do with it", he says, and obviously he doesn't beleive it. "Besides, its over now. Its my policy to not dwell on the past" not true at all hachiman. We have seen you dwell on the past in almost every episode now. Hachiman beleived that Yukinoshita would never pretend one thing while knowing another, would never lie. So how could she have pretented not to know Hachiman? She was in the car when that car ran into him. 

19:37 While walking back together, Yui says that she beleives that even if Hachiman didn't save her from the car, he would have saved her at some time later. She believes that they were fated to meet, that she was fated to fall for him. And Hachiman knows what she is going to say. Hachiman is not able to accept "you're nice" as a complement, how could he accept "I was destined to fall in love with you"? He is not ready for it. He doesn't believe in himself, doesn't believe that anyone could like him. He wouldn't not be able to accept her feelings. 

20:47 "Well, long time no see" She has been waiting to see him. She started a converstion with him outside of the clubroom the first day they return to school. She has heard that he met her sister, and was worried what she had said to him. And she was right to be worried: Hachiman has heard the worst thing possible, in the worst way possibe. She wants to ask, to know if they can go back to the way they were before, to make amends, to explain herself. But Hachiman cuts her off. Come on, Hachiman, did you have to? She misses her chance here, and now it will be forever before they get the chance again. Maybe the chance to explain won't come again. 

22:08 Hachiman is a cynical loner. He beleives that everyone lies to others, eveyone lies to to themselves. He can't stand this. But in Yukinoshita, he for once saw an exception. She didn't lie to others, she didn't lie to herself. She told things as they were, without hiding unplesant truths. He admired her for it. And somewhere in his heart he started to believe in her. But now, "Even Yukinoshita tells lies". Despite activly distancing himself from it, he had once again fallen for the world, but was betrayed.
