# One Day, They Shall Learn the Truth
## Oregairu Episode 8

00:00 Funny. 

02:45 And Hachiman is the only one in the dark, and in the corner. 

05:42 Im going to ignore this joke, but Hachiman's reaction to Yukino in her swimsuit is great. 

10:44 And they are at it again. This time with backhanded complements. At this point we might as well call it flirting. 

11:50 And he figures out a way to give everyone the pain he feels. He works under the assumption that people are selfish and mean, and they are unable to change. Under this cynical light, his plan makes some sense. Rumi is being excluded, so if there is no group to be excluded from, then there is no exclusion. How Hachiman manages to convince them is another story, and doesn't really make sense to me. 

12:44 When did she start calling him hikigawa KUN? 

18:51 What is the shaft head tilt here for? Hmm, this is definitly interesting. The head tilt brings to mind Senjogahara. But what about what Yukinon says next is akin to senjogahara? "I think its acceptable for something good to happen". For sure that does sound at surface level like something Senjogahara would say. Unfortunatly, I have no knowledge of the Monogatari franchise, thaough I have watched it all the way through more than twice. Just how impovrished was my ability to understand art?

19:56 "If we had gone to the same school... many things would have been differnt, but I don't think that we could have been friends". This sounds like Hayama is insulting Hachiman. And maybe he is. But I think he is also looking back on his past self, and seeing that he wouldn't have been able to do anything about Hachiman. Hayama is restrained by the chains of the social order as much as Hachiman is. Its lonely at the top they say, and that is how it is for Hayama. This is because of the necessity for Hayama to act good at all times, to represent his family. Much like Haruno. That might by why he wasn't able to save Yukino. 

22:09 "One day, they will learn the truth" The episode title. The 'they', when looking at the beginning of the episode, would seem to refer to the children. But then again, Hachiman and Yukino are children too. Is the truth that they will learn what it means to grow up? What it means to be an adult? The path taken to adulthood is long and trecherous, and it is the path that Yukino and Hachiman are currently treading. Will they make it past the mountians? Or will they lose their way? 
