# Finally, His and Her Beginning Have Ended
## Oregairu Episode 6

00:00 Surrogate tears, that aren't being spilt. "Why did it turn out this way?" Well, when you like someone like Hachiman, you are bound to be burned. 

03:30 "Did you have a fight?" asks Yukinoshita of Hachiman. "No, we didn't ... I don't think". This nicely recalls Hachiman's previous statement that people who aren't close can't fight. In a way it is asking the quesiton, were we close? did we become close? 

03:34 Ah, he actually says it explicitly lol. 

04:15 Hmm, interesting 8man and Yuki had again started their playful verbal sparring, only to be interupted physically once more. This time it is not Yuki, but Sensei getting in between them. It again begs the question, how would things have gone if Yui wasn't there? Maybe Yui is the catalist for their conflict, their change. Maybe without her Hiki and Yuki would have become friends, but wouldn't have been to change eachother's problems. Maybe they would. Who knows? 

05:24 Again, there is alot going on in this seemingly simple conversation. Lets try to unpack it, shall we? Hugging herself, Yuki seems to be rationalizing here. She is unable to make decisions for herself. What she really wants is Yui back. "When people people leave, they stay gone" Espousing his philosophy once more, and excuse to not give it a try. That look means "You're insulting my communicaiton skills, when yours got us into this mess?" 

06:34 Yui is gone for a week, and they have already started dating. 

10:24 Sigh, these scenes just make me feel inexplicably sad. If they dont get together, that would be a crime, and in all likelyhood they wont. His sister leaves, and they start 'pretending' to be on a date in minutes. "I don't mind it, as long as there is no one around to see it".

11:42 Holy shit, are they cute together. 

12:38 And the thing of others getting physically in between Hachimand and Yuki is becoming a thing. Tbh its causing me a bit of frustration. Why can't people just let them be? In this scene Haruno gets in between them. 

14:55 Actually, the Hachiman's relationship with the dog is a bit like the what his relationship with Yui is. Hikki thinks that Yui is feeling bad for him because of the accident, but actually the fact that Hikki saved her means that she actually likes him. Just like he rejects the dog's feelings, he rejects Yui's feelings. 

17:44 This scene is filled with so much misunderstanding. But the biggest misunderstanding of all lies in the fact that Yuki and Hikki were actually on a data and they don't seem to realize it. They may be able to see the misunderstanding the are products of ambiguous words or actions, but they are not able to see the misunderstandings of their own emotions. 

20:44 "Only the cause should be the one who can be viewed as having done wrong. ... you two can make a proper start" But not you, Yukino?

22:13 "Her beginning with her fineally ends". You might read that as Hachiman's false start with Yui has been fixed, but I prefer Hachiman's beginning with Yuki ends. Their first and last carefree date. 

22:34 Wow, it really is amazing how these same lyrics in the ed can be applied to Yuki as well as Yui in the last episode. I almost thought it was written for Yui, but now I can see how amazing this is. "I swallow the feelings I wanted to say.  They become words I can’t say… Even until the very end… I couldn’t be honest." This is about her inability to tell Hachiman the truth. 

