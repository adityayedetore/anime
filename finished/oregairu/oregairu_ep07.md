# Regardless, Not Getting a Break over Summer Break is Wrong
# Oregairu Episode 7

00:00 Hachiman ignoring the texts is (also something I always do) is an act of pushing problems under the rug. This is one of the problems he has in the solutions he finds to help other people. 

06:20 "Chiba is where we went for an outdoors trip in middle school." Doubtless Hachiman, and some of the others, have memories of their middle school trips. I too remember mine. Wow, memories. Ort, yoshimi, Serena ... Had I stopped eating sweets by that point? I definitly did like being alone then too. It seems I could have become like Hachiman had I not cared so deeply about what others thought of me. Or rather, had I given up trying to get others to like me. 

08:04 Putting the high schoolers in charge of kids is a good idea if you want them to grow up. Nothing makes you learn more than teaching others. 

09:11 And Hachiman is only able to put Hayama's good nature in terms of a manipulative "secret technique". 

13:25 I love their verbal sparring matches. 

14:31 Hachiman (albiet jokingly) equates adulthood with immoral behaviour, Rumi equates it with being alone. 

16:08 And Yuki and Hayama are bringing baggage to the table. To save Rumi, Hachiman and Yukino need to save her from becoming versions of themselves. To do so would be to at some level recognize that they too need to be saved, or at least to save themselves. The way yukinoshita acts, I realize, is a problem for her and those around her. She might say, "My words wouldn't be a problem if everyone else thought rationally" or something like that, but the fact is that no one exists at the standard she holds them to. To recognize other's failings and accept them is important to become a well adjusted adult. But she dislikes Hachiman's philosophy for this very reason. Hachiman takes things to the other extreem, believeing that even attempting to improve is a waste, as it will ultimatly end up in failure, since perfection is impossible. Both are naieve opinons, but neither are completly wrong. It's just that thinking at those extreems makes you life a lot harder. 

20:08 "Who's there? Hikigawa Hachiman. ... Who's there?" This response more than any other indicates how comfortable Yukinoshita is being playful with Hachiman. She doesn't treat anyone else like this, except maybe Yui, and that too to a much lesser degree. 

20:19 "Miura picked a fight with me" again shows the comfort with Hachiman, but I'm droneing on about this point now. Also the phrasing "picked a fight with me" indicates a certian level of childishness, an inability to see her own fault in the issue. 20:20 "I spent 30 minutes out-arguing her until she cried" Well if there is no recognition of fault, at least there is a feeling of guilt. 

21:10 And there is a switch here between the names Yui and Yukino. Why does Yukino speak of Yui as having the similar experience to Rumi, when she herself had that experience? It is possible that Yui did as well, but Hayama is more worried due to the parallels to Yukino's not Yui's experience. 

22:10 Hachiman is gently pushing her for details of her past. But the instant he starts asking, she leaves. Is this because it relates to the accident in some way? Is yukino sill not comfortable enough to tell Hachiman? 
