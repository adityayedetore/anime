# And Again, He Returns from Whence He Came
## Oregairu Episode 5

00:00 It seems we are opening again with Hachiman's attempt at a rebuttal of his ways to his teacher. She his gatekeeper, his wise old sage. She is the one who throws him into his hero's journey. On that note, there are some things I'd like to do during this episode. Namely, I'd like to figure out what Hachiman wants, and what he needs. 

00:14 Oh shit, almost didn't listen to what was being said here. Seriously, this anime is so packed, every line is important.  "You said you want to be a stay at home dad, and to work is to lose" says Sensei to Hachiman. why exactly is it that he espouses this ideology? Hmm, to work is to lose. This definitly fits in with his belief that trying and failing at anything is not worth it. 

00:41 "You know, police and fire fighters are always late to the scene. In a way lateness is justice ... " says 8man, right before he is floored by a sucker punch from his teacher. Her fist makes the irrefutable argument that it is power that is justice. This is great, and so quickly done. 

00:50 Oh and the obigatory perverted joke. I can't say I didn't like this one though, so I can't really complain without being ironic here. 

05:27 Reintroducing characters, and setting up the 'monster' of the episode (refercence to monster of the week plotlines, not calling kawasaki saki a monster). I can't exactly see what more is being done here. 

08:29 Now what was the point of this, other than being cute? 

10:08 "There is one thing that will make a girl change: love". The word love is matched with a key fitting in a lock and being turned. Is love suposedly the key? Dunno what is being said here, if anything, especially given that this plan fails. Maybe this sub-subplot is a critique of the common angle 'love will cure everything?' If so, does is fit with how the anime progress in the future?

11:21 Why the fuck did Hachiman invite him? That makes no sense given Hachiman's character. Maybe there is a differnt but longer explanation for how Z is involved in the source material.

12:52 And yes, the diversions do seem to be comments on how this would go in other anime. They don't really need to be included though. 

16:45 In the last scene there was more talk of stories, explicite references to Cinderella and Little Mermaid. There is almost certianly things being this, but as it is about kawasaki saki more than our main trio, It probably isn't worth the time to think about. 

17:58 Why does Komachi give this mini speech in adulation of her brother? What purpose it it serving to advance the overarching plot? 

22:08 "Once again, he turns back on the path from whence he came". This is inevitable, as Hachiman has not yet changed. Yukinoshita's words have challenged his philosophy, but he does not yet have a stake in any issue that hinges on him changing that philosophy. He feels pleasure in his pain. He is turning away from others so that he saves himself from pain, but by doing so he is hurting others and himself.

