# Sometimes the Gods of Rom-Coms Smiles Upon You
## Oregairu Episode 3

00:53 I very rarely felt that feeling of hating partner work in high school, but now in college I feel it more often than I would prefer. The awkwardness of that motion is painful to watch, along with the fact that they get his name wrong. The nod too, is so relatable. So many relatable things in this opening two minutes. Looking at racket while talking to self, its perfect. 

03:20 Alone with the wind, knowing others are together behind you. I love this feeling. Feels like this scene was speaking directly to me. I wonder how Wataru can be so accurate with his dipiction of the loner's life. Are the feelings and desires of a loner so ubiquitous? A bit ironic if so. 

03:26 and with this wind comes Yui. 

04:52 Yuigahama is being awefully honest here. I wonder if this is yukino's influcence? Also, Why are they showing the bikes at this time? A memory of the morning, when they talked breifly alone? Or is it something else. Whether they would admit it or not, Hachiman and Yukinoshita do get along. Their conversaion flows. 

05:09 And here is the always present touching that indicates level of intimacy and comforatablity. She even touches his neck. 

I only vaguely realized that this episode was about hikki, giving us more to latch onto and empathize with in his character. I guess it should have been obvious, what with how much I was empathizing with him.

