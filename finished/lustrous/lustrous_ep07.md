## Hibernation
### Land of the Lustrous Episode 7

02:59 Fear paralyzed Phos in her first battle. She runs in the night, saying "I need to be able to run like this during battle". Seeing Cinnabar, Phos hides in the grass. "What would I even say to her right now?" Phos has gotten her wish, to be able to fight like the others. But as she changes, becomes more capable, more valuable, the shared conditions that ties her to Cinnabar grows weaker. They were both deemed useless by society, and now Phos is no longer so. Phos' drive before was to help Cinnabar, and that give her value. But now she has not worked on that in a while, and her promise is slowly loosing strength. 

07:10 They hibernate? Interesting. They look like flowers. I guess that hibernation is the logical narrative conclusion of using the sun for sustenance, but I wonder if hibernation is doing anything on a thematic level as well. 

08:34 The Lustrous all say "Good night", and the teacher says "Good Night" back. Their relationship is an odd mix of paternalism and love and other things. 

08:38 And their yearly tradition is a hug. How cute. But also this plays into the pretty strange relationship between the Teacher and the Lustrous. 

09:20 Ice tells Phos "Always to fickle and aimless. You're half a level harder than me, don't you feel ashamed? I'm sure this has been another idle year for you, right?" Phos doesn't immediately respond. Ice has been asleep for half a year, and still remembers the Phos of the past. But that Phos is only half here. In her pace, something new remains. 

11:14 Phos is bothered that she wasn't punished for failing to act. If she was punished, she could forgive herself for the incident, as harmony has been restored. But now, she cannot. 

12:19 "But Teacher once called them 'sinners' ... I never forgot that". What does this mean? Also they are fighting the screaming ice so that the other Lustrous can sleep soundly? 

15:52 Its a training episode! Awesome, I haven't seen one of these in so long. 

17:34 She is putting crying children to sleep! The ice is screaming like crying children, and Ice is running around putting them to sleep!!  What? What is with this episode? 

19:00 Ice makes an offhanded comment about how much better it would be if Phos were to have arms like her legs. Phos takes this to heart, and thinks about destroying herself for the sake of feeling valued. She decides against self harm, but the ice has other plans. 


### And Done!

Great episode! It didn't turn out to be a training sequence like I thought it would be, but I'm fine with that. This episode further pushes Phos' character development, and I can't wait to see where this goes. 
