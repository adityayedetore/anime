## Return
### Land of the Lustrous Episode 5

I have a problem with analyzing shows that make their themes easily seen. The problem is twofold: when this is the case, I do not have to stop to think about the themes as they are presented, so I do understand them well. Secondly, the fact that I do not stop to think means that I will be sucked into a nonthinking state. This is what happens with SHIROBAKO, and it is what is happening here. To combat this, I need to force myself to stop and think about the themes at every opportunity. More than two minutes should not go before I stop, and think. 

00:00 Ruby has realized that Phos is in the ocean. The seriousness of the situation has not yet dawned on them, and they wait in the swaying grass. 

00:12 This is the sort of shot that would be impossible with traditional cgi animation. Here the camera slowly moves downward, constantly changing the perspective of the shot as we see the Lustrous gathered around the meeting table to discuss how to save Phos.  

00:48 "Or maybe Phos has completed the assignment and is working har- That is Impossible!" Even when Phos is not present we see that she is disparaged. It is no wonder that she believes herself to be of no use, when she is constantly being told that that is the case. This points to how our perception of our own value exists in our society as a reflections of other's value of us.

01:20 Diamond realizes how important Phos it to Cinnabar, and constantly makes fun or Cinnabar for it. The internal feelings of Cinnabar when she realizes that Phos may be in danger are communicated not only by her expression, but by the suddenly spiking floating poison around her. "Maybe Phos was looking for something that isn't around here. ... Like your heart or something!" Hahaha, that is great. You're awesome Diamond. 

01:41 Phos's words echo in Cinnabar's mind. Cinnabar has stopped trusting that people will be kind to her, but Phos has. Cinnabar now cares for Phos, though she'd never admit it. Phos has given Cinnabar hope.

02:37 Phos looks out at me with kindness and curiosity. She cares that I exist, no matter who I am. 

03:36 The lyrics of this opening are about chasing dreams, literal or metaphorical, and those those dreams slipping out of reach. We may never catch those dreams, "Slipping through my fingers before disappearing", but they are the things that keep us going, "Are the only things connecting me to here". 

05:09 After a betrayal, after being broken and bound, Phos yells in concern of the King when her gag comes united. She is kind, caring, and utterly amazing. 

05:48 Oh no, is the King's brother going to eat her? Oh lol, the first thing Phos says is "Kawaii". Actually the thing being cute puts into contrast that terrible treatment of it. The soul people torture it with fire, as a way of controlling the King's brother. 

08:44 Doing horrible things for good causes. The king's brother wishes to use Phos to help free his family. Interesting that this is a battle between the parts that were once human. The bones, the flesh, the soul. It seems the sense of justice of the bones is amazing, while the flesh is able to do terrible things for good reasons. The Soul is impenetrable, alien. Any battle here can be read as a battle within ones self. So what does it mean that the flesh attempts to use the bones to help save itself, to free itself from enslavement of the soul? Maybe there is nothing here, but it certainly is worth keeping in mind and thinking about

09:00 And Phos really is an angel. "How am I supposed to be mad after hearing that?" This is Phos' greatest strength, her ability to care for others. And this naivete and kindness is also the usual power of the shounen protagonist. It is just done so much better here. 

09:53 After Phos accepts the plan to use her body to battle the Soul people, the flesh King says "Are you sure about this?" and Phos responds "I am too weak to fight back anyways". This could be a statement about her current predicament, but more so it is a statement about her general condition. Phos is unable to fight, unable to work, because of the state of her body, and so believes that she is no use. 

10:06 "What have I been doing? I turn 300 today, you know. You must be so much younger than me, and look at all you have accomplished." Phos, its your birthday? This is horrible. In this show the specter of death is said to be driving force that gives life meaning. But we can see that the Bone people are not immortal like they think they are. 

11:41 "We would be better off without someone like that" Cinnabar is unable to see her own value, since her body makes her unfit to be around other people. So she believes that she is of no value. That is her concious narrative of herself. But somehow, she cares about Phos, and sees Phos' value, but Phos is just like her, her body making her unable to do work. So if Phos has value, does Cinnabar not have value too? But Cinnabar cannot simply accept that, so she tells herself that Phos is useless, and is better off gone, when she doesn't really feel that way. To accept Phos, Cinnabar will have to accept herself. 

12:22 "Even out at sea I was unable to find a new job for you. Tomorrow I'll try harder ... So forgive me." Aw Phos, you have the skill of saying exactly the right words. And trust me, they are getting through, even to the most hardheaded ones around you. 

16:34 Phos' punishment is that she is no longer given the task of working on her encyclopedia. This is interesting, as if it does effect her that would be marked change from the beginning of the series. In the beginning Phos didn't care about the work, but now she has realized that it is starting to give her life meaning. Also, the dynamic between the Teacher and all the Lustrous is pretty interesting. He treats them as children, and so in some ways they have never grown up. Their relationships (diamond and Bortz) are centered around the desire to protect and the desire to be of use, just like that between the Teacher and them. 

19:38 Phos now has a pair of prosthetic legs. This continues the theme of body perception and image, now with the added twist of disability. Also Phos has forgotten some memories. I don't see what themes her amnesia at this point could portend, but for sure in the future it will result in some dramatic moments. It is interesting that death is equivalent to loosing all your memories here. Death is in this case a loss of the sense of self. 

20:06 Before it was white butterflies that represented Phos, but now a multicolored bug of red with blue wings flies by. This represents Phos' starting to change, not only externally, but internally as well. Now Phos has become more complicated, more composed of varied pieces. 

22:08 And Phos has gained the ability to run like the wind. It makes sense, given what she has gone through. Phos is changing in countless ways now. She is no longer the same person she was before. 
