## Phosphophyllite
### Land of the Lustrous Episode 1

I've heard alot about this anime, and that it is a great example of the use of cgi. Lets see how it goes!

00:00 Interesting that the background for this starting scene is clearly _not_ cgi, and is not that outstanding. Is the point here create a stronger comparison to the amazing cgi to come?

01:20 Ah I love this playful voice acting! Not what I was expecting. I don't know why I thought the voice acting would be stiff and robotic, but this is nothing at all like that. Pho's voice is fluid, and though she doesn't move compleatly naturalisticslly, to call it robotic would be a great disservice. And the facial features here are not cgi, hand drawn, and so are dynamic and expressive. Well done. 

01:40 And we are introduced to our carefree protagonist, in her peaceful normal world. She doesn't take battle seriously, doesn't seem to take anything seriously. 

02:10 Pho looks at herself in the water, fixes her hair, and comments "Perfect! Cute! Dignified!". She is, so her playful act here draws attention to the fact that this anime actually does manage to make the characters look cute, though they are cgi. 

03:40 Introduces the teacher, introduces his power in a great sequence. With a breath he causes a gale to blow a stray dandilion floter from Pho's hair. This demonstrates control of power, and caring of Pho in one breath. Then when he hears about the other Lustrous going to battle on their own, he wastes not time nor words sweeping off to go help them. This is great. 

03:50 And these battle scenes are amazing! Its been so long since I've enjoyed a battle anime with great action sequences (shounen isn't my thing) and the few starting seconds of this one is enought to bring tears to my eyes. Please be a good show! 

05:53 Ok, and as I say that there is an info dump in a pretty boring way. Not all anime can do everything right I guess. 

12:24 Ok, we are learning more about the world through these info dumps here. Having Phos be a bit stupid, and make an encyclopedia is proabably a good way to excuse the narrative act of telling instead of showing. It seems that the Lustrous are basically immortal, and can be put back together again after they break. Phos is an especially fragile Lustrous, and is clumsy at that. That is why she hasn't been given a job before, those two facts have probably lead to her naievity and sense of complacency. But there is a way for the Phos to be perminantly damaged. One of the Lustrous, Cinnabar, extrudes a poision that kills the living minerals that make up the Lustrous's bodies. So she has been exiled to a useless job, to patrol at night when no enemies appear. The starting scene takes on new meaning now, as does Cinnabar's words. "Ah. Time for work". Phos and Cinnabar are two sides of the same coin: One given useless becuase they are to fragile, the other given useless work because they are dangerous. When the poisin of Cinnabar kills the minearals of the body, the Lustrous loses the memories stored inside. 

17:51 Lol they are explaining it. Seems this show just doensn't want to be subtle, does it. 


### And Done! 

A great start, in spite of all the explaining. 
Phos is caring and kind, and her state of constant fragility combined with the lack of consequences for breaking means that she has lost her sense of self preservation. 
She is the perfect foil for Cinnabar, who has given up on connection due to the pain that it causes herself and others. 
Cinnabar wishes to go to the moon, to die and not trouble anyone anymore, but even that she is unable to do. 
Both Phos and Cinnabar struggle with their place in society, but while Phos has been gifted with good looks, Cinnabar is a monster and is therefore alone. They might both think of themselves as useless, one for a lack of skill, the other for a lack of social acceptance. 

