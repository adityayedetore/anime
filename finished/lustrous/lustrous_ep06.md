## First Battle
### Land of the Lustrous Episode 6

02:32 Breaking into pieces is normal for these Lustrous. Decapitation is inherently horrific, and feels wrong to watch, but here it is not treated as funny or serious, but part of life. While running, one Lustrous is struck in the neck by an arrow, and her head flies. The next scene is of her laying on the doctor's bed, the gap between her neck and her head looming. To us it is horrific, but to them it is normal. 

03:59 The weariness of immortality has seeped into the bones of the oldest Lustrous. "All my friends are gone, to the moon. I am 3000 years old. I don't even remember why we are fighting anymore" she says, piecing yet another of her friends together again. She spreads glue on the severed head of her friend, something horrifying that has become mundane. 

08:42 "I have thought this odd for some time. Why do you so wish to fight knowing that you are not suited for it?" The music stops. It can sense how important the next words are. "... That's simple. I want to help because I love you, Teacher." The relationship between the Teacher and the Lustrous has been that of a parent and a child, a paternalistic one. But these words instantly change the whole dynamic. The relation is now closer  that of the General and the Soldier. But that is not quite it either. It feels like nationalism, the love of one's country and desire to protect it. Phos wishes to protect the Teacher, not herself. 

10:26 The traumatic time at sea is hiding in Phos' memory. Her new found power came through the price of a terrible ordeal. Betrayal, knowledge, wounds. Though they are beautiful, her legs bear the scars of the incident. 

11:08 "Humans". Is the Teacher human? If so, how has he become immortal? What happened to humans such that they became this way?

14:28 The Lustrous predict the chance of the attacks of the Souls at the same time as predicting the weather. The Souls are a natural force, like rain, not to be feared or hated, but to be accepted and lived with.

15:37 Phos' first day at on the front is anticlimactic. The day is spent looking at bugs and plants and animals, until it is time to go home, by which time Phos' constant panic has drained her. The second day Phos and the Amethyst twins collect nuts to create glue. Phos can't relax, and feels completely wiped out by the time she gets home. The third day, Phos is resigned, too tired of being stressed all the time to move. The mundane nature of what she has seen of the job so far has lulled her into a feeling of boredom, and she lies in the grass just like she did in the first episode. When the Amethyst twins suggest sword practice, Phos says "What I want to practice is drawing my sword and tossing away the sheath. So cool, right?" Actual battle is not on her mind. Phos is still young, and has yet to see someone be taken to the moon, has yet to see someone die. She is taking this seriously, but with no experience she still can't appreciate what it is to do battle with your life and the life of your loved ones on the line.

18:30 The Souls arrive, a menacing blot on the horizon. But this is no cloud. The Souls are more horrible than we have seen before, lit from below, and terrifying. Phos can do nothing but watch, as Amethyst's screams echo through the island. Horror and despair rain from Phos' eyes, as she sees what they are fighting. She knew this was not fun and games, but oh, this is so much worse. 

## And done!

I think I was slightly afraid that the fact that Phos gained some power means that this anime would turn into a self insert power fantasy, but I shouldn't have worried. 

One thread running through this episode was the mundanity of existence even in the face of horrifying circumstances, and the false sense of ease that can lend the uninitiated. We see this in the description of the Soul's attack as weather, Gold's weariness of battle, and in Phos' first two days on the front. 
