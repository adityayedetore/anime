﻿## YURiKA - Kyoumen no Nami
### Land of the Lustrous Opening Theme Lyrics
Nemuri no te tojikomerareteita netsu ga          |  The heat held within the hand of sleep
Mizugiwa ni tadayotta sono iro                   |  Floated at the water's edge, emitting a color.
Yukisaki o wasurete                              |  Forgetting their destination,
Hibi ni toketa musuu no tsubu ga                 |  Countless particles dissolved into each day.
Yotsuyu uketomeru utsuwa                         |  With a container to catch the sun and dew
Kaketa hitokakera sagashiteru                    |  I'm searching for the single missing piece!
Yuudachi ga nazukerareta                         |  The moment the evening showers
Sekai o hagashita toki                           |  Are released from the world that named them,
Sore wa nami no you ni                           |  They'll become a wave,
Yubi no sukima o surinukete kiete                |  Slipping through my fingers before disappearing.
Katachi o kae kurikaesu yume dake ga             |  Dreams that repeat, changing form,
Watashi o tsunaideiru                            |  Are the only things connecting me to here.
