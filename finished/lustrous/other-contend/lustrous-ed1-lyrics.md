﻿## Kirameku Hamabe(Glittering Beach)
### Land of the Lustrous Ending Theme Lyrics
shiawase na toki ni me o samasou                              |  Let us awaken to a happier time
asa ni kidzukanu sukima de ne                                 |  In the moments before we realize it’s morning.
tsuki wa itsu no hi mo sugata kaete                           |  For the moon changes shape each and every day,
yami mo keshite shimau kara ne                                |  Extinguishing the darkness.

ariamaru mirai wa shiawase ka dou ka wa wakaranai             |  There’s no telling whether our many possible futures will be happy or not.
kagiri aru kako yori ima o erande okiagarou ka                |  So instead of living in our limited pasts, shall we rise up and choose this moment?

shio to suna ga uzumaku tokoro                                |  In a place where the tides and sands form a pool,
kako to mirai ga te o futte                                   |  The past and future wave farewell.
mizu to tsuchi ga kasanatte                                   |  If water and Earth could overlap
kirameku hamabe de ima issho ni iru to tanoshii darou ne      |  And we could be here together on this glittering beach… I’m sure it would be so pleasant.
