### Antarticite 
#### Lustrous Episode 8

The painful void calls to Pho. It speaks of self mutilation for acceptance. The ice swallows Pho's arms. 

The title of this episode draws attention to the ice gem. They have spent their time looking for Pho's arms, but hasn't found them. Both Antarticite and the Teacher blame themselves for Pho's injuries. If Pho did not mean to injure themselves, then Pho must feel horrible that others have to once again cover for them. But if Pho did deep down mean to hurt themselves, then both Ant and Teacher are taking away their agency. 

The lustrous are born from the rock. Micro organisms wash up from the deep sea, and form crystals in the rock. Many are still born, and only some become the Lustrous. 

The Souls long for Teacher's humanity. They worship him, desire him. 

Ant's last act is to quiet Pho, as they quieted the screaming ice. 

Pho finds the courage to fight back, but it is too little too late. The souls carry Ant to the moon. The lustrous can always put themselves back together: that is a lie.  There are some wounds that cannot heal, and death cannot be undone. Pho is always loosing more and more of her body, her memories. The inevitable end is on the horizon, and to dust she will return. 
