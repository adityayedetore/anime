
Yamanaka Koyomi
Chise Asuka

They are still going with the mundane vs the fantastic. Here there was a real Kaiju fight, but the most distressing thing for the students is that the classes changed schedule. 

It is partially about how passion for these mechs can bring people together. 

Hm. There are two things to note here. Her parents are constantly arguing, and also, she thinks of his face. It isn't explicit but I think we can infer that this is the first time she has actually seen the face of her victim. And that makes all the difference. Now she feels sorry, and that is the first step towards healing. 

And it seems that it goes further than that. She admits that her sister died at that flood gate. 

