A place to cling to 

This episode starts out with Hachimaki and Tanabe back at work. Tanabe asks Hachimaki if his hospital visit was ok, and suggests that he probably had fun because of the girl. Looks like she doesn't know about the other astronaut, or about the details of the girl. So this is going to hang over her head, making her feel a bit jealous, and feel like she is unneeded. 

Ah, Tanabe sees the best in people, and she is idealistic. This probably means that if something bad is actually happening, it will be all the worse for her. Not that this episode is going to go there. 
