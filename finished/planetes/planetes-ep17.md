His reasons

This is going to be about the conflict between work and loved ones, especially when work takes you away from them. This whole show has been very gung ho about work, like that last episode, so it is nice to see them looking at it from the other side. 

This is about the reasons that people are pulled away from their work, from doing 'amazing' things. Claire is being demoted, Hachimaki's father is trying to leave. 


