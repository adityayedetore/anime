My favorite episode so far. While I do like all this money incentive stuff, I like even more when this show focuses on other things. This episode was about accepting the world you live in as beautiful, and choosing to live in it. It's about loving a place so much that you want to die in it, vs loving a place so much that you want to live in it. 

I guess romance is just my thing. 
