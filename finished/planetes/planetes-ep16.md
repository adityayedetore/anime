Ignition

Oh wow, so this episode is focusing on PTSD (called spatial loss disorder). 

And the possibility of loosing your livelihood based on that. 

The seduction of giving up, settling down, and turning your dreams into excuses. 

He sees the results of all those people reaching out their hands in the dark, and it shows him that he can succeed. 

The battle against impossible odds is framed as a battle against the self. 
