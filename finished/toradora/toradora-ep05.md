Hmm, he is basically a parent to his mom. 

I love this opening, but it's so sad that the audio is such low quality. 

How we look, our physical characteristics, are sort of intertwined with who we are. We rebel against them, try to change them, accept them, and they change how people see us. As teenagers, this is an even bigger part of our lives.  

Ah, so people grow jealous of her very quickly, so she does this as a way to preempt that. Again, it's about perception, being percieved, who we are, who we try to be. 

They pretend to be angels, but are not good. 

Things do hurt her on the inside, but she is strong emotionally. She has to be, since she has such strong emotions. If someone is trying to hurt her, she just gets angry, and that acts as a shield. 
