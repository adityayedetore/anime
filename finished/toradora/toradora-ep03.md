They are pretty married. 

"Guys are supposed to..." Hmm this is doing something with traditional gender roles. Takatsu is on about cleaning laundry, and Kushiadea is playing baseball. And the actions of the other students are like a performance. 

What he doesn't like about himself is the way that he looks. He doesn't like the fact that he constantly makes people afraid of him. What he likes about her is that she constnatly looks happy, and makes others look happy as well. 

"Your health comes first, after all." Is she saying that she is selfish? 

He likes looking. But really, when they were talking, he had no interest in her. It really just is about the way that she looks. 

This bird thing is about trying to tell people who you are, but being unable to express it in words. 

"What you wear won't make a difference" She really doesn't care about people's perception of her. But really that is her blind spot. She isn't able to tell what people are thinking at all. Ryuji, on the other hand, cares overly much. 

She is willing to do things for other people, but isn't able to express herself. 

We are getting stuff about letting people know about your self, looking at people, and really seeing them. We all want to be seen for who we believe we are on the inside. But Kushieda is a living example of the ambiguity of the line between what is inside and what is outside. She is able to put on a smile even in tough times. People can change, they can work towards something that the are not. 

Our own sense of self, our perception of others, our struggle to improve, our desire for our 'true' self to be seen, all of it is in flux in these teenage years, and clashes with others who are struggling with the same things. 
