He covered his mouth, not his eyes.

Damn, after all this, the thing that gives me shivers is Inko-chan saying his own name. 

Holy shit what a good anime. The romance of all romances. 

Themes:
- The (intentional and unintentional) blindness of youth
- Running away from the desires of others and your own desires
- The desire to be perceived as your true self, and how that intertwines with the desire to change. 
    - As an idol, Ami was always idolized, alternately as a stereotypical celebrity, then as an adult. At first she believed that she had to 'hide' herself, in order to fulfill that perception, while on the inside being the absolute opposite. But then after seeing Aisaka's strength (which was actually her brittle exterior), she decided to fight that hiding of her self. She likes Takasu because he happens to treat everyone around him like they are his children, and so he never idolized her in the way that the others did. 
- The desire to be depended on. 
- The power dynamics of a relationship, and how a good relationship is both give and take. 
- The blindness of youth. 

Wow, toradora was amazing. Hold up, let me check what I rated it. Yes, I rated it a 10, where it should be. 
