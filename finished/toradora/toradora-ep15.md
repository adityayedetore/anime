Expectations, rebellion. 

"People who think someone is bound to help them if they shout loudly are very lucky." 

The blindness of youth to problems is exactly what allows them to help in certain situations. Ami can see what the problem is, and believes that it is frivolous. But the fact that Ryuji and Minori are not able to see the problem means that they will work towards it with all their might. In the same way, Ryuji's blindness to the fact that people have problems and need to change makes him always try to help. He never sees someone as broken, or to be fixed, but rather accepts them for who they are. 

Also doing something with those stars, we can't see the ones who are actually close to us well. 
