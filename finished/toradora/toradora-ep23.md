The desire to be needed. 

This is pretty amazing. Exactly what she hates about herself, that she pretends to not hear things, and forgets things on purpose, now that she sees it in others, and the pain that it causes, she herself is feeling pain. 

---
Rewatch:

Desire to make people see, fear at seeing the truth. 

He calls his mother by his first name. 

The desire to be special to someone. 

"The desire to blame others for the inabilty to go after what you want". 
