He's mad because Taiga is working so hard for Maruo's sake, but his problem so frivolous. 

People wait for a push to change?  

Ultimately, Maruo's reasons for asking Taiga out were not at all love, and he turned fully towards the next person he set his eyes on. 

"I also want to be a idiot who only knows how to move forward". Moving forward here is not  self change, per se. It is more like a lack of thought about consequences, or the future, or reflection on the past. 

This arc was about the strength/weakness of youth, in that they are unable to see that others' failures are the true cause of their own pain. So they never blame those in pain, or seek to change them. Takasu is a gigantic example of this. If he sees someone hurting, he'll rush to their aid, and never think that they might be the cause. But the ones who are grown up, like Ami, are able to see these things. She is even able to see that Minori's guilt (over what is not yet clear, though it certainly has something to do with Taiga and Takasu). 


