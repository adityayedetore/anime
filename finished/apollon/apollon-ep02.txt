Look like this is about how our relationships of love of things can change us. Mainly the love of music, but I
'm thinking that good ol' traditional love is going to be thrown in the mix as well.

Oh yeah, I forgot to comment on the fact that Apollon's (is that his name?) love of music started with his father, who played the piano for him. His father then probably died at sea. He was a sailor. 

And you have him running up and down to do this. Well, there goes his hatred of slopes? Maybe this is saying: life is a series of uphills and downhills, and though the uphill might be tough and the downhill might be scary, they are both a part of what makes life worth living. 

I like the progression, yes, but isn't it going a bit too fast?? He's already running down the slope! This is the second episode! 

Of course, here is the problem. 

And the problem lasts for about two seconds. 

Oh well, it was enjoyable at least. 
