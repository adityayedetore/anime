# A Normal ... Girl
## Sankarea episode 4

Little did I know, there does seem to be a good amount of depth to this. Guess I'm not dropping this, for now at least. 

04:31 The maids of the Sanka household discuss Sanka Rea's absence, and Sanka's mother walks in on them. "Sanka's mother is frustrated that her husband wont pay attention to her". Not the most elegant way of communicating this, but sure. Now I realize that Sanka's mother and Wanko have something in common. They both want attention from someone who is obsessed with something else. This show, at the very least, is about the effects of obsession. Also, both Wanko and Rea's mother attempt to use their sexuality to intice the attention of the man they like, but both fail. I'm not really a fan of the way this use of sexuality is also a excuse to throw fansevice into the show, but oh well, having decent themes with fanservice is better than no themes i guess.

04:58 Rea's father's obsession with her could also be imagined to be related to the fact that to him, Rea cannot be lost. Since Rea is of his blood, there is a bond there that cannot be broken. I can't really see much textual evidence for this, but it is possible I guess. While depressed in the bath, what he says is "She got up and walked away from me". Here, Rea's father is experienceing loss of a loved on, sorta like Furuya has lost his mother. 

07:09 Furuya is not treating this situation as he would if he were to truly believe his own stated desires for a risen girlfriend. "Listen! Do you understand? You died!". There is none of the realization that the situation that he is in is his 'dream' situation. This is an indication that his stated desires are not what he actually wants.

10:09 "Honestly, she's hot enough the way she is" His desires for a zombie girl really aren't lasting long, huh?

11:30 And this plot devlopment really plays into his character problem. Rea says that she wants to experience all the things of a normal girl. If Furuya is able to enjoy these things with her, if she were to leave, maybe that will cause him to want to experience these things with others.

13:31 "What is going to happen to Babu now? Is he going to be immortal?" Wan asks Furuya on the riverbed. This brings up the problem of mortality with zombies as well. It begs the question, will Rea be immortal? Her immortality, or lack of it, is something that surely will worry Furuya, as (at least I think) the cause behind his desire for a zombie girlfriend is not her, but the fact that she is immortal. "That is a good question." Furuya says with a sober look on his face. 

14:44 Furuya is only now realizing that Rea is a zombie, and a girl, and staying in his room. Again, the fact that he is so slow in realizing this indicates that what he believes is his deepest desire may not be so. 

17:56 Zombie grandfather is actually pretty intersting. He wanted some woman named 'Sada' to come back to life, so he created a potion of resuruction, he succesfully used it on himself, but for some reason it did not work on her. And maybe being a zombie means that your memories will slowly erode. If that is the case, thats sad, but I can deal with it. That would be a pretty great story, I think, and would fit in nicely with Furuya's character arc.

19:12 His sister is actually pretty cute. She is constantly trying to get Furuya's attention by appealing to what he says he desires, but since he doesn't actually want those things it is not working. In episode 1, she dressed in the attire of a dead person to get his attetion, but that didn't work. Then now, she pretends to be a suregon just back from a surgery and with blood spattered gloves, but that doesn't work as well. Unfortunatly, the fact that furuya's obsession is not a true obsession comes into conflict with my idea that the show is depicting the differnce between benign and malignant obessions, but I think this is better, so its all good.

20:30 "My dreams are coming true, so I lost myself there. Oh, and you dont you hurry and get dressed", Furuya, you are clearly not too attached to your dreams. Neither are you very contoled by your base desires, it seems. 
