# Childhood ... Friends 
## Sankarea Episode 7

00:00 Ah, this episode starts with a scene we have not yet seen, and I am alrealy liking this episode so much more. The scene is of Ranko walking with the clothes and things for Rea in her hand. She asks herself, "Why is Furuya asking me to do this?". This question is really a stand in for, "Why do I like Furuya so much?". The shadows of a young ranko and Furuya run by, and we know that and the answer to that qustion will be in Ranko's past.  

01:05 The childhood friend is such an overdone trope, but I guess it happens to everyone. It's happened in one way or another to me, has it not? 

04:00 Hmm, both Furuya and Ranko's fathers reactions to the questions of Ranko's mother are to be silent and not react. What does this mean? Do they know something that Ranko and her mother do not? Are they not on the best of terms? What is going on here? 

04:47 "When I first met Furuya, I was in kindergarden". And here starts the flashback. The young Ranko was scared in a graveyard, and Furuya came along and rescued her. And we, the viewers know what he was doing there: probably visiting the grave of his mother. Furuya says "I'm not scared at all", and it is probably true in a way. He has repressed his feeling about his mother's death, and cannot see the fact that her death is what is driving his interest in Zombies. 

07:00 I guess Furuya liked zombies before his mother died. This compleatly defeates my theory that he likes zombie girls becuase they cannot be lost. Don't know what to think now. 

09:40 Hm, I guess this is an sort of inversion of the childhood friends trope. Ranko liked Furuya at first, but then disliked him after learning more about him. But still, she does dislike him. Now, we are to wonder why she has grown to like him again.

20:30 All the characters here seem to have weird obsessions. Ranko is in love with her cousin. Furuya professes to like zombie girls. Rea's father is obsessed with conroling his daughter. 
