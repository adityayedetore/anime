01:10 What brings a girl to scream down into a well in the middle of the night? What is this boy doing surrunded by candles in the dark? Why does that boy have cat ears?

02:50 'Ideal' blond woman in movie, protagonist is not interested. Creepy zombie, he pauses the movie to take a closer look. 

03:11 I dont like the angle at which this scene is shot. It leaves open the posibility that the protagonist is masterbaiting while watching the movie. If this is intentional, either they should have been more obvious, becuase this is not at all clear, or just excluded the possibility if they were scared someone realize it. Hmmm, just took a look at the first few pages of the manga, it seems pretty bad. I guess I can complement the creators of this anime for improving so much on the source material. Really, why did they decide to adapt this shitty manga? It really is terrible. 

03:33 This is pretty gross too, but its not nearly as bad as the manga. The manga feels exploititive of women, the anime instead portrays the main character as young teenager with weird fetishes, but otherwise is fine. 

04:38 Lives in a temple. Why? Is it because zombies are the anthesis of being in a temple? Does a temple have something to do with the afterlife, and therefore zombies? To really be able to dig into this deeply I probably would need to know a bit more about japaneese religious practices. 

05:26 His Mother passed away when he was a kid. Is that where his obsession with death comes from? 

06:54 His cat dies. He has a funeral for it in the backyard. And I see now, temples are where they hold funerals, or at least the place where they conduct the funerals. This kid really is surrounded by death. 

07:03 "In fact, he shouldn't be dead. It's not possible" He is in denial. Unable to accept that death can enter so quickly, and so suddenly. 

08:19 Denial of death of a loved pet is probably something many of us can empathize with. Though they may not be portrayed too strongly, I understand his feelings. 

12:01 Introduction of this 'Wanko' (cannot believe thats actually her name) is so so so much better than in the books. You can tell she is willing to do all this for the shrine becuase it would give her the chance to talk to cat ears. 

12:29 Its a cliche, but the thick-headed protagonist who doesn't see that a girl likes him is done well though not subtlely in this scene. The blond girl (I refuse to call her Wanko) is clearly willing to be touchy feely with cat man on his bed of all places, but he asks about another girl. The pause, and the dark lighting in this zoomed back shot all emphaize feeling of realizing the person you like likes someone else (can't put a word to that feeling, maybe there isn't one). 

15:37 Desire to be free, desire to be like other teenages. Embarrasment of parents especially seems like innocent teenage feelings. But what I know of this anime means that I really can't see them that way. It's gross. Maybe what is going on here is an comparison of innocent perversion on the part of the cat eared boy to perversion on the part of the father? One is strange but innocent, the other is masquarading as love and has terrible consequences. 

16:17 Dunno why, but I have a soft-spot for clean animation like this. 

17:31 "I want to die and come back as someone else". Again, this is framed like a normal teenage hyperbolic wish, a reflection of embarassment. Metaphorical language. But what I know of the anime means that this sentence takes on a more literal meaning. She does die, and does come back as someone else. Also, I understand that this anime is partially about teenage perversions, but did they really have to keep in the line "I'll do anything"? It doesn't feel real. Unless japananeese girls do say that, which is even more of a problem. 

21:11 "I like only zombies... Do you buy that? Well, its true". And here we ask, is it? Is he not saying he prefers zombies to real people because he is afraid of something? Of rejection? Of loss? Or is it something else. Overall this episode seemed to be filled with what seem to be the uaual candidates for teenage issues. Obsession with death, perversion, girls, relationships, opressive parents, desire to be like others, feeling trapped by family, etc etc. Most of these issues are played off as humorous or not that serious, but knowing the story, some of them will turn out to be real serious. What is being said here? 

21:20 REWATCH: I think that this could have been alot more explicite, but the reason that the only relationship Furuya wants with a girl is one in which the girl is a zombie is that he is afraid of loss. He is haunted by the ghost of his mother's death (speaking metaphorically, of course). 
