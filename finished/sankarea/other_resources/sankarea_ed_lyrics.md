# Above Your Hand
## Sankarea Ending Theme
### Original / Romaji Lyrics     English Translation

Hitotsu dake kanau no nara                                   |  If I'm allowed to have only one wish come true,
Kimi no te no naka                                           |  then I wish to always be
Yureru chiisana hana de itai                                 |  the tiny flower swaying in your palm.

Hajimete miru mabushii kono sekai de                         |  Opening my eyes for the first time to this dazzling world,
Kimi to moshi hagureta toshite mo kitto                      |  even if I should lose sight of you,

Mitsuke dashite                                              |  I will surely find you again.

Mezametara                                                   |  When you wake up,
Soba ni iru yo                                               |  I will be right by your side.

Hitotsu dake  shimatta mama                                  |  There's only one thing I've put aside out of your sight;
Kimi ni tsutaerarenai omoi kakaete iru                       |  I'm still holding on to my feelings not yet conveyed to you.

Hateshi no nai ooki na jikan ga                              |  Even if the boundless ever-great time
Itsunohika futari wakatsu toshite mo                         |  should someday pull us apart,

Mitsuke dashite                                              |  I will surely find you again.
Nando datte kimi o yobu kara                                 |  I will call your name again and again.
Sono te ni dakareteiru                                       |  Even if I should no longer be
Chiisana hana ja naku natte                                  |  even if I should become a formless wind,
Kimi ni mienai kaze ni natte mo                              |  the tiny flower held in your hand,
Kienai  afuredasu omoi                                       |  my overflowing feelings will not disappear.
Owarinaku  koko ni aru                                       |  They will forever remain in this place.

Mezametara                                                   |  When I wake up,
Soba ni ite ne                                               |  please be by my side, please,
Ashita mo                                                    |  for tomorrow?
