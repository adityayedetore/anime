Wow I dropped this show a long time ago after watching up to episode 11. But is is actually amazing how good the last episode is. It is a takedown of a form of Paternalism, and it does it gracefully and beautifully. And then it has set up a discussion of toxic relationships, and living life to the fullest, and dealing with a dying loved one... All of this is pretty amazing. The setup has so so so many possibilities. 

I think there are things I didn't like about the middle of this show, but I've forgotten them, tbh. Now I'm left with the good memories, and really, that is how it should be. 
