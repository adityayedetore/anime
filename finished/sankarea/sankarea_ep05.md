# If she's a Zombie ... That Means ...
## Sankarea epsisode 5

00:00 The beginning of this episode is a reiteration of the conflict that reared it's head in the end of the previous episode, though it is phrased in a bit more dramatic terms. 
While it is good I guess to be reminded of the conflict, I can't help but wonder if the anime would have been better had it trusted its viewers to remember what the conflict was. It feels to me like lazy writing, but maybe it is beneficial, I don't know.
The conflict is that even though Rea is a zombie, she will not last forever. Furuya has to find some way to preserve her body, and this feeds into a possible coming realization of his Lie. 

02:43 A reminder of Furuya's dead mother. As Furuya's Ghost, it is appropriate that we are reminded of her here. 

05:01 Ok, his sister sleeps in the grave cloths? What is going on here? 

07:03 Furuya is struggling with the fact that he doesn't actually take as much pleasure in being in this situation as he thought he would. One with his proffesed desires would ostensibly take any opportunity to spend time with the zombie, and to do other things, but he sleeps alone, and covers her body up.

15:12 Ok, her name is Ranko, thats a bit better.  

19:16 Dramatic irony for the rest of the episode, didn't really enjoy it, nor the stupid fanservice. I, but am not sure, that this episdoe is not as complex as the previous. 


