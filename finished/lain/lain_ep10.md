### Serial Experiments Lain episode 10
### Love

I've never felt silence to be as unsettling as with this opening. Is silence now a lie? 

The man who created Lain, the man who was found to be dead after inventing the way to permanently connect to the wired world, is apparently god. So Lain is the child of god. 

One part of lain has encoded herself into the wired world, so that she can live there forever, and control the workings of the world. As such, she no longer needs a body. That is exactly what Chisa did. She encoded herself into the wired world, so no longer needed a body. We are being asked the question, what is the point of our bodies is we live our lives through the wired? Do we not wish to escape our bodies, and become something that can escape death? But the only way to escape death is to die. Its a contradiction.

Lain goes to school. She is the victim of cyber and real world bullying. Her classmates, even her teacher, are pretending that she doesn't exist. Her classmates because of what she did to Alice, and her teacher because of she used her phone in class. 

Her entire life was an experiment, she was a test tube baby. Her father is now leaving, leaving her alone. The connection through the wired is not what Lain wants. She wants connection to people in the real world. 

Identity discovered, and they commit suicide. This is about revealing identity online, and the effect that can have on a person. 

Lain was not born, but created by this man who wrote himself into the code of the wired world. Since she was created in the wired world, then given a body, she is almost like a god of the wired world. 

If existence is defined by connections to other people, then where do we exist more than in the wired world? And if no one cares for us in the real world, then do we only exist in the wired? And if so, what is the purpose of our flesh and blood bodies? 
