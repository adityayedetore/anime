## Distortion
### Serial Experiments Lain Episode 5

02:02 Things that I notice in this opening sequence: Lain seems to be trapped inside the screens. But it is not only her, her name is trapped along with her. Crows usually symbolize death. They also symbolize freedom and escape. Time stops near the end of the video. The drugs/pill that the poor man took makes time dialated. 

04:02 "If you can hear it, it is speaking to you. And if you can see it, it's yours" These are the words of the opening voiceover, which thus far seem to usually touch on the theme of the episode. The words, "if you can hear it, it is speaking to you" are not exactly ironic, but rather painful in the context of the constant murmer of conversation that this show swims in. It is not true. People are speaking, saying things, but in this show no one really speaks _to_ another. 

03:42 The voiceover here states that the wired is a way to escape humanity's inability to change or to evolve.

04:24 Lain's sister is cold and apathetic. She has spent the place at her boyfriend's house, and compelatly ignores him when he wakes. There is an accident on the street she just crosses, and she barely spares a second glance back. It seems there is something wrong with this girl. She doesn't care about death, or anything really. 

05:18 Next we see a scene from Lain's childhood. Is the fact that this scene comes immediatly after Lain's syster's scene meant to imply that Lain is somehow responsable for her sister's apathy? It seems she has seen ghosts and apparitions since she was little. The ghostly form of some doll hovers in the darkness of Lain's room, while lain sits in the light, her stuffed animals on the windowsill as ominous as ever. Lain is talking to the inanament, the dead, with a delight we have never seen from her before. Does her obsession with the afterlife come from the fact that she has always been able to speak to the non-living? But this scene is ambiguous. We don't see the doll move at all, don't see it's mouth when it answer's Lain's request for a story. Is lain dreaming this up? Hallucinating? 

07:04 We hear that there has been an incident with self driving cars, on the very road that lain has been standing on, and the one that her sister crossed just before the accident. 

11:46 Lain seems to have some kind of strange abilities, to commune with the dead, and to control electronics. She has been appearing in the electronics, and causing all sorts of havoc. Her goal is to merge the wired with the real world. But she is not self aware it seems. She doens't know that she is doing this. Is this a comment about the fears of a creating a self aware system? Has the monster that dwells in lain simply not yet devloped conciousness? 

13:10 It was a doll, then a mask, now her mother. Is what she is seeing becoming more human? "It is reasonable to see the Wired as an upper layer of the real world. In other words, physical reality is nothing but a hologram of the information that flows through the wired." Now that I think about it, this is kinda true. Are we really our skin and bones, our flesh and blood? You can take the peices of a human and put them in a vat, but no human will ever be born. We are not only the peices that make us up, but are the relationships between those peieces. And our brains, where we think we truly exist. If there were some other thing, some machine that could perfectly model all the synapses and neurons, wouldn't that be the exact same person? Thus it is not the flesh and blood that makes us up, but rather the information that the flesh and blood contains. 

13:12 "Are you my real mom?" Again we see the ambiguity. Is Lain speaking of the apparation she sees? In that case, does she mean that the mother that is downstairs is not her mother? Or is she asking if the mother that she has known all her life is her actual mother? Is she asking an apparition, or is she asking her real mother? 

20:05 This is the way to do horror, not that shitty stuff from the last episode. Lain's sister is seeing things, seeing the command to "fulfill the prophacy." Oh shit, Lain's sister has turned into a ghost? So it seems that the ghosts that Lain sees are the people who are being killed and replaced. Is it the wired that is eating them up? 

22:29 "Who is it today?" Lain's eyes stare out of the screen, seem to stare into my soul. Is she asking, "who is going to fall to the onslaugh of the wired world today?" And it seems that it will be me, the one staring into this computer screen. You're next. 

### And Done!

Amazing ep. I loved this one. We get to see the consumption of another, this time Lain's sister. And it seems that the ghosts that Lain sees are the souls consumed by the wired. Lain's mother, a doll, and her father, a stone mask, have already been consumed long ago. 

We are still stuck with the ambiguity that is Lain. How is it that she doesn't know that she is the driving force behind the invasion of reality? Has she already been consumed, and it is the ghost of her soul that is doing this terrible work? And we are still left in the dark about how Lain became able to see ghosts. What side is the concious Lain on? What is she doing in the cocoon of wires she has spun in her room? She doesn't seem to know that she is leading an coup on reality, but does she?

There are some things about this episode that I don't quite understand. What is up with the prophacy? It seems that there is someting being said about time, but I am not sure what it is. Is fulfilling the prophacy the act of dying? Is it talking about how everyone dies? I am not really sure. Also these first words, "If you can hear it, it is speaking to you. And If you can see it, then it is your..." I dont know what to make of them. Based on everything in the show so far, they seem blatantly untrue. Nothing in this show speaks to you, and nothing is yours. 

