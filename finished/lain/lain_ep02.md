## Girls
### Serial Experiments Lain Episode 2

00:00 This episode starts the same way as the last, with the words "present day, present time, hahaHA-". 
We see lain reflected in the glass of a fuzzy computer screen. Or is she trapped there, looking out at us? 

03:00 The first scene is of a party, with people dancing and surrounded by fluorescent lights. These are people who have been consumed by the monster, or are well on their way there. "Cyberia? I don’t come here because I _want_ to." and again we see the words in the multicolored flourecent liquid. What do these words mean? What is "Cyberia"? A pun on "Siberia"? A barren and cold place, housed in the faux world of the internet? 

03:37 "We call it Accela" A drug, then. "Its not exactly a drug, but you have to jump though the same hoops to get it". Inside of an organic shaped capsule the actual 'drug' is a black, spiky piece of electronics. This drug is a piece of technology, and this poor man just swallowed down. I have a feeling this is not going to end well for him. 

05:08 Looks like this drug makes the world seem like it is filtered through some technology screen. You become part of the technology world. Who is this woman that the man seems so captivated by?

05:53 What a change since last episode. Lain was previously uncomfortable with the computer, bad at typing on it. Now she is typing at the speed of light, her eyes wide and dead. Looks like she is starting to be consumed.

10:00 The girls start talking about Lain like she isn't right in-front of them. The communication here is terrible. The conversation goes is circles, since none of the girls want to ask their actual question. Was she at the club last night? And when they do ask, they cut her off before she has a chance to say anything. 

11:00 We see an ad for the drug taken by the man in the beginning of the episode. Apparently the technology resonates at a particular frequency in the body, causing hormones to be released that alter the user's sense of time, and making them hyperactive. Seems people can't get enough of the buzz of the power lines and computer screens surrounding them, it seems. They don't merely want be surrounded by technology, but they want to become one with it. 

11:26 Lain has a phone now it seems. Maybe it is because she wanted to talk to Chisa some more. Is this all driven by her obsession with death? 

12:15 While walking through the hallways of school, Lain hallucinates a ghost of a girl, someone other than Chisa. Or is it not a hallucination? Can Lain see ghosts? What is going on here?

13:15 "For communication, you need a powerful system that will mature along with your relationship with people", says Lain's father as he is setting up her personal computer. The only Communication here is electronic communication. Face to face communication isn't considered. 

22:19 Lain hasn't received mail from Chisa, even with her new computer. With nothing better to do she decides to go to Ciberia. There she meets her friends, and they have another 'conversation', which amounts to the the dropping of backhanded insults and the reiteration of information. Then the man from before, the man who took the drug, shoots a woman. Everyone scatters, but Lain stands there, looking at him. To this main, Lain looks like the woman from before, the woman who the user believes is ordering him to do things that will bring the wired closer to reality. He asks her why she is making him do this. "you're that scattered god's ..." and this is the second time we have heard the word god. Who is this god that exists in the wired? "The wired can't be allowed to interfere with the real world" screams the man. Lain then walks towards him, and as he is about to pull the trigger, she says, with a look in her eyes we haven’t seen before, "No matter where you go, we are always connected". There is no escape from the wired. Hearing this, the man eats another pill, the last pill he'll ever eat, and blood splatters on lain's cheeks. Who is Lain? What power does she have? Is she unaware of herself? And in the end, we hear the heartbeat of the techno music, the heartbeat of a watching monster. 

22:50 The ending sees Lain naked in womb of metal. Was she birthed by the wired world? 

### And Done!

Another great episode, the drama of the last scene was enthralling. Who is Lain? Does she realize who she is? Is she pretending to be a young girl, but is an agent of the monster? 

This episode marks the beginning of an invasion on Reality. The march of the monsters living in the cyber realm has begun, and their chosen form of attack is a pill by way of which they can control human minds. 

The intermediary of the computer screen blurs the lines between the living and the dead. There is little way to know if the words on the screen come from another human, or are created by the unliving computer. In a way, when looking through a screen whether there is a live person on the other end doesn't matter. 

Lain believes that the only way to escape the oppression of technology is death. That is why she is so interested in Chisa, who has died, but somehow seems to be still alive within the technology. It wouldn't be a stretch to say that she may desire to escape the oppression of technology, but is afraid that even that may not be enough. But also, she believes (maybe as her alter ego, or subconsciously) that even death is not enough to escape. Hmmm. 

The only thing I am certain of about Lain is that she is very interested in Chisa's ghost, and that she wants to know what lies beyond the doors of death. I think she dislikes the technology that surrounds her, and so has in the past disengaged from it. I suspect she believes that even death is not an escape from technology, but this doesn't fully fit with her desire to find out what happens to people when they die. What realizations can help her overcome her situation, I am not sure. Maybe things will become more clear next episode. See you then!
