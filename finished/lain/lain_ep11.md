### Infonography 
### Serial Experiments Lain Episode 11

Last time we had Lain find out that she was created, not born, and that who she thought her family was is not her actual family. 

Wha? We didn't have a voiceover this week?

I get the feeling that this episode is speaking directly to the viewer of this episode.  

No no, these are lain's memories. She knew Chisa. They were even friends. 

Lain is searching through her memories for reasons to live. She searches through all the people she knows, and the one she ends with is Alice. Alice, who she hurt so badly. 

A lot of things are making sense now. Most of what Lain sees (the people, the scenes, the objects) is ambiguous between delusions and visions induced by her connection to the wired world. She probably hallucinates. 

The clothes and body that was once an alien is now Lain. To lain, her family is alien. To Alice, lain is now someone that she doesn't recognize. 

A recap episode, probably would have been more necessary had I not been talking such careful notes. 

We didn't learn much new this episode. Some things were cleared up though. So here is the story. The evolution of the wired world has gotten people hopeful about people being about to make better connections to others. But the wired world only provides false connections, and comes with its own problems: misinformation, invasion of privacy, ease of access to adult information, obsession with the ease of connection, cognitive dissonance between online persona and everyday persona, and more. Some time ago, a scientist discovers a way to interface the brain with the wired world. He is killed or commits suicide, but encodes himself into the 7th gen internet protocol. At the same time, he (or someone else) creates lain to exist in the wired world, and gives her a physical body. She is then sent to live with one of the people who worked on the technology. They are raising her to be able to connect better to the wired world, and finally give up her body in the end. Lain experiences her own whole host of problems, including hallucinations, Dissociative identity disorder, and social anxiety. While she facing her problems, a group of network specialists form an organization called the Knights. They seek to merge the wired and real worlds, and they worship lain, who is well on the way there. Chisa, who lain met briefly, is able to do this, and then commits suicide. She lives on in the wired world, and contacts people to tell them that she exists. Those other people include Alice, and through her Lain. Lain becomes very interested in Chisa's ability to transcend her human form, and starts to play around with computers. At the same time, lain's alters are wrecking havoc in the wired world. Lain is slowly discovering that she has multiple selves, and that is causing her problems. One of lain's alters enjoys causing the other lains and the people around her pain. Alice, who is someone who cares for lain, likes one of the teachers. Lain spreads this information around, and causes Alice pain. At the same time, Lain is being bullied in school because it is found out that she is the one who started the rumors. Lain now fully devotes herself to the task of intergrating herself into the wired, and manages to delete the information about Alice. This scares Alice. 

There are some other things about tachibana laboratories, and the children who were controled, but ill talk about them later.
