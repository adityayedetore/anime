## Weird
### Serial Experiments Lain Episode 1

Ok, I'm going to try to make sure that I explain what it is I am commenting on while watching this anime, so that my comments aren't as fragmentary and incomprehensible. 

00:00 The show starts on a visual of someone typing characters, with the English voiceover saying "Present day. And present time ... hahaha-". a-lot this means that this show is talking about not only the time in which it was made, but the today for the viewer. The static that cuts out the speaker brings to mind faulty electronics. 

01:52 "I am falling. I am drowning. Help me to breath" Great op, great visuals. We see the main character, who I assume to be Lain, surrounded by green lines and symbols, seemingly overwhelmed by them, as we hear these lyrics. Drowning in what? 

02:20 "Why don't you come here?" We hear this plea as we see sights Ive grown accustomed to as a product of the modern era and influence of Bladerunner on the cyberpunk genera: crowded streets, telephone polls and wires, neon signs, people walking together and alone. All the people are faceless, and that makes it seem like though they are together, they aren't really present. No one seems to be talking to each-other, all going about their business. 

02:47 A cut to a screen with the english words "weird    Layer: 01". 

03:03 The one person with a face is a woman leaning up against a building, in business attire with a case. She seems to be struggling to breath clutching her chest. Heart attack? Anxiety? She is made all the more alone by nearby passer-byes seemingly taking delight in her misery, laughing and pointing for the brief seconds they are on screen. It seems the first and only connection between people was when they were laughing at other's pain. 

03:38 "Why you have to do that ... is something you should figure out for yourselves" these words appear on the screen, seem to be floating in  vibrantly multicolored  water. Why we have to do what? Use the pain of others to bring ourselves closer? 

04:13 A woman and a man get handsy right after we see the business woman with the panic attack. It seems even those who are physically intimate are distant, by way of numbing substances. And their kiss is broken by the woman's suicide. She is falling, and drowning, crashing into the neon signs and telephone lines and alcohol bottles that overwhelm her surroundings. She said "I don't need to stay in a place like this", her internal words made apparent on that multicolored water. The surround is what she is escaping from, the neon lights and the buildings and the cars and the faceless people. She's left her unfinished work behind, and the keychain of a penguin humanizes her after her death. 

05:24 "If you stay in a place like this ... you might not be able to connect" we see the words on that hypnotic water again, and this time just after we have seen the telephone poles and lines that ostensibly do connect people. 

06:17 "Can't you be quiet?" Through these five minutes we have been listening to the background chatter incessantly droning above the hmmm of fluorescent lights and telephone wires. It's the background, the place, and adds to the a sense of claustrophobia. Our protagonist boards a train, and these are the words she speaks to the others. A momentary respite, a silence of many eyes. Again the only personalization is through disconnect and small conflict in the people around us. And yet that silence is not absolute as it is broken by the sounds of the train and the wires carrying people and electricity. 

07:23 One of the students in class has gotten mail from the girl who committed suicide. She is crying on her desk, faceless as they all are. The only ones with faces apart from lain are the girls who seem to be comforting her, by telling her to get over it. They leave her soon though, and move on to lain. Again, we see that the only human connection comes from a cruel response to other's emotions. The girls say "Get over it, its just a prank"

08:07 "Lain, have you gotten the mail?" asks the girl who was two seconds ago comforting the still crying receiver of the mail. "No I haven't. Im not good with computers" "You should check your email at least once a day". What good would that do her, if it made another girl break down into tears? It seems that the communication afforded by electronic messages causes pain. 

08:14 And the two girls who we doing so well at comforting her have left to speak to lain, leaving her all alone. 

08:52 Lol they are teaching c or java by writing it on the chalkboard it looks like. Of course, students shouldn't be using electronics in class, so the way they will teach programming with without computers. This anime seems to be envisioning a dystopian world (current day) filled to the brim with electronics. Also the string " printf("%C<%C  " looks a-lot like "print fuck" lol. 

09:43 The tips of lain's fingers, the parts she uses to type, seem to be emanating some kind of miasma filling the room. 

10:47 Lain has gone home to an empty house. She checked that it was empty first, before opening the front door. No announcement that she has arrived, it's an empty house, so no need right? The red computer on her desk is partially covered with objects, like Lain wants to forget about its existence, but its blood red color is demanding attention. Lain can't forget about what she heard from the disconnected classmates of hers, and wants to read the mail of the dead girl if she received it. 

11:42 Lain sounds a bit surprised when the computer asks her who she is. Has she never used a computer before? 

12:48 After receiving the mail from ghost, Lain seems to be having a conversation with her. Is she having a conversation with a ghost, or a troll? The voice says "Rumor has it that this is prank mail, but I want you to know that it isn't, Lain", says the disembodied voice, seemingly speaking directly to lain. Lain can't help but respond, asking "Why did you die?", but this is no conversation. The way that the computer asked for lain's name and is using it now suggests a transformation from a person to an electronic avatar. In some way I get the feeling that when the computer asked for Lain's name and she gave it away, she was reducing herself to mere electronic bits of 1s and 0s. Her computer name is now all she is, all that is being communicated too. 

15:42 Lain tries to bring up the fact that she got mail from a dead girl with her mother, but her mother only glances her way, and doesn't seem to respond in the surrounding. Later her father has come home, and begun to work on the many computers that lie on a table. Lain comes up to him, and he is hidden behind the computers, the electrons literally blocking the communication between Lain and her father 

17:26 I could write about how the father is using the computer to speak to headless, faceless people, and what that suggests about the actual connection that the electronic communication affords, but I don't think its worth it: clearly this anime has a bone to pick with electronics surrounding us and how it is not allowing us emotionally concrete forms of communication, and cutting us off from our loved ones. 

18:30 On the train to school, another suicide. This time, the blood drips from the wires, right in-front of Lain. Seems surreal to her, and the school day passes in a sweat soaked daze. The ghost of Chisa haunts her on her way home. Chisa was killed by her surroundings, the electronically stifled communication consuming her. And now she lives on inside it, seemingly communicating through the wires. Lain asks her "Where are you?" and her response is a fake smile, her eyes looking pained and dead. She has been swallowed by the wires, and now is a part of them. Lain has vague feelings of hatred towards all the buzzing things around her, but nowhere to direct it. Why does Chisa interest her so much? 

22:27 Ok I really loved this episode. The atmosphere of the claustrophobic sounds and lights of the modern world really rung true to me.

### And done!

Here we have the protagonist living in 'our world', where our connection to others is faceless and mediated by the electronics that surround us. 
The technology that surrounds Lain is overwhelming, and Lain wants no part of it. But the death of a classmate, Chisa, seems to have produced a ghost living in the electronic world, and the opportunity to communicate with the dead draws Lain into that world that she is so uneasy of. 
Lain wishes to know where we go when we die, and how it feels. Not a very happy anime, is this. 

I can't help but feel that the neon signs and telephone wires and railway trains are an ever present monster in this show, lurking in plain sight, and consuming people one after another. The monster draws us in with the promise of the connection that we deep down crave, but only affords us a hollow and faceless world to pretend in. The light of the computer reaches out and grabs the soul of those who use it, and reduces them too but a single name. 

Given the opportunity to connect the character in this show mock others for their emotions. No one seems able to connect to other's feelings in a significant way. The closest we come is Lain conversing with the computer possessed by the ghost of Chisa, and even that is undercut by way that the message seems to be prerecorded. Lain's attempt to talk to her father is literally blocked by the computers, and her mother has been rendered dead and listless for some yet unknown reason, but doubtless because of the monster.  