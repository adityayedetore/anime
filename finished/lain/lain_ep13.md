### Serial Experiments Lain episode 13
### Ego

Starting differently this time, are we? OK, so here is the dielemma: If we only exist when others perceive us, if a large part of our existence is other's memories of us, then what happens when we are speaking to ourselves? Who is perceiving us then? Are we perceiving ourselves? But how is that possible? 

So Alice has been traumatized by what happened. And this requires Lain to reset everything. Well... Shit. 

Sigh... she reset herself along with it. We are seeing the same world without lain in it.

"If you don't remember something, it never happened. If you aren't remembered by anyone, you never existed." 

Even as a god, lain is still struggling with her identity. If her identity hinges on people knowing of her, and now no one knows lain, then who is she? But she loves everyone, and that is enough.

Lain represents a breakdown of the division between the wired world and the real world, and the problems with identity, memory, information, rumors, apathy, body disphoria, and other things that come with it. But when she chooses to reset the world, she makes it into one in which there is no breakdown of reality, as long as she is kept inside the wired world.

In the end, its about love. The solution to the question of how we can exist is that we love others, and with that love we can exist. 

This is a show about a program created by a man who is trying to escape death and become a god. That program, lain, is struggling with a lot of things that humans struggle with, only more so because she is so connected to the wired world. She doesn't realize what or who she is, and that she is being raised specifically for the purpose of breaking down the barrier between the wired world and reality. She craves connection to others, she doesn't know how to connect. 

Ill write up my overall thoughts on the show in a bit. Let me sleep now. 

Literal events: Lain is a computer program created by a man who is trying to transcend death and become a god of both the internet and the real world. She was given a body by him as well. Lain is struggling with a lot of things, but right now her main problems are classic "to be, or not to be". She feels like her existence causes others pain, so she decides to remove herself from the world. She could choose to delete herself as well, but decides against it. She realizes that the very fact that she loves others is enough of a reason to exist. So Lain keeps a small fragment of the memories of Alice alive with her reset. i

On a literal level, I thought that Lain was a software program created by Masimi Eiri, a man who is trying to transcend death and become a god. Though Lain is a computer program, she struggles with a lot of things that humans strggle with, only more so because she is so connected to the wired world. She doesn't realize what or who she is, and she craves connections with others, but doesn't know how to connect. Eventually, Lain grows to love Alice, the one person who was kind to her. At the end of the show, Lain tricks Eiri, who is now a god, into manifesting a physical body, and is able to kill him. Alice is traumatized by these events, so Lain, now to one god of the wired and the real world, resets everything and erases almost all of herself from the collective conciousness. But now she is faced with a problem: to be, or not to be? In other words, does she erase herself completely or not? After that reset, we can see that Lain still has a tenuous presence in the real world, manifest as the memories of those she knew. So Lain has decided to not erase herself completely. One of Lain's alter ego's questions her as to why she didn't just erase herself completely, and Lain erases that part of herself in anger. In a imagined conversation with her adoptive father, Lain realizes that the very fact that she loves others is a reason enough to continue to exist. 

I mainly asked because 
o

One question for you is do you think it is a bit ironic that the slogan for SEL is "lets all love lain"

I thought Lain believed that other's love for her wasn't enough to keep her going 
