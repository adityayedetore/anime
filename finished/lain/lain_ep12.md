### Landscape
### Serial Experiments Lain episode 12

"Oh, so that's how it works. I had no idea that the world was so simple. I always thought that the world was a big and scary place, but once you figure it out, its all so easy." "See? I told you it would be." 

Well, they are being pretty clear and obvious right now. "You exist in people's memories of you. That's why there were all kinds of me". Lain has wiped the memories of those around her to protect Alice, but Alice is uncomfortable about this. Does this mean that Lain has killed Alice? Her memories might be painful, but aren't they what make her who she is? 

And now they are laying out many of the central issues of the series. And here is the "Lets all love lain". Lain is becoming more and more integrated into the wired world, and so is becoming a mythic figure. The "Lets all love lain" as a sort of religious sounding bent to it. It is probably what Lain wants, to be loved, and that is filtering through even in her wired self. But the lain they love is only their image of lain, not the actual physical lain. This advent of the computer era has people thinking less of their physical bodies. 

Hmm, lain seems to be taking revenge on the people that hurt her? Or is this just the evil side of lain that is doing this. 



"What you did was remove devices from the wired. Phones ... TV ... The network ... Without those you couldn't have done anything. " "Yes. Those are things that accompanied the human evolution." "Humans who are further evolved than other have a right to greater abilities." "Who gave you those rights? The program that inserted code synch ed to the earth's characteristic frequency into the Protocol Seven code, which would raise the collective unconscious to the concious level. Did you really come up with the idea yourself? " "Are you telling me that there really is a god?" "It doesn't matter. Without a body you wouldn't be able to understand."

So this man coded himself into the wired world, and created a program (lain) that would dissolve the boundaries between the wired world and the real world. That way he could become god of the both the wired world and the real world. He believes that he has become God, whatever a god is. But before he was god, he came up with the idea of god (the idea that would allow him to become god). Who gave him that idea? 

What am I still confused about? One thing I am confused about is why this 'god of the wired' cares so much that he is not the only god. Why does he fall for lain's trick, saying that he wouldn't understand because he doesn't have a body? 

So there were multiple people controlling the cyborgs. They thought that they were working to prevent the integration of the real world and the wired world, but they were actually hired by the guy who created lain, the guy who wants to control lain and thereby control the entirety of the wired and real worlds. Thats why they were so scared when they they found out that the wired world was being integrated. They had done some bad things, so Lain, or at least the person controlling lain, was coming after them. 
