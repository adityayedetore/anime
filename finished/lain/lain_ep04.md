# Religion
## Serial Experiments Lain Episode 4

02:00 I could listen to this acoustic guitar part for hours. 

02:28 "I don't need parents. Humans are all alone. They're not connected to anyone at all." Who is speaking here? Is it lain? If so, that is a reasonable reaction to the events of last episode: when Lain really needed human connection, after the trauma of seeing a man commit suicide right in front of her, no one in family was there for her.

03:00 Lain's faceless dead animals are finally brought into the light. But we still have things to be worried about: Lain is enclosing herself within the wires, and is focused like nothing we have seen from her before. I wonder what the reason for her change is clothes is. She kneels, bent over on the floor of the middle of her room, computers surrounding her. 

03:36 Lain's father looks in from the door that has been left ajar. 
He has things to say, things to ask, but he closes his eyes and silently shuts the door. 
What did he want to say? 
Was he worried about the sudden changes in Lain's behaviour? 
Proud that she is being consumed by the electronic world like he loves? 
Jealous that she somehow has this psych drive, when he so much desires it? 
Sad that he missed his opportunity to help her with the electronics by rejecting her previous ask for help, and wishing to make amends? 
It is not clear what he is thinking, but I think that is the point: once again the computers that surround us are blocking our communication. 
This scene is a clear reversal of a previous scene when Lain tried to talk to him in one of the previous episodes. 
Then, the computers were blocking Lain's words from getting through to her father, now it is the other way around. 
And the scene ends with the door shutting in his face, just as in the last episode there were doors always shutting in Lain's face.

04:00 Gotta say, whoever did the art for this episode didn't really do a very good job with the characters. The forms are all over the place. This shot is of Lain's sister stealing some drink from the fridge, and her profile looks like it's a different character entirely. 

05:45 Maybe I'm just in a bad mood, but these scenes are kinda pissing me off. This whole chase seems unreasonably long. Like to I really have to listen to this guy's screaming and heavy breathing 3 minutes? We have had greater emotional resonance with a scene half as long. Maybe there is something to be said about the fact that even though the lights in the neighboring houses are on, no one comes to this screaming man's aid. But I think that is kinda a stretch, and it certainly doesn't justify the length of this scene. It is ironic though that I've spent more time complaining about the length of the scene than that time I've spent watching the scene itself. Anyways, the monsters of the city are coming out to play, and it seems they have a propensity to take the shape of young girls. How very interesting. 

07:54 There is something going on with lain, and I can't figure it out. At school, she is being horribly chatty, hiding the book about modifying the computers that she has been so engaged with. Given the emotions we have seen from her before, I can't help but think that she is putting on a facade. 

08:55 This saccharine, syrupy conversation is making me gag. I guess I really am in a bad mood. I have no idea what this conversation means. 

10:00 Death flag for my second favorite character. 

12:12 There is a computer game that is invading reality. The stakes are high, and once you enter, the only way to leave is to die. These seem to be what is happening to the men that we are seeing. Game coming true, kinda cliche. 

19:38 I am having trouble seeing anything in this right now, so lets just summarize what just happened, shall we? Ok so Lain is definitely acting strangely at school, and her friends notice. The exact cause does not seem to be explained yet, but the episode emphasises the computers that Lain is working on, so we can ascribe some of the change to that. Lain is diving deeper into the wired world, and is trying to transfer herself to the wired world. She may be still driven by curiosity about Chisa, or maybe she has accepted that escape from technology that encloses us is impossible. Maybe trying to stand apart from the technology is painful because you can see the full extend of the lack of connections between people, so Lain has decided to go in fully. Whatever the reason, lain seems to be interested in some game that is entering our world. This episode may be speaking to the fear that the violent games that children play may be affecting how they act in the world. I don't really find that a compelling argument though. 

22:00 And Lain is now an exper, and her ability is in some way related to her use of the computers. 


### And Done!

I should definitely re-watch this episode when I feel a little less wonky, but still think it was the least compelling so far. Horror works so much better when you care a bit for the characters undergoing the terrible events, and the show didn't do a great job portraying these characters as people we could emotionally attach to. 

The whole thing about the kid mistaking the real world for the game world and killing the little girl they were playing with may have been more emotionally resonant if the scene had been set up better. Horror is really tough to do in anime, and outright screaming and running is not really the way that is best suited to the medium. Also, the idea that video games could make children more violent has been proven false, so I guess seeing it here felt a little out of date and cliche. However, people are still worried that it is a problem, so at the very least this episode predicted people's fears. 

One thing that I realize in retrospect is that although Lain is changing a lot, it does seem like she has cause for change. In the last few episodes she has been haunted by ghosts and seen the death of at least two people. She has discovered the wired, and been unable to find comfort with her family in her time of greatest need. She has gone more than once to a adult party, and she is being chased by strange cyclops men. She even seems to be developing psychic powers. And worst of all she is on the cusp of her teenage years. A little change is warranted, I believe. 

Not a bad episode at all, especially that last reveal. I'm excited to see what Lain's burgeoning esper powers have in store for us in the next episode. 
