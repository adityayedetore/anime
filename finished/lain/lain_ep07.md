### Society
#### Serial Experiments Lain Episode 7 

00:20 As always, this episode opens with the now infamous words "Present day. Present time." I had previously not recognized that the words are actually spoken in English, not Japanese. The language of code is English, and for a Japanese viewer using English for these opening words likely resonates with the influx of English technology. Also it might be that this sort of technological advancement was associated with the west. 

00:30 And of course, this amazing opening is also in English. I don't know the rates of English fluency among the Japanese when SEL was made, but the decision to use English here to may have also added to a sense of disorentation. 

02:29 "I'll tell you, but it's just between me and you. I'll tell you whats happening in this society, what's beginning... because you don't know about it". The fact that these words in the last two episodes were lies makes me suspect that this is a lie too. The lie is that the problem is a secret. The problem is obvious: it is the technology that closes us off from other people. 

03:20 Wires are spilling out of Lain's house like the guts of a zombie. The wires seem oddly organic, yet dead at the same time. The hole in Lain's wall from which they spill is a result of the attack on her computer system in last episode. Green liquid flows through pipes in Lain's lair, the whole thing looks like its alive. 

03:34 Lain understands the strange alien language of the computer, and talks to it like its alive. Just like English may alienate a Japanese viewer, this strange language does the same. Information is being hidden here, and that might make us uncomfortable. 

06:01 A man walks with his computer strapped to him, filtering the world around him through the wired. A high ranking coder working on a computer is invited to play a new game. 

07:18 "The real world isn't real at all." says lain.

08:10 Alice! Go Alice! Alice takes Lain's hand, and this might be the first non sexual contact we have had this entire anime. Lain is slipping into thinking that the wired world is more important than the real world, but with an act of kindness Alice is able to bring her back. 

08:52 "This news is being sent out at the moment, but please be aware it may arrive tomorrow, or possibly yesterday." The scrambling occurs again here. 

11:40 I know nothing about analysis involving the male gaze, but there is probably something to be said about it here. A postman objectifies a woman he delivers a navi to, just like previously a worker offers herself up for objectification for the high profile coder from before. Micro-interactions involving objectification like these are not ideal for real connection, as they turn people into things. 

18:59 "Lain Iwakura. Are you and the Lain of the wired one and the same?" the coder from the beginning of the episode asks lain. Lain is in distress. How could I have not seen this before, the conflict here is between our online selves and our outer selves. So much of our lives are online, and the way we act online could be completely different from our real world personalities. The coder continues: "Are your parents your real parents? Do you know their birthdays? When they met? Have you every celebrated your birthday with them?" Often our real world selves are defined by our connections to others. But technology in the current era is eroding those relationships. Our sense of self is in the real world is rotten, and crumbling away. So it makes sense that our online selves are more real in some way. 

#### And, Done. 

Not the my favorite episode so far, but still pretty great. It was just a little on the nose for me. I guess I've come to expect subtlety from this anime.

The focus of this episode was the dissolving boundaries between the real world and the world of the wired, and the corresponding dissolving boundaries of the self we present to the internet, and the self that we are to others in life. Since our notion of self often comes from the reflections of ourselves we see in how others see us, if we have no connections to others, then we have little sense of self. If this happens, then it is not surprising that our online selves feel more real. 

I may be missing things, but this episode didn't seem as thematically dense as the previous ones. A good fraction of the episdoe was spent setting up the coming conflicts with Lain and herself, and Lain and the Knights, and painting the Knights in a threatening light. We haven't had any other episode laying so much groundwork the next until now, so I suspect that the next is going to be a blast, and I can't wait to see it. 
