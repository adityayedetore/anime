## Rumors
### Serial Experiments Lain episode 8

"Do you want to be hurt? Do you want to feel like your heart is being scraped with a rasp? If you do, don't look away, no matter what." 

I am not sure how to talk about lain now that I know that she has DID. At the risk of sounding demeaning, I am going to call the shy lain 'lain 1', the belligerent lain 'lain 2', and the destructive lain 'lain 3'. Without asking lain herself, anything I call her alters wouldn't quite cut it, so I'm going for as generic as possible. I'm sure there are better names here, and I'm just not quite good enough at words to think of them. 

Lain talks to one of the kids from Cyberia within a video game in the Wired. She can't see the appeal, but clearly video games are deeply loved by many who would be hard pressed to justify their love of them. From there, lain enters the online Cyberia and sees the front page sporting words "Cum on in! Click here". I've never been a fan of this play on words, but here it is a great shorthand for the type of advertisement one might see on porn sites. Lains discussion with a faceless man takes place within this virtual strip club, next to an exotic dancer. 

The 6-th gen internet protocol they are talking about here is not that widely used today. It was an upgrade from the ip 4, which supported 32 bit addressing. The 6th gen internet protocol, ip 6, supports 128 bit addressing. This means there are many billions of times more possible addresses than there are stars in the universe. And this 'data throughput limit', that not related to the ip addressing. To the non computer scientist, though, I'm sure the internet protocol is a bit like magic. And the idea of trying to control or at least harness portions of the internet isn't that foreign to the world today: we have russian bots (or at least bots using russian utf-8 characters) attempting to polorize political using the YouTube algorithm. 

Lain's sister is talking, but communicates nothing, just like the news anchor on the buzzing tv screen. 

I love this shot. The horizontal lines of the kitchen are slightly off, lending a sense of uneasyness to the entire thing. This is very well done. 

Eyes are the way in which we connect to others. That is why so many times the eyes of the evil creatures are altered, or covered. Without human eyes, things feel monstrous. But even human eyes can feel alien. Unblinking, and with no emotion. They can feel dead. That is what lain is seeing in this scene. The eyes of her dead parents. 

I love this Alice. Please, please, don't let anything bad happen to her. There is someone stalking her, or her father likes to drop her off at school or something. Alice has an emotional connection with lain, the only emotional connection of the entire show. She believes that Lain isn't the one to do whatever bad thing has happened on the Wired. 

The floating mouths represent the flow of rumor in the wired. These rumors have the power to alter minds and destroy lives. Lain is hearing about her other selves again now, and there is nowhere to hide. She is the one who is causing all this trouble in the Wired, with her alter self. This is about our online personas, and the dysphoria they may cause in our sense of identity. 

The eyes of her classmates are on her, staring her down. They are like knives, or gunpoints, all drawn and pointed. In the wired, while you are always being watched, you can still hide. But when the wired bleeds into reality, there is nowhere to hide. The eyes will always be following you, and there is nowhere to run. Lain is running here, but no matter where she goes there are accusing eyes of her classmates.

Oh shit, so Alice has a crush on one of her teachers, and Lain's alter spread that rumor to the other students. The pain that information can bring. It is happening to lain, it is happening to Alice. But don't give up on lain, Alice. She needs you, she needs you more than you know. 

Lain is trying to commit suicide here. She is trying to strangle a part of herself. 

Lain has erased their memories of the event somehow, and is now having an out of body experience. The person who she was on the internet, the person she deleted, was in some way a part of herself. 

Lain has DID, but is also struggling with who she is. There are parts of herself that do terrible things, and those parts are as much a part of her as the righteous and childish parts. She is struggling with who she is in the Wired, and who she is there is in any way a real part of her. On the internet, we do not only exist as ourselves, but as the many copies of ourselves that others see, the false, dummies of ourselves. But that is how it is in reality as well. Our 'self' is not only our self perception, but also how others see us. To be perceived is to exist, but the act of perceiving alters what is being perceived. 
