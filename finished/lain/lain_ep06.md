## Kids
### Serial Experiments Lain Episode 6

Last episode Lain's sister was consumed. Who's next?

02:30 "If people can connect to eachother, even the smallest of voices will grow loud. If people can connect to one another, even their lives will grow longer. So... " I previously thought that this changing voiceover in each episode was a statement of the episode's theme, it's purpose. But now I realize that they are _lies_. These voiceovers are the foolishly optimistic lies that we told ourselves as we built the telephone poles and trains and cellphones. We thought that the wired would allow us to "give a voice to the silent" and "gain the sustance of the soul that is human connection". But, at least according to this show, we were wrong. 

04:33 Lain's father feels something is wrong, as he sees Lain changing, becoming enveloped in the wired. He climbs the stairs to her room, and the tension within him makes him stop, hesitate, before entering her room. He wishes to speak with her, to find out what is changing her, what is wrong, but the thing that is changing Lain is the wired, the world that Lain's father so loves. To Lain's father, the wired is the greatest creation of mankind, so how could it be bad for Lain? He is struggling with his own beliefs. And then when Lain's father opens the door to lain's room, he sees lain wrapped in a wired womb. But lain is not there: she has left her skin and bones behind, and entered the wired.

05:18 I've heard that it may be worth it to pay attention to the blood on the screen, the red splotches that glow from the shadoes. To me these are just another representation of the monster lurking in every sceen, that violent death hangs imminent for all. 

08:04 Lain goes out with her 'friends', and yet again sees a kid basking in the sights of the technological world. His arms are raised, saying "Look at all this! Isn't it amazing?" 

09:20 Wha? The kids are worshiping Lain, who appears through the clouds like the visage of a god? I get that Lain's alter ego is some god of the wired world, but what is going on here? Why is it the kids that see her? 

10:13 Lain's sister's corpse, dead eyes staring at the t.v. screen. On the tv the news is playing, and a woman moves her mouth, but no words come out, only the hummmm of the wired. The news is as empty and senseless as Lain's dead sister. 

10:14 OOOOOH I like where this scene is going! This is the middle of the series, where we would conventionally find the turning point, where our protagonist begins to take action. I love that look on lain's face! And she is literally opening a door, her room door, the door to the wired world, with such a look of determination! Lain! 

12:54 And shit, she's going to talk to the sage, the old wizard, the gatekeeper. This is getting interesting! 

14:00 "I want to realax here until my body rots away in the real world" says the hospital bound mad scientist. Damn, I just realized that to perfectly ascend into the wired world is to become immortal. And what measure of godhood is better than immortality? 

19:05 The mad scientist was actually not all that evil after all. He was simply messing with powers beyond his control, and created a monster. Those children that the scientist experimented on have been sucked into the wired, and now they are being used by the Knights to perform an attack on reality. But the Knights are not only using those kids, but Lain as well. Or is lain one of those kids? 

22:28 And an attack on Lain, but the cyclops' appeareance save her. And Lain is left alone to wonder how deep the Lie goes. 


### And, Done!

Awesome! Awesome, awesome, awesome!

This episode has pushed the plot forward, introducing the antagonists and bringing us to the midpoint of the story (Or is it the end of the first act?). Lain is starting to smell the rot at the center of Denmark. She does have a character arc. 

Lain's Lie is that no matter where we go, we will always be connected. She believes this because of the literal ghosts that haunt her. To her, even death is not a barrier for connection. But she fails to see that the wires, ironically enough, are themselves barriers to connection: the people the wired monster consumes are lost forever. The knights have been harnessing The monster's power, and also Lain's power without her knowledge. The Knights wage war on reality, merge the two worlds by consuming everyone. But to be consumed means to die, to become an inanimate object, and Lain must realized that death is an end of connection. 

How does Lain's ability to solve the conflict hinge on outcome of her internal battle with the lie? To do battle with the Knights, Lain must realize that it is a bad thing if people are consumed by the wired. To do this, she must accept that death is the end. 

I am not entirely sure that I've gotten Lain's character down. But I have started piecing it together. In the next few episodes, I hope to get a better grasp of it. Till then!


