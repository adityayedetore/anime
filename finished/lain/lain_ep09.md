### Serial Experiment Lain episode 9
### Protocol 

I've realized that the crows and the lyrics in the opening do not portend good things for our lain. I think she might die by the end of this series. 

"If you want to be free of suffering, you should believe in God. Whether or not you believe in him, God is always at your side." Its not difficult to see how this is a lie. It is even self contradictory. 

A blurry, hand-held shot of the discovery of a crashed UFO in the desert of America. "Conjecture has become fact, and rumor has become history." There are two meanings that are possible here. The first is the obvious one, that people's conspiracy theories are true. But the sentence is also understandable as meaning that the very fact that many people believed a lie about the object rewrote history, so the lie became the truth. 

The events of last week are hanging over lain like the ropes of the gallows. But these ropes are covered in black plastic and stretch across the whole world. Lain is enveloped by paranoia, fear, and self loathing. 

A conspiracy of aliens. 

Lain is closing her eyes to the world, and submerging herself in the wires. Its an escape from pain, from loneliness, from paranoia. 

We are switching back and forth from this documentary style telling of the conspiracy, and Lain's continued stuggle with her other selves, and with what she is able to do, and with what she has done. 

Her identities are merging? She is starting to accept that she has mutiple identities. Is this about leaving copies of yourself on the internet, and that being related to memory? Are our memories constructed and overwritten by the way the appear in the wired? If that is the case, it is no wonder that Lain has an identity crisis. 

So lain really is the result of an experiment. The story goes that some scientist created a way to make people biologically plugged into the wired world. This researcher was killed, but Lain's adoptive father worked with him. When we are connected to the wired world, and our memories are part of the wired, changing the truth of what happened is equivalent to changing the wired world. 

Memory, truth, different selves in the internet, conspiracy, rewriting history, aliens, the collective. 
