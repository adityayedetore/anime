## Psyche
### Serial Experiments Lain Episode 3

01:24 I may be mistaken here, but I think during the middle section of the opening, the girl we see is not the lain we have come to know, but instead her alter ego. I don't know exactly what makes me think this, maybe it is her eyes, maybe it is the earring, maybe it is the hair. That alter ego seems to be trapped in the screens, always watching. 

02:00 Crows, death, freedom. Kinda fits with the thread of escaping through death we have seen in the last two episodes. 

03:42 This is the first genuine moment of an attempt at connection. It seems some characters we can be sympathetic with do exist in this anime. The girl (whose name I have forgotten) acts in a way that contrasts sharply with the actions of the adult detective/policeman in the room. His attempts at communication are as fragmentary as the rest. 

04:12 And this is the first moment of silence in the show. Is there something to be said here about the constant noise drowning out our ability to connect to others?

04:19 It seems something has broken inside Lain, and the only thing she do or say is to call the name of the one person who has shown her any compassion so far. In the wake of the trauma of seeing a man commit suicide infront of her, Lain has reverted to a childlike state where the deep desire for human connection has surfaced. But that one person has gone, and Lain is alone. 

05:27 And Lain is completely alone in her house too. Her parent's beds are empty, and who knows where her sister's gone. Except ... there in her room is cyber eye, calling out to her, drawing her in with the temptation of even a fragment of connection. 

06:27 Why is it always that the silhouettes of the animals on Lain's windowsill seems to menacing? They don't look at all like soft toys, but instead like the shadows of animals, lurking and waiting to strike. Stuffed animals are like imaginary friends, surrogate emotional connections to dead and soulless objects. And that is just what the wired provides: a unidirectional, false sense of connection to a black hole. And the wired too seems like a monster, lurking in the shadows, in plain sight. Is there a connection here, or am I just reading too deeply into this? 

07:06 The words no one is present to give Lain are spoken to her by a computer. "good night, Lain." A false connection, indeed. 

09:13 And yet again, as Lain walks to school there are strangers watching. This time from the darkness of tinted windows shines a red laser, reminiscent of the gun of the last episode. Strangers always watching, kinda like the internet, no?

10:38 As Lain sits in class, scribbling messy spirals in her notebook, she hears voices in her head. "However, activity in the wired is limited by the machine. The Psyche can dramatically increase the performance of any Navi". Clearly this is about the encroachment of the digital world into the consciousnesses of real people. As we hear those words, the spiral turns into a vortex, seeming to suck inwards into the page. Seems to suggest that not only is the wired worming into our brains, but also we are being sucked into the wired. As we offload more and more of our consciousness into the wired world, we become more and more a part of it, and less of ourselves. This whole "loosing the boundaries of the self" thing reminds me of A.T. fields and the porcupine's dilemma. 

11:06 Hmm, they are making these themes awfully explicit. "There was no reason for me to stay in the real world any longer. In the real world, it didn't matter if I was there or not. When I realized that, I was no longer afraid of loosing my body." I do like things spelled out once in a while, but not sure if I am a complete fan of it here. At least they are building upon Lain's hallucinations/clairvoyance, making them a larger part of the story. 

12:24 I'm liking this Alice more and more. She is the only one with an attempt at real connection, though she has so far been twice derailed by the circumstances. So... she better not die. 

13:12 LMAO, Pshh-YOU-kay? I'm sorry, that made me laugh. It's psyche, damnit. 

15:23 She stops so far away from her father. It's a tiny object, how could he possibly see it? Is she afraid of coming closer? This distance speaks volumes about the emotional distance between them. Clearly he knows something about this situation though. Have we been given hints about this object that I have just not been picking up? To me it's a complete mystery. One thing that is clear though is that this episode is about the lack of connection that makes people turn to the wired. This episode is filled with doors closing in Lain's face, and rooms devoid of everyone but the glowing computer. Also there are so many dutch shots here, maybe a bit too much if you ask me. 

16:48 Last time Lain went to Cyberia, a group of young children pushed past her, calling her an 'older sister'. This time, Lain walks by an adult couple getting handsy, and again she is reminded adulthood, and steels her resolve to go into the club. Seems everyone she looks so is making her grow up too quickly. 

17:06 "What the--? Damn, you're sure going for the little girl look today..." says a glasses wearing older man as Lain walks into the club. Again with the reminders of her age. It seems the things she sees are pulling her in two directions. And then we see the kids from last time. What exactly is going on here? 

17:48 Again with the Pshhh-YOU-kay? I sure hope that name only sticks around for this episode, or I wont be able to take this seriously. Dunno why it's making me laugh so much. 

18:34 I think earlier in the episode there was something said about how even middle schoolers are getting Navies... ah thats right, the voice in Lain's ear. Children are especially at risk in the wired, as it gives them access to things they normally would not have access to. This whole scene, of the trio of children explaining the latest technology to Lain, occurs within a _nightclub_, the epitome of a place not for children. It seems the world of the wired is blurring the lines of what is fit for children and what is not, due to the universal access. You know, I think this might be one points made in the show thus far that holds up the best to the world of today. Children are seeing things much earlier than they had before. 

22:35 Why end with this? The last scene of this episode is Lain taking up a smiling face, saying "Welcome back!" to her sister. True, this is the first time we have heard those words, but why here? Is Lain happy that her sister says, "are you an idiot?". Does she crave connection that much? 

### And Done!

Great episode, but not as entertaining as the first two. I really liked the feeling of the neon lights and electronics always hanging and menacing and buzzing in every scene, but that wasn't the focus this episode. Thats a good thing, I guess, as repeating the same thing over and over again probably doesn't make for the most complex work. 

It seems we are getting a bit more information about this other side of Lain. It seems that her wired avatar is leading the march on reality, or at least is a part of it. We also are being slowly introduced to sinister forces in the real world, in the shape of red eyed cyclopses stalking Lain from behind black tinted windows. 

I don't think I have fully pieced together the thread about the youth and their early exposure to the world through the wired, mainly because I can't see how it is being portrayed as something bad. There is no way, in my mind, that the creators of this show would be so critical of technology and yet think that the internet's influence on the youth is a good thing. I guess I'm still waiting for the other shoe to drop, and for something terrible to happen to the kids who know more than they should. 

Maybe the point is simply that lack of connection in the home is driving children towards the wired. That would for sure make sense given Lain's constant failure to reach her family in the home. 

In this episode creatures are starting to come out of the shadows: the Cyclopes people, Lain's alter ego, the ghosts haunting Lain. Maybe the reason I liked this episode a bit less than the others is that I felt more afraid of the monsters when I couldn't exactly see them: I fear the unknown above all else. But that is a personal preference, and I couldn't fault the show for skillfully done progression. Even if the show were to maintain this level of quality, it's going to be a fun ride. 
 
