### Secrets / Wishes
#### Kakushigoto Episode 1

I've heard that this is a pretty good anime, and it is airing right now! This is the second anime that I will be watching as it airs. The synopsis wouldn't have caught my eye, but Bobduh recommended it, so I'll give it a try. 

The narrator of these opening scenes is the manga titular manga artist's daughter, when she is 18 and has found the keys to her father's secret job. She seems to be looking back on the incident, but her feelings about the subject are not clear. She calls the shed a "pandora's box", but says that "my father draws for a living", not "my father makes dirty manga for a living". There is tension in that ambiguity. 

I appreciate that this opening has a male singer. Also a recurring motif so far has been the waves of the ocean, and it has come up again in this opening. I wonder what it will come to symbolize, if anything? 

The color are so nice here! Love the abrupt shift to sepia tones to exaggerate the humor, and to a lend the air of a memory to the scene. 

A lot of setting up of characters is going on here. There is the teacher that knows about the father's occupation, the assistants at work, and the editor. A running thread through this is being embarassed of your work. 

This episode was about 

Interesting that there doesn't seem to be much that I can analyze here. 
