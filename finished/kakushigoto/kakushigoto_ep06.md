### Kakushigoto episode 6
### School rucksack 

The pair receives a backpack every year from some person named Naoto. Hime has already gotten a new backpack from Kaku, but feels bad about not taking one of them, so she compromises by putting them both on. She is sympathetic and compassionate, and respects the feelings of others. She has one backpack for every year here, just like they had boxes for every year before. 

This episode doesn't start with the present, with Hime at the old house. I wonder if the creators are going to be putting that at the end from now on. 

"Come forth and bloom, flowers, and catch the fragments of dreams."
"The future you wished for will soon, yes, bloom into spring"

Interesting that the changing of the seasons from fall to spring is accompanied by the visual image of Kaku fading away. Spring is usually thought to be a time of birth, of life. There's an interesting connection here to sing "Yesterday" to me. The seasons change, and while every year the sakura trees bloom, some things don't come back. That image is strangely set against the lyrics, which speak of dreams flowering to life. 

"Beneath the sunset, two shadows sit on swings and blend into the sky"
"The higher I go from the momentum, the higher my happiness rushes too" 
"These ordinary days" 

"The only thing we awkwardly have in common is that we cannot be honest with each-other"
"I keep the words I want to say a secret at I look up at the sky"

These lyrics were probably originally meant for a pair of lovers, but they are also suited to a Kaku and Hime. 

"May the wind blow and color this world and your smile shine. 
Don't be afraid to take the universe that has filled up both your hands"

This is about looking at tasks, and saying, "I've been given the privilege of doing this". The sentiment could very well apply to the fact that Kaku takes care of Hime all on his own. Honestly it's a bit sad to try to think about it that way. 

And that is exactly what is made clear a few minutes later. 

He is estranged from his father in law, who did not approve of him marrying his daughter. 

And this is about pride and confidence in your work. Kaku likes doing what he does, but also he does not think of it as a respectable profession. There is self loathing there, and that is part of what makes it so tough for him to have an honest conversation with Hime. He believes that she will not accept him for who he is. I can understand that feeling. 

Sigh... This is his chance. He has just realized that he can tell his child who he is, and what he is feeling, and things will turn out alright. But we know that is not going to happen. 

She literally looks at him for 5 seconds, and doesn't recognize him. Your daughter doesn't recognize you, Kaku. That must hurt. 

"Artists do their job, even if they have to pay for it". Ah, so we are flipping around the usual perceptions of what is fun and what is necessary. In life we usually pay to do what is fun, like working as a manga artist 




