### Kakushigoto Episode 10
### I"S (Izu)

"I thought we were stocking up for the new years. But they are all empty." "I'm sorry! I'm a bad girl and I can't help but eat everything infront of me." Waiting so that the pleasure will be greater in the future, vs having it now. 

"I'm just a bad boy who can't help but slack off as much as possible." Drawing a similarity here between father and daughter. Sacrificing the future for the present. This is also kinda related to the way that Kaku does not discuss difficult topics with Hime. He want's to make her as happy as possible right now, but he doesn't realize some times having those difficult discussions may make the future a lot easier. 

Ok, so Hime literally says something to this effect. "You are too soft on your daughter. I'll grow up into someone who is unable to plan anything". 

Hmm, they hypothesize that the reason that Kakushi stopped writing his manga is that the fact that his wife was lost at sea was revealed to the public. 

On second thought, this episode has more going for it than I thought. It is about exposing the private life of someone in the public eye, digging up their ghosts. Apparently the public perception of a manga artists affects how their work is perceived. Kakushi presents a falsely happy image to his fans, by appearing in photos naked. The death of a gag manga artist is their private life being leaked. 
