### Kakushigoto Episode 11
### The last chapter doesn't bother her

This show is filled with humorous misunderstandings that produce little conflict. When people communicate with each other they are often talking at cross purposes. 

There is a conflict here. They all don't like meetings, but that is the place where people communicate. So they don't like to communicate. 

This is humorous, but also it is really sad. 

Enjoying the time that is left. Misunderstandings reduce the amount of time left. 

Now a ghost is preventing the meetings. 

This episode really fit with the theme of this anime. Lack of communication leading to misunderstanding of the intents and feelings of others, and that blowing up into a larger problem. Then at the end, the communication happens, and the problems get resolved. There is also symbolism here, with the ghost in the well, and the ending of the manga. 


