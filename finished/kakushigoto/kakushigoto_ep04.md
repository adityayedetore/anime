### Normale Namae 
#### Kakushigoto episode 4

Her explanation of why there are boxes labeled with years on them is interesting. She believes that the boxes were left for her by her mother, and they were meant for her to be opened every year. Based on how her father reacted to the boxes, I don't think that is what is going on. It is more likely that her mother kept the boxes from her own childhood. 

Interesting that her eyes are so dead now. What has happened such that she is now so sad? 

Ok so the lyrics of the opening talk about not being truthful to eachother, not fully telling each-other what you are thinking. That accords with my analysis of what has happened so far. 

This whole thing about names, it be connected to family.  

Interesting that she is eating lunch with Hime's teacher. Are they dating now? Is that a thing?

Hmm, one's name is being linked with fate here. So if Goto's name indicates that he will be alone, and his name is a part of his daughter, what does that mean? 

And now names, fate, luck, pen-names, identity, and ghosts are being linked. 

"I'm actually here with someone far away" This writing is great. What seems like just a joke is actually a comment on the situation that Goto is in. He is not really here with the teacher at all, because in his heart he is still with his wife. 

Even the scene about depicting monsters without ever drawing them by drawing the world from their perspective is subtly telling something: it suggests that Hime's mother's ghost is watching over them, and actually the entire episode is from the point of view of that ghost. 

The ending theme is about the past, and wanting the past to come back. 
