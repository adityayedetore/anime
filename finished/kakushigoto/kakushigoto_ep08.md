### Our Rough Draft / Unfortunate Memories Club
### Kakushigoto Episode 8

What is the relationship between Hime's mother and water? 

He really is serious about giving the dog a name. Until the dog has a name, it is not really yet a part of the family. Why is that such a big problem for Kakushi?

There was something in his past, something I don't quite understand. I remember that as soon as Kakushi saw his daughter, he wanted to name her Hime. Maybe her grandfather on her mother's side took ownership of the name? 

I'd guess that this is exactly what happened to Kakushi. He gave her the temporary name "Hime", but it stuck. His father in law gave him flack for picking that name, so he doesn't want a repeat experience. But Hime names her dog on a whim, then says that it doesn't matter who names the dog as long as it is a good name. 
This anime describes the relationship between Kakushi and Hime from multiple different angles. One is the dog, another is how they celebrate special days, another is him hiding his work, another is grief about their mother.

Shit, heart attack.  

Holy shit, this is the first real conversation. "You need to celebrate the things you have while you have them, because at some point it may be too late. At some point, you might not be able to celebrate even if you want to, or you might not want to celebrate but be forced to." 

Really, at it's core, this is an anime about a father trying to make his daughter happy. There are many obstacles in the way, but the main two are the fact of her mother's death, and his shame about his work. Another problem is the very fact that Kakushi cares so much about his daughter's happiness, as this often makes unreasonably worried. 
