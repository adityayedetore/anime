### Kakushigoto Episode 12
### Hidden truths 

He is the secret child of a Kabuki. 

He quit as a mangaka and took a blue collar job working equipment. He moved Manga boxes, but one fell on him. Someone had taken out a manga, trying to reveal the secrets. He was buried by the weight of revealing his job. 

Its pretty interesting, these characters are static. Secrets don't get revealed because the characters realize that their lives would be better if those secrets were out in the open, but rather by accident. Kakushi never gets over the death of his wife, he never tells his daughter his secret, he never resolves his relationship with his father in law. Still engaging. 


