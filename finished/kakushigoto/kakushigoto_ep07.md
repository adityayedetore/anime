### Dog wanted/the one who connects mother and child
### Kakushigoto episode 7

"I memorized the 10 commandments of dogs" "Really?" "I only live for about 10 years, so stay by my side." Are we talking about Kakushi here? 

The ten commandments are supposed to be "The ten promises that dog owners are supposed to make to their dogs". Kakushi is the dog, and Hime is his owner. 

The dog represents responsibility in relationships, which is intertwined with Goto's core problems. He has to take care of Hime, but alone. 

Represents family bonds as well. To get a dog, to have a child, means to take on a responsability. And the way they are framing it in this anime, it means to be aware of the fact that it may die before you do. Is Kakushi dying? 

Kakushi links the responsibility of owning a dog with his feelings of shame about his work. 

Oh shit, is this the first time that Hime acknowledges that her mother is not around? "Because I don't have a mother there are a lot of things that I cannot do" This is exactly what Kakushi wants to prevent. He doesn't want Hime to ever miss her mother, and so doesn't discuss her. But that is preventing them both from getting over the grief. 

What? There is a painting of Hime hugging an old dog. But how? Did they have a dog before? Is this a picture of Hime's wife when she was young? 

I think it is. I also think that this dog is the child of the dog in the painting. 

Ichiko's eyes... they are dull black, like Hime's. 

This is about comedy in spite of grief, comedy not as a way of dealing with grief but rather as a way of avoiding it. It is about reconciling shame about work with the desire to appear the best to your children. It is about the specter of loss. It is about the feeling of needing to do whatever it takes to make you children happy. It is about death. It is about secrets. It is about inheritance. 
