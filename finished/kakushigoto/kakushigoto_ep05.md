### Everyone Gets Their Turn in the End / You’re Spending the Night!
### Kakushigoto Episode 5

That quick scene in the opening song with Kaku and his daughter painting the house is a pretty good encapsulation of his relationship with her. Kaku is overly worried about her, reaches out (without words) to help her, and ends up dropping the paint on his end. 

Oh I just realized that this is the first episode that doesn't start with a scene of Hime in the old house. What is going on here? 

The wheel chart from before had three slices, and the work was divided up evenly. Even Hime had work to do. But now, it is Kaku doing all the work, and trying to get his kid to just play. He is overprotective, and working too hard to make her life as easy as possible. 

Interesting, there wasn't much to say about this episode. I wonder if that can be attributed at all to the episode director. It seems that the information about this episode's director is not yet online, but I might want to check out who it is when it does come out. 
