#hi
### hi
Maiagare saku hanabira tsukamaete yume no kakera          | Go fly, blooming petals catch the fragments of dreams
Kimi ga negau mirai ni sou haru wa yatte kuru             | In the future you wish for, Spring is arriving 

Yuuyake no shita buran ko ni notta                        | Under the sunset on the swing
Futatsu no kage sora to mazatte yuku                      | The two shadows mix with the sky
Ikioi tsuke takaku naru hodo ni                           | The higher it gets, the more momentum it gets See, 
Shiawase mo hora kakeagaru arifureta hibi                 | our happiness jumps up, Normal everyday life

Sunao ni narenai tokoro dake myou ni nite shimatta bokura | We were strangely similar only where we couldn't be honest
Tsutaetai kotoba himitsu no mama de miageta sora          | The words we want to convey remain secret We look up at the sky

Kaze yo fuke sekai wo somete kagayaite kimi no egao       | Let the wind blow and color the world
Osorenaide sono ryoute ippai ni kakaeta uchuu e           | Let your smile shine Don't be afraid,
Maiagare saku hanabira tsukamaete yume no kakera          |  go to the universe you are holding in your arms fully
Kimi ga negau mirai e to kogidashite                      | Go fly, blooming petals, catch the fragments of dreams To the future you wish for




