### Beach Sandals and B4 /Don’t Place, Don’t Draw, Don’t Finish Up
#### Kakushigoto episode 2

Hime's father and his coworkers procrastinate for a bit by cooking instead of working. This familiar desire to cook or clean instead of doing work makes Hime's father and his coworkers more relatable, and add to a sense of everyday humor and drama. The humor of this scene doesn't land well, but that is fine. 

Hime will be going to the beach, and lets her father know. But her description of catching bugs, swimming, and making fires makes him worried. Looks like the episodic conflicts of this anime are going to revolve around the relationship between parent and child.

I am not really a fan of this "the cuteness is undeniable" gag. If the fact that Him's mother is no longer around is brought into it, and it becomes more dramatic, I think I could buy it, but now it just seems lazy. 

Oh shit, that is exactly what happens! Oh I will never doubt you again, Kakushigoto! 


