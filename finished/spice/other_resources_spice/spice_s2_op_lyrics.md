neiki yori sotto yoake ga kuru            | Softer than a sleeping breath, the dawn arrives.
kimi no yume wa ima mori o kakeru         | Your dream now dashes through the forest. 
mezamete akari o sagasu shigusa           | Waking up, you look for a light.
nakushita nara mitsuketeageyou            | If you can't find it, I'll show you where it is.
kimi to boku no mirai ni kakureteiru      | It's hiding in our future. 

saa koko e                                | Come now, if we join our hands here, 
te o tsunaidara hodokenai y               | They will not come apart, 
kimi to tooku                             | As we go far with each other.
haruka na hi no kioku ni                  | The memory of a distant day that bloomed,
aoi kuni o miru sutoori                   | Of the sight of a country of blue,
kagayaku hitomi tsutaeyo                  | With shining eyes, I will tell you.

hieta moya no iro amai                    | The color of cool mist, the sweet sound of flapping wings.
furueru katasaki ni ai ga tomaru          | Love rests on the top of your quivering shoulders.
omoi o aa dare ga habamu darou            | Who would try to stop such feelings?
hi ga sashitara atomodori shinai          | If the sun shines, I won't look back.  
ayamachi demo eranda michi o yuku y       | Even if it's a mistake, I will stay true to the road I chose.

daiji na kotoba o kiit                    | I heard some important words,
kaze ga hakobu sora no yakusok            | Carried by the wind, the promise of the sky. 
toki ga kimi o tsuresaru no nar           | If it happens that time steals you away,
ima kou shite tada futari                 | Let's stay this way now, just the two of us.
tada futari...                            | Just the two of us...
