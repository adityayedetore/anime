# Perfect World Lyrics
## Spice & Wolf Ending

soshite koi suru bokutachi wa           |  And so, we, who are in love,
ai no temae de surechigau               |  Passed by one another on this side of love.
tsutaetai no ni damarikomu              |  I want to tell you, but I keep my lips shut.
take me, take me to the perfect world   |  Take me, take me to the perfect world.  

soshite tomadou bokutachi wa            |  And so, we, who are perplexed,
ai no mawari o mawatteru                |  Go round and round love's edge.
tashikametai no ni karamawari           |  I want to be sure, but to no avail.
take me, take me to the perfect world   |  Take me, take me to the perfect world.  

ano miro no viinasu wa ryouude o        |  The two arms of that Venus de Milo,
doko de nakushichatta ndarou ne         |  Where did she lose them, I wonder?
fukanzen na bokutachi da kara           |  We aren't perfect,                     
motomeawazu ni irarenai                 |  So we can't help disagreeing sometimes. 

dakishimetekuretara fureaeru ki ga suru |  If you hold me close, I feel we can touch one another.
dakishimeaetara chikazukeru no ni ne    |  If we come together and embrace, even if we're getting closer, well...

keredo koi suru bokutachi wa            |  But we, who are in love,
ai no temae de tachidomaru              |  Stood still on this side of love.
tsutaekirezu ni nigawarai               |  I can't tell you everything, so I smile bitterly.
take me, take me to the perfect world   |  Take me, take me to the perfect world.

