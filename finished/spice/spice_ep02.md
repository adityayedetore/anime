6:09 Irony. She is hiding her true form, the form of a wolf, in order to be in human form. Holo says, "I wouldn't hide my ears or my tail" but she is. In addition she is hiding her face and her tail. 

9:23 Ignorance is bliss. Without knowing the full truths about another person, the picture we paint of them in our minds is without bemish, free of fault. But learning the true nature of the beast may give us access to unfortunate realities that are best left covered. 

22:19 Both Holo and Spice bring a past to the table. Their pasts are not all full of roses, and at points they conflict with eachother. A wolf in a human's body is not a human, which is a fact Spice should do well to remember. This episode is about the masks we wear, and the stories they hide. 



