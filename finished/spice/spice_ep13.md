05:50 Lawrence has gotten over his fear of the Wolf. This could have been a bit of a plot point, but I guess it didn't need to be. 

06:19 "I just don't like shepards, okay?" This is communicating to the viewer that Wolf's negitive feelings towards the shephard is actually not simply because she is a shephard. I believe they might be becuase Lawrence likes and cares for her, and is so worried that he cares less for her by comparison. That has been the conflict since we first met the shephard, and it will be resolved, or at least adressed, by the end of this episode. 

06:37 Wolf's feeling for Spice have grown enough to this point that she is willing to do anything for him, including turn into a wolf. This is different from the last few times she has turned, where she absolutly did not want to, and hated the fact that she had to do it. 

14:43 Is that something that you "You didn't have to talk to the little girl like that. Did I overdo the cool act? Is that something that you should sholder all by yourself?" What is the meaning of this converstion? Ah, I see, 'that' is the necessary betrayal of the trio on the part of the trading company. Lawrence sought to protect the Shephard from the fact that they were going to be betrayed all along. Maybe. Maybe there is a better explanation, but if so I'm not sure what it is. 

16:19 This is a pretty ingenious plan. Smuggle the money, give it back to lemerio trading, and force the company to sign a deal for $500 over the course of the next ten years. Then sell the contract, making back the money that was borrowed from the other merchants and possibly making a profit. Desire for revenge satisfied, debt paid, everyone is happy. 

18:26 And this is the real conflict of the episode. Wolf wants to be the one special woman for Spice, and thus cannot stand the Shephard. The question whose name did you call? is really a stand in for the question, which of us do you like better?. But like all of their conversations, this is a battle. If Spice were to just go out and say that it was Holo that he called, not only would he not really be satisfying Wolf, but he would also be losing the battle of wits. 

18:51 The sun rises, a good omen if there ever was one. 

19:32 Spice is walking in circles around Wolf, literally and also with his words. He has never before won a battle of wits with Wolf, and now that he has the upper hand, he is milking it for all its worth. 

20:31 A kiss? 

20:55 Wolf hears the sound of bells that means the Shephard is coming, and thinks that she has a chance to get the answer to her question and win the battle with Spice at the same time. She asks spice to call her name, though Spice would shurly see that the Shephard is coming. If spice were to say Holo, then that means that he has conciously decided to choose holo over the Shephard. If Spice were to say the shephard's name, then Holo would be able to get angry, which is better than her current feelings of jelousy. But for the first time in the series, Spice has a way to defeat Wolf in the battle of wits. As the clock tower tolls, his calls out one of the names, and waves to the Shephard at the same time. Angrly wolf asks him which name he called, but hah, the bells are tolling, he cant hear her. Then, "Holo. That is the name I like better." And she blushes and calls him an idiot, and he has just won his first battle. And it has not even been close to 100 years!

22:24 And now wolf admits that the dog, who was before a stand in for lawrence, was not cheeting on the Shephard (wolf) with her (the shephard), but was loyal the whole time. What a great episode, that has wrapped up many of the major conflicts and shows the character devlopment from the beginning of the show. A great end to season 1. 



