02:04 We open with menecing music, and the sound of bells. A shephard, it seems walks in an opening in the woods to the eyes and sounds of snarling wolves. The wolves are afraid, it seems. 

02:20 The face is purposly hidden, and unfortunatly I already know what they look like do to the episode image on funimation. 

02:29 We see a new town, indicating that the pair of main characters has travled a bit now from their first large town. 

02.40 Talked about as if she's a boy the 'sorcerer' I know to be a girl is probably not as scary as she appears. 

03.40 Explicit forshadowing that she is a girl. This is interesting. I wonder why? Maybe it is setting up the fact that Spice is more liable to be hurt when his opponent is a girl? If so, why is it doing this? It refrences the betrayal by the red haired woman, and it is show to still be bothering Spice. 

04:41 Conflict with the church is brought up in a natural way through discussion of bars closing early. 

07:53 The church again. This time, when proposing a trade with a believer, the man chooses to use one set of scales over another when Spice says he doesn't have the scales (read as holy symbols) but he does believe in God. 

11:45 He is a cheat. Ok great, what did we learn here? Those who believe in god can also be corrupt? But we already knew this. That Spice and Wolf (especially wolf) are good merchants? This we knew too. What was the point of this scene? It was enjoyable and all, but I'd like something more. The trick with the wine flowing across the slanted table was pretty good. They also talk about chloe again. Seems we are meant to keep her in mind. 

22:41 And after a large buildup, the reveal of the face is not nearly as dramatic due to the fact that I already knew the shephard was a girl. Is this me stuck in my stage 2 ways? Bobduh and Hulk may say that a good story cannot be spoiled, but it seems to me that this one was. I guess I am able to appriciate repeated use of the upward shot of the bell accompanied by the noise to create a sense of mystery and suspense. But since I felt little suspense, I wasn't able able to appriciate it as much. Maybe once I fully transition to stage 3, spoliers like these won't bother me. 

