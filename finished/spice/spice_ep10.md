01:45 The shadow of the cross hangs like a dagger over the shephard. 

11:54 The visuals match the words he says. As he talks of the fact that information spreads like wildfire amoung the merchants, there are men in the background pointing and whispering. It is interesting that this is done so on the nose. I guess the fact that the cut is so short makes it ok not to be too subtle. However, of course, I'd prefer subtle over such blatant storytelling. I guess when creating the story you have to walk the balance between making things obvious and making them obfuscated. The fact that others are talking about it also makes it seem like a big deal as well, which adds to the sense of tension. Now, we know that our protagonists are going to make it out of this bind somehow, otherwise the story wouldn't be all that interesting. The question is, how are they going to do it?

17:10 And we are again beaten over the head with the true significance of the debt that has been placed on our hero. I guess that a debt does not seem like motal peril, so there needs to be some way to underscore the drama. The way that this is done is by explaining three times that the debt is significant and will cause Spice to fall in to ruin if he doesn't make the required amount in the given number of days. 

18:33 When we see this archway and then the look on wolf's face, we already know that Spice has failed to get the small loan that he wants. We are even more certian when we see the beginning of the interaction when the door opens. How is it that we know? It is probably a bunch of differnt things. For one, Wolf and spice are standing in shadow. Wolf doesn't look happy. The conversation is cut off. Wolf looks alone and sad in the shadow. 

20:21 As we see a montage of Lawrence being rejected, we also see him walking away from Wolf as she struggles to catch up. What is this telling the viewer? The stress of the circumstances is driving a wedge between our main characters. Lawrence is so stressed he has no time to walk hand in hand with Wolf. 

21:34 And then the next thing we see is him being rejected because and only because he is with Wolf. In story terms the conflict regarding the money is being equated to a conflict between Spice and Wolf. This is good, because while we have little care for the debt the lawrence is in, we do care alot for the romance between the main characters. If something is getting in the way of that, we can truly be afraid.

21:59 Oh no no no no. And the writers were succesful! I couldn't bear to see it. And it seems like a reasonable reaction too, based on characters and circumstance. Good conflict. Now, how are they going to make up? Find out in the next episode of Spice and Wolf! 

