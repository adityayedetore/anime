3:10 Im sure the details of the trading and all are done exceptionally well, but despite that they dont really interest me. I guess they might interest me a bit, and that itself may make the story an outlier, but nevertheless I don't find trading to be that engaging. However, I wonder what the story is doing to make the trading interesting at all?

8:17 Why do anime so often display negitive feelings by avoiding the face of the character who is feeling them? Maybe if the script and other factors can tell us how the character feels without seeing their emotions, we can create a better image within our imagnation than they can show us? Or maybe it is to make the telling of the emotions more subtle? In this arial shot we can see the distance between the charcters. Spice is in the light, while Wolf is in the dark. 

10:30 A full minute without sound. Spice and Wolf are seperated in the room, by the lighting and the distance, with empty space between them. There is a gulf also in their emotions, along with their locations. Spice's closing of the distance is saying "I will stay with you". 

10:50 Why do we see the moon here? "We can live 100s of years". Is it a sense of distance? Of travle? A space filler?

11:00 And after saying she doesn't want to be alone, they are seperated. Its drama, of course. The conflict is that Spice wants Holo back, and Holo is held captive. It is not the company that is holding her captive, but instead the church. They believe in one God, so a incarnation of a Wolf is of course going to be a problem. 

22:22 A cliff hanger. Who is the robed man? Dramatic, low angled shot. She recgonizes him. Who could it be? Is it someone we have seen before? Is it someone Holo knows, but we havn't seen? I'd guess that its the man they made the deal with. The chipped tooth man. 

23:37 A ending song about growing, and plants and seasons and coming and going. A song about travle. Fits nicely, though I'd like some better visuals. 

