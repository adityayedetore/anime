00:00 This opening is still amazing. It tells Wolf's story of meeting Spice. He saved her from a place of isolation, where she was slowly being forgotten. The story begins in a place wolf once journeyd to, only to find herself bound to the land, unwilling or unable to leave. Spice takes her away, gives her freedom to journey with him to foreign lands. Wolf wants to return to her home, a place she has dreamed of, where all her friends await her return. But that place, that dream, may be just that: a dream. They search for a place beyond the cold sunrise, beyond the blistering day, beyond the freezing night. Does a place like this exist? Will they find it? 

02:51 And we are finally introduced to this thus far hidden liebert. From his words and his dress, we see he is a merchant. Is there are secondary plan in the works? The head honcho is worried, but is he worried for more than just the plan?

08:30 Dutch angle emphesizes the drama, along with a sharp change in the music to a slightly grating violin. Though there is no action on the screen, the tilted angles seem to suggest a calm before a strom, or at least something about to fall over.

11:52 Wolf here seems to be talking about the dog, but she is talking about Spice. She says that the dog is assured in the Shepard's affection for him, so he is willing to play around with others. Wolf suggests that the shepard treat him coldly sometimes, so dog will be more loving towards her.

12:17 What was Lawrence's worry? That the shepard will continue to be a shepard, afterwards, and thus be exposed to more danger? That she doesn't realize that she will have to quit being a shepard? 

12:44 This face portends bad fortune. But no, nothing bad will happen, everythign went well. A little bit of built up tension, then that tension is broken. 

13:22 Again, the rain portends bad fortune. 

15:06 The tension is built through the dramatic music, dutch angles, frantic movement and voices, until it is broken again. 

15:40 Tension built once again, and kept tense by the howl of the wolf and the seperation of Wolf and Spice.

22:09 And we end the episode in a place of high tension. The rain, the empty cloak, and the ariel shot on the prone upside down figure all make spice more alone. 

