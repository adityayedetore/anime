# Wolf and the End of Shallow Thinking
## Spice and Wolf episode 17

02:27 We pick up on this episode right where we left off. Spice & Wolf's fight has left Spice to wander the streets of the town. The many people surrounding him celebrating the festival make him even more alone, since the argument with Wolf has left him closed to the world and stuck in his own thoughts. As he walks through the festivites, the words of Wolf run through his mind. "I remember, I have someone who loves me" "What am I to you?". Spice cannot commit to their relationship, because it is temprary, and it conflicts with his other goals. Wolf also wants phyical intimacy, in order to not feel so alone, but again, for whatever reason Spice cannot give that to her.

05:04 Spice goes to his merchant friend to discuss ways to keep the Knight from amassing the silver required to buy Wolf.Spice is now focused on keeping Wolf with him by battling against the knight. This is the childish thing to do, but in this situation it might be exactly what Wolf wants him to do. Wolf wants to feel desired by Spice, not because she is a lost lamb but because he loves her. What better way to do that than fighting for her? Feelings of pity wouldn't drive a man to fight, but love will. But I can't help but feel that the way that Spice is going about this is by leaving Wolf. How can that be the right thing to do?

12:26 "I finally understand what you meant when you said you wanted to do buisness with me" Welp i dont. 

22:20 The rest of the episdoe is buisness deals. I am sure that there is some devlopment of the conflict, but it bothers me little, as I have little interst in the merchant aspect of this show. 
