Trauma, but for the bandits

This is actually pretty complicated. He goes on about how this system is fair, in that people aren't judged by single actions, but the net balance of their actions over their entire lifetime. This makes sense, but of course it comes with the problem of someone believing that they should be able to perform a terrible deed because they have the points for it. But then, when it comes down to it, a person's personality is shaped by what they have done. Now that this man has done so much good, even though he has always wanted to kill someone, he can't think of someone to kill. Now he believes that his points will go to waste if he doesn't use them before he dies. 

And he hates himself for it. This is a show about systems in which people can live, and is interested in pushing them to their limits. 

Maybe this cooking thing is about how strange customs come about? 

Hmm, so this asks the question of what the value of traveling it. Is it about the experience, or about the memories? What is the point of doing something that you can't remember? 
