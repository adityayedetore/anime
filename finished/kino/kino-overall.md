oopsie, looks like this is the wrong version of Kino's journey. Lol. I wonder if I haven't been able to get much out of it because as Bobduh says, the stories that have been adapted for this version were not as conducive to the analysis as the original?

I did really like it until I realized that this is not the show that Bobduh speaks so highly about lol. Maybe that's not a good thing, but it doesn't bother me so much, since I didn't ever fall in love with the show, and also it means that I have another really great show to watch. 
All in all, I'd give this one a 7/10. The visuals were very nice, and I liked the opening and the ending. Not that much thinking that I could do with it, for whatever reason, so that brings down it's score a bit. On to other anime!
