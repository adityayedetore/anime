Wow, I don't know if it is just that my taste has changed, but when before I disliked this show, now I love it. Maybe it's just that the first two episodes were not my thing. Or it could be that knowing that Kino is a girl makes a big difference. Whatever it is, I'm glad to have found another show that engages with ideas below the surface. 

The setting of this show is a bunch of 'countries' that Kino travels through. The countries are of different technological levels, and have many differing philosophies and ways of life, and also conflicts. This makes it perfect for exploring different societies. 

This episode asks the question: is a country that exerts it's complete power to get what it wishes, though it tries to minimize damage, a good thing?

Is it ok to barrel through a country without paying it's tolls? Is it ok to have such high tolls that people might not be able or willing to pay? Both have their problems. 

I'm not being very ... shit I can't even think of the word, but that's what I'm not being right now if that makes sense. It's ok though. 
