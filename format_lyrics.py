# Basic script to produce a side by side lyric from a interleved one 
# Input file name is first argument, output file is the second argument
# Aditya Yedetore, March 2020

import sys
import pandas as pd

lyrics_file = sys.argv[1]

#combine all files in the list
df = pd.read_csv(lyrics_file, sep='$') # a seperator that won't occur

#strip the whitespace from the lyrics
df.iloc[:,0] = [x.strip() for x in df.iloc[:,0]]

#find the maximum length of the strings
maxlen = len(max(df.iloc[:,0], key=len)) + 1


count = 0 #count tells me the index to append the english string to
lyrics = [] # will hold the lyrics
comments = [] # will hold the title and all that 
#loop through the lyrics df, and append the string to comments 
#it starts with a # and to lyrics if not. Concatenate every other lyric
#(japaneese then english) to the previous
for x in df.iloc[:,0]:
    if x[0] != '#':
        if count % 2 == 0:
            lyrics.append( x + (' ' * (maxlen - len(x))) + '|  ' )
        else:
            lyrics[int(count/2)] = lyrics[int(count/2)] + x
        count += 1
    else:
        comments.append(x)

lyrics_df = pd.DataFrame(comments + lyrics) # combine the title and the lyrics

lyrics_df.to_csv( sys.argv[2], index=False, encoding='utf-8-sig', sep='@')

